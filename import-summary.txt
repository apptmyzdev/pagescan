ECLIPSE ANDROID PROJECT IMPORT SUMMARY
======================================

Risky Project Location:
-----------------------
The tools *should* handle project locations in any directory. However,
due to bugs, placing projects in directories containing spaces in the
path, or characters like ", ' and &, have had issues. We're working to
eliminate these bugs, but to save yourself headaches you may want to
move your project to a location where this is not a problem.
/Users/swathilolla/Documents/Studio Workspace Gati/PageScan
                                   -         -             

Ignored Files:
--------------
The following files were *not* copied into the new Gradle project; you
should evaluate whether these are still needed in your project and if
so manually move them:

* .DS_Store
* ic_launcher-web.png
* proguard-project.txt

Replaced Jars with Dependencies:
--------------------------------
The importer recognized the following .jar files as third party
libraries and replaced them with Gradle dependencies instead. This has
the advantage that more explicit version information is known, and the
libraries can be updated automatically. However, it is possible that
the .jar file in your project was of an older version than the
dependency we picked, which could render the project not compileable.
You can disable the jar replacement in the import wizard and try again:

android-support-v4.jar => com.android.support:support-v4:19.1.0
gson-2.2.1.jar => com.google.code.gson:gson:2.2.1
joda-time-2.4.jar => joda-time:joda-time:2.4

Potentially Missing Dependency:
-------------------------------
When we replaced the following .jar files with a Gradle dependency, we
inferred the dependency version number from the filename. This
specific version may not actually be available from the repository.
If you get a build error stating that the dependency is missing, edit
the version number to for example "+" to pick up the latest version
instead. (This may require you to update your code if the library APIs
have changed.)

gson-2.2.1.jar => version 2.2.1 in com.google.code.gson:gson:2.2.1
joda-time-2.4.jar => version 2.4 in joda-time:joda-time:2.4

Moved Files:
------------
Android Gradle projects use a different directory structure than ADT
Eclipse projects. Here's how the projects were restructured:

* AndroidManifest.xml => app/src/main/AndroidManifest.xml
* assets/ => app/src/main/assets/
* libs/armeabi-v7a/libiconv.so => app/src/main/jniLibs/armeabi-v7a/libiconv.so
* libs/armeabi-v7a/libzbarjni.so => app/src/main/jniLibs/armeabi-v7a/libzbarjni.so
* libs/armeabi/libiconv.so => app/src/main/jniLibs/armeabi/libiconv.so
* libs/armeabi/libzbarjni.so => app/src/main/jniLibs/armeabi/libzbarjni.so
* libs/commons-io-1.2.jar => app/libs/commons-io-1.2.jar
* libs/jackson-all-1.7.5.jar => app/libs/jackson-all-1.7.5.jar
* libs/ksoap2.5.4.jar => app/libs/ksoap2.5.4.jar
* libs/liblistviewanimations.jar => app/libs/liblistviewanimations.jar
* libs/libstickyheaders.jar => app/libs/libstickyheaders.jar
* libs/x86/libiconv.so => app/src/main/jniLibs/x86/libiconv.so
* libs/x86/libzbarjni.so => app/src/main/jniLibs/x86/libzbarjni.so
* libs/zbar.jar => app/libs/zbar.jar
* res/ => app/src/main/res/
* src/ => app/src/main/java/
* src/.DS_Store => app/src/main/resources/.DS_Store
* src/com/.DS_Store => app/src/main/resources/com/.DS_Store
* src/com/revamobile/.DS_Store => app/src/main/resources/com/revamobile/.DS_Store
* src/com/revamobile/gati/.DS_Store => app/src/main/resources/com/revamobile/gati/.DS_Store
* src/com/revamobile/gati/custom/.DS_Store => app/src/main/resources/com/revamobile/gati/custom/.DS_Store
* src/com/revamobile/gatiscan/.DS_Store => app/src/main/resources/com/revamobile/gatiscan/.DS_Store
* print_icon.PNG => print_icon.png

Next Steps:
-----------
You can now build the project. The Gradle project needs network
connectivity to download dependencies.

Bugs:
-----
If for some reason your project does not build, and you determine that
it is due to a bug or limitation of the Eclipse to Gradle importer,
please file a bug at http://b.android.com with category
Component-Tools.

(This import summary is for your information only, and can be deleted
after import once you are satisfied with the results.)
