package com.revamobile.gati.custom;

import java.util.ArrayList;

import se.emilsjolander.stickylistheaders.StickyListHeadersListView;
import android.annotation.SuppressLint;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.PopupWindow;

import com.revamobile.gatiscan.datacache.DataSource;
import com.revamobile.gatiscan.datacache.SaveDocketInfo;
import com.revamobile.pagescan.OnLineScreen;
import com.revamobile.pagescan.R;

@SuppressLint("InflateParams")
public class PrintHistoryPopup extends PopupWindows {

	private ArrayList<SaveDocketInfo> saveDocketInfos = new ArrayList<SaveDocketInfo>();
	private StickyListHeadersListView history_stickyListHeadersListView;
	private Button btn_cancel, btn_clear_all;
	private PrintHistoryAdapter historyAdapter;

	public PrintHistoryPopup(final OnLineScreen main,
			ArrayList<SaveDocketInfo> saveDocketInfos,
			PopupWindow.OnDismissListener listener) {
		super(main.getApplicationContext());
		LayoutInflater inflater = (LayoutInflater) main.getApplicationContext()
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		this.rootView = inflater.inflate(R.layout.history_popup,
				null);
		this.history_stickyListHeadersListView = (StickyListHeadersListView) rootView
				.findViewById(R.id.history_stickyListHeadersListView);

		if (this.saveDocketInfos == null) {
			this.saveDocketInfos = new ArrayList<SaveDocketInfo>();
		}

		this.saveDocketInfos.clear();
		for (SaveDocketInfo saveDocketInfo : saveDocketInfos) {
			this.saveDocketInfos.add(saveDocketInfo);
		}

		this.historyAdapter = new PrintHistoryAdapter(main, this,
				this.saveDocketInfos);

		this.history_stickyListHeadersListView.setAdapter(historyAdapter);

		this.btn_cancel = (Button) rootView.findViewById(R.id.btn_cancel);
		this.btn_cancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dismiss();
			}
		});

		this.btn_clear_all = (Button) rootView.findViewById(R.id.btn_clear_all);
		this.btn_clear_all.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				DataSource dataSource = new DataSource(main
						.getApplicationContext());
				dataSource.saveData.clearAll();
				dismiss();
			}
		});
		if (listener != null) {
			setOnDismissListener(listener);
		}
		// setBackground(main.getResources().getDrawable(R.drawable.screen_bg));
		setContentView(rootView);
	}

	public boolean isVisible() {
		return isShowing();
	}

	public void show(View anchor) {
		preShow();
		try {
			popupWindow.showAtLocation(anchor, Gravity.NO_GRAVITY, 0, 0);
			int screenWidth = windowManager.getDefaultDisplay().getWidth();
			int screenHeight = windowManager.getDefaultDisplay().getHeight();

			int windowWidth = screenWidth - (int) (screenWidth / 8f);
			int windowHeight = screenHeight - (int) (screenHeight / 10f);

			popupWindow.update((screenWidth - windowWidth) / 2, (screenHeight
					- windowHeight + 20) / 2, windowWidth, windowHeight);

		} catch (Exception e) {
			popupWindow.showAtLocation(anchor, Gravity.NO_GRAVITY, 0, 0);
			popupWindow.update(0, 0, 320, 480);
		}
	}

}