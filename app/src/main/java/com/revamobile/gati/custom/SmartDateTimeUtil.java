package com.revamobile.gati.custom;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.joda.time.DateMidnight;
import org.joda.time.DateTime;

import android.annotation.SuppressLint;

@SuppressLint("SimpleDateFormat")
@SuppressWarnings("deprecation")
public class SmartDateTimeUtil {

	public static String getHourMinuteString(Date date) {
		SimpleDateFormat hourMinuteFormat = new SimpleDateFormat(" h:m a");
		return hourMinuteFormat.format(date);
	}

	public static String getDateString(Date date) {
		SimpleDateFormat dateStringFormat = new SimpleDateFormat(
				"EEE',' MMM d y',' h:m a");
		return dateStringFormat.format(date);
	}

	public static boolean isToday(DateTime dateTime) {
		DateMidnight today = new DateMidnight();
		return today.equals(dateTime.toDateMidnight());
	}

	public static boolean isYesterday(DateTime dateTime) {
		DateMidnight yesterday = (new DateMidnight()).minusDays(1);
		return yesterday.equals(dateTime.toDateMidnight());
	}

	public static boolean isTomorrow(DateTime dateTime) {
		DateMidnight tomorrow = (new DateMidnight()).plusDays(1);
		return tomorrow.equals(dateTime.toDateMidnight());
	}

	public static String getFormattedTime(Date date) {
		if (date == null)
			return "";
		return new SimpleDateFormat("HH:mm:ss aa").format(date);
	}

	public static String getFormattedDate(Date date) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
				"d'DAY_SUFFIX' MMM, yyyy");
		String dateFormat = simpleDateFormat.format(date);

		dateFormat = dateFormat.replace("DAY_SUFFIX",
				getDaySuffix(date.getDate()));

		if (isToday(new DateTime(date)))
			dateFormat = "Today, " + dateFormat;
		else if (isYesterday(new DateTime(date)))
			dateFormat = "Yesterday, " + dateFormat;
		else if (isTomorrow(new DateTime(date)))
			dateFormat = "Tomorrow, " + dateFormat;
		return dateFormat;
	}

	public static String getDaySuffix(final int n) {
		if (n < 1 || n > 31)
			return "Invalid date";
		if (n >= 11 && n <= 13)
			return "th";

		switch (n % 10) {
		case 1:
			return "st";
		case 2:
			return "nd";
		case 3:
			return "rd";
		default:
			return "th";
		}
	}

	public static Date randomDate() {
		int year = randBetween(1989, 2015);// Here you can set Range of
		// years you need
		int month = randBetween(0, 11);

		GregorianCalendar gc = new GregorianCalendar(year, month, 1);
		int day = randBetween(1,
				gc.getActualMaximum(Calendar.DAY_OF_MONTH));

		gc.set(year, month, day, 10, 10, 10);
		return gc.getTime();
	}

	public static int randBetween(int start, int end) {
		return start + (int) Math.round(Math.random() * (end - start));
	}

}