package com.revamobile.gati.custom;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.PopupWindow;

public class PopupWindows {

	protected Context context;
	protected View rootView;
	protected PopupWindow popupWindow;
	protected WindowManager windowManager;
	protected Drawable background = null;

	public PopupWindows(Context context) {
		this.context = context;
		this.popupWindow = new PopupWindow(context);
		this.popupWindow.setTouchInterceptor(new OnTouchListener() {
			@SuppressLint("ClickableViewAccessibility")
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_OUTSIDE) {
					// mWindow.dismiss();
					return true;
				}
				return false;
			}
		});
		this.windowManager = (WindowManager) context
				.getSystemService(Context.WINDOW_SERVICE);
	}

	protected void preShow() {
		if (this.rootView == null)
			throw new IllegalStateException(
					"setContentView was not called with a view to display.");
		onShow();
		if (this.background == null)
			this.popupWindow.setBackgroundDrawable(new BitmapDrawable());
		else
			this.popupWindow.setBackgroundDrawable(this.background);

		this.popupWindow.setWidth(LayoutParams.MATCH_PARENT);
		this.popupWindow.setHeight(LayoutParams.MATCH_PARENT);
		this.popupWindow.setTouchable(true);
		this.popupWindow.setFocusable(false);
		this.popupWindow.setOutsideTouchable(false);

		this.popupWindow.setContentView(this.rootView);
	}

	public void setBackground(Drawable background) {
		this.background = background;
	}

	public void setContentView(View root) {
		this.rootView = root;
		this.popupWindow.setContentView(root);
	}

	public void dismiss() {
		this.popupWindow.dismiss();
	}

	public boolean isShowing() {
		return this.popupWindow.isShowing();
	}

	public void setOnDismissListener(PopupWindow.OnDismissListener listener) {
		this.popupWindow.setOnDismissListener(listener);
	}

	protected void onDismiss() {
	}

	protected void onShow() {
	}
}