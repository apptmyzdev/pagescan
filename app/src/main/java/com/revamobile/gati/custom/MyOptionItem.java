/*
 * 
 * Developed by Reva Tech Solutions (India) Private Limited
 * All code (c) 2011 Reva Tech Solutions (India) Private Limited
 * All rights reserved
 * 
 */

package com.revamobile.gati.custom;

import android.view.View.OnClickListener;

public class MyOptionItem {

	private String text;
	private boolean enabled;
	private OnClickListener onClickListener;

	public MyOptionItem() {
	}

	public MyOptionItem(String text, boolean enabled,
			OnClickListener onClickListener) {
		super();
		this.text = text;
		this.enabled = enabled;
		this.onClickListener = onClickListener;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public OnClickListener getOnClickListener() {
		return onClickListener;
	}

	public void setOnClickListener(OnClickListener onClickListener) {
		this.onClickListener = onClickListener;
	}

}