package com.revamobile.gati.custom;

import java.io.IOException;
import java.util.List;

import android.content.Context;
import android.hardware.Camera;
import android.hardware.Camera.AutoFocusCallback;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.PreviewCallback;
import android.hardware.Camera.Size;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;

public class CameraPreview extends ViewGroup implements SurfaceHolder.Callback {
	private final static String TAG = "CameraPreview";

	private SurfaceView mSurfaceView;
	private SurfaceHolder mHolder;
	private Size mPreviewSize;
	private List<Size> mSupportedPreviewSizes;
	private Camera mCamera;
	private PreviewCallback previewCallback;
	// private FlashSupport flashSupport;

	private AutoFocusCallback autoFocusCallback;

	public interface FlashSupport {
		public void flash(boolean isFlash);
	}

	public CameraPreview(Context context, SurfaceView sv) {
		super(context);

		mSurfaceView = sv;
		// addView(mSurfaceView);

		mHolder = mSurfaceView.getHolder();
		mHolder.addCallback(this);
		mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
	}

	public void setCamera(Camera camera, PreviewCallback previewCb,
			AutoFocusCallback autoFocusCb) {
		mCamera = camera;
		// this.flashSupport = flashSupport;
		previewCallback = previewCb;
		autoFocusCallback = autoFocusCb;

		// if (this.flashSupport == null) {
		// flash(false);
		// }

		if (mCamera != null) {
			mSupportedPreviewSizes = mCamera.getParameters()
					.getSupportedPreviewSizes();
			requestLayout();

			// get Camera parameters
			Camera.Parameters params = mCamera.getParameters();

			List<String> focusModes = params.getSupportedFocusModes();
			if (focusModes != null && focusModes.size() > 0) {
				if (focusModes.contains(Camera.Parameters.FOCUS_MODE_AUTO)) {
					// set the focus mode
					params.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
					// set Camera parameters
					mCamera.setParameters(params);
				}
			}
			if (previewCallback == null)
				mCamera.setPreviewCallback(null);
		}
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		// We purposely disregard child measurements because act as a
		// wrapper to a SurfaceView that centers the camera preview instead
		// of stretching it.
		final int width = resolveSize(getSuggestedMinimumWidth(),
				widthMeasureSpec);
		final int height = resolveSize(getSuggestedMinimumHeight(),
				heightMeasureSpec);
		setMeasuredDimension(width, height);

		if (mSupportedPreviewSizes != null) {
			mPreviewSize = getOptimalPreviewSize(mSupportedPreviewSizes, width,
					height);
		}
	}

	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b) {
		if (changed && getChildCount() > 0) {
			final View child = getChildAt(0);

			final int width = r - l;
			final int height = b - t;

			int previewWidth = width;
			int previewHeight = height;
			if (mPreviewSize != null) {
				previewWidth = mPreviewSize.width;
				previewHeight = mPreviewSize.height;
			}

			// Center the child SurfaceView within the parent.
			if (width * previewHeight > height * previewWidth) {
				final int scaledChildWidth = previewWidth * height
						/ previewHeight;
				child.layout((width - scaledChildWidth) / 2, 0,
						(width + scaledChildWidth) / 2, height);
			} else {
				final int scaledChildHeight = previewHeight * width
						/ previewWidth;
				child.layout(0, (height - scaledChildHeight) / 2, width,
						(height + scaledChildHeight) / 2);
			}
		}
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		// The Surface has been created, acquire the camera and tell it where
		// to draw.
		try {
			if (mCamera != null) {
				mCamera.setPreviewDisplay(holder);
			}
		} catch (IOException exception) {
			Log.e(TAG, "IOException caused by setPreviewDisplay()", exception);
		}
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		// Surface will be destroyed when we return, so stop the preview.
		if (mCamera != null) {
			mCamera.stopPreview();
		}
	}

	private Size getOptimalPreviewSize(List<Size> sizes, int w, int h) {
		final double ASPECT_TOLERANCE = 0.1;
		double targetRatio = (double) w / h;
		if (sizes == null)
			return null;

		Size optimalSize = null;
		double minDiff = Double.MAX_VALUE;

		int targetHeight = h;

		// Try to find an size match aspect ratio and size
		for (Size size : sizes) {
			double ratio = (double) size.width / size.height;
			if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE)
				continue;
			if (Math.abs(size.height - targetHeight) < minDiff) {
				optimalSize = size;
				minDiff = Math.abs(size.height - targetHeight);
			}
		}

		// Cannot find the one match the aspect ratio, ignore the requirement
		if (optimalSize == null) {
			minDiff = Double.MAX_VALUE;
			for (Size size : sizes) {
				if (Math.abs(size.height - targetHeight) < minDiff) {
					optimalSize = size;
					minDiff = Math.abs(size.height - targetHeight);
				}
			}
		}
		return optimalSize;
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
		if (mCamera != null) {
			mCamera.stopPreview();
			Camera.Parameters parameters = mCamera.getParameters();

			// if (flashSupport != null) {
			// try {
			// if (parameters.getFlashMode() != null)
			// flashSupport.flash(true);
			// else
			// flashSupport.flash(false);
			// } catch (Exception e) {
			// Log.d("DBG", "Error flash Support: " + e.getMessage());
			// flashSupport.flash(false);
			// }
			// }

			parameters.setPreviewSize(mPreviewSize.width, mPreviewSize.height);
//			Utils.logD("Preview width : " + mPreviewSize.width
//					+ "Preview height : " + mPreviewSize.height);
			requestLayout();

			mCamera.setDisplayOrientation(90);
			mCamera.setParameters(parameters);
			mCamera.setPreviewCallback(previewCallback);
			mCamera.startPreview();
			mCamera.autoFocus(autoFocusCallback);

		}
	}

	public void flash(boolean isFlash) {

		if (mCamera != null) {
			Parameters p = mCamera.getParameters();
			mCamera.cancelAutoFocus();
			mCamera.setPreviewCallback(null);
			mCamera.stopPreview();
			if (p.getFlashMode() != null) {
				if (isFlash) {
					p.setFlashMode(Parameters.FLASH_MODE_TORCH);
				} else {
					p.setFlashMode(Parameters.FLASH_MODE_OFF);
				}
				mCamera.setParameters(p);
				mCamera.setPreviewCallback(previewCallback);
				mCamera.startPreview();
				mCamera.autoFocus(autoFocusCallback);

			}
		}
	}

}
