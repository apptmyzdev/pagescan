/*
 * 
 * Developed by Reva Tech Solutions (India) Private Limited
 * Date: February 22, 2012
 * All code (c) 2011 Reva Tech Solutions (India) Private Limited
 * All rights reserved
 * 
 */
package com.revamobile.gati.custom;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RadioGroup;

import com.revamobile.pagescan.R;

public class ADRadioGroup extends RadioGroup {

	public ADRadioGroup(Context context) {
		super(context);
	}

	public ADRadioGroup(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	protected void onFinishInflate() {
		super.onFinishInflate();
		changeButtonsImages();
	}

	private void changeButtonsImages() {
		int count = super.getChildCount();

		if (count > 1) {
			super.getChildAt(0).setBackgroundResource(
					R.drawable.segment_radio_left);
			for (int i = 1; i < count - 1; i++) {
				super.getChildAt(i).setBackgroundResource(
						R.drawable.segment_radio_middle);
			}
			super.getChildAt(count - 1).setBackgroundResource(
					R.drawable.segment_radio_right);
		} else if (count == 1) {
			super.getChildAt(0)
					.setBackgroundResource(R.drawable.segment_button);
		}
	}
}