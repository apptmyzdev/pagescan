package com.revamobile.gati.custom;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;
import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.SectionIndexer;
import android.widget.TextView;

import com.revamobile.gatiscan.data.DocketInfo;
import com.revamobile.gatiscan.datacache.SaveDocketInfo;
import com.revamobile.gatiscan.utils.Constants;
import com.revamobile.pagescan.OnLineScreen;
import com.revamobile.pagescan.R;

@SuppressLint("SimpleDateFormat")
public class PrintHistoryAdapter extends BaseAdapter implements
		StickyListHeadersAdapter, SectionIndexer {
	private ArrayList<SaveDocketInfo> saveDocketInfos = new ArrayList<SaveDocketInfo>();
	private int[] headerIndices;
	private String[] headerTitles;
	private LayoutInflater layoutInflater;
	private PrintHistoryPopup printHistoryPopup;
	private OnLineScreen main;

	public PrintHistoryAdapter(OnLineScreen main,
			PrintHistoryPopup printHistoryPopup, ArrayList<SaveDocketInfo> list) {
		super();
		this.main = main;
		this.printHistoryPopup = printHistoryPopup;
		this.layoutInflater = LayoutInflater.from(main.getApplicationContext());
		restore(list);
	}

	private int[] getHeaderIndices() {
		if (saveDocketInfos != null && saveDocketInfos.size() > 0) {
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
			ArrayList<Integer> sectionIndices = new ArrayList<Integer>();
			String lastDate = null;
			for (int i = 0; i < saveDocketInfos.size(); i++) {
				String date = sdf.format(saveDocketInfos.get(i).getTimestamp());
				if (lastDate == null || !date.equalsIgnoreCase(lastDate)) {
					lastDate = sdf
							.format(saveDocketInfos.get(i).getTimestamp());
					sectionIndices.add(i);
				}
			}
			int[] sections = new int[sectionIndices.size()];
			for (int i = 0; i < sectionIndices.size(); i++) {
				sections[i] = sectionIndices.get(i);
			}
			return sections;
		}
		return new int[0];
	}

	private String[] getHeaderTitles() {
		if (headerIndices != null && headerIndices.length > 0) {
			String[] titles = new String[headerIndices.length];
			String headerTitle = "";
			for (int i = 0; i < headerIndices.length; i++) {
				headerTitle = "";
				try {
					SaveDocketInfo saveDocketInfo = saveDocketInfos
							.get(headerIndices[i]);
					if (saveDocketInfo != null) {
						Date date = saveDocketInfo.getTimestamp();
						headerTitle = SmartDateTimeUtil.getFormattedDate(date);
					}
				} catch (Exception e) {
				}
				titles[i] = headerTitle;
			}
			return titles;
		}
		return new String[0];
	}

	@Override
	public int getCount() {
		return saveDocketInfos.size();
	}

	@Override
	public Object getItem(int position) {
		return saveDocketInfos.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder = null;
		if (convertView == null) {
			convertView = layoutInflater.inflate(R.layout.history_docket_info,
					null);
			viewHolder = new ViewHolder(convertView);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		SaveDocketInfo saveDocketInfo = (SaveDocketInfo) getItem(position);
		if (saveDocketInfo != null) {
			DocketInfo data = saveDocketInfo.getDocketInfo();
			viewHolder.setInfo(data.getDocketno()
					+ Constants.DOKET_PRINT_SEPARATOR + data.getNo_of_pkgs()
					+ Constants.DOKET_PRINT_SEPARATOR + data.getDlystn());

			viewHolder.setTime(saveDocketInfo.getTimestamp());

			viewHolder.setReprint(position);
		}
		return convertView;
	}

	@Override
	public View getHeaderView(int position, View convertView, ViewGroup parent) {
		HeaderViewHolder headerViewHolder;
		if (convertView == null) {
			convertView = layoutInflater.inflate(R.layout.history_list_header,
					parent, false);
			headerViewHolder = new HeaderViewHolder(convertView);
			convertView.setTag(headerViewHolder);
		} else {
			headerViewHolder = (HeaderViewHolder) convertView.getTag();
		}

		headerViewHolder
				.setHeader((String) getSections()[getSectionForPosition(position)]);

		return convertView;
	}

	@Override
	public long getHeaderId(int position) {
		return getSectionForPosition(position);
	}

	@Override
	public int getPositionForSection(int section) {
		if (headerIndices.length == 0) {
			return 0;
		}
		if (section >= headerIndices.length) {
			section = headerIndices.length - 1;
		} else if (section < 0) {
			section = 0;
		}
		return headerIndices[section];
	}

	@Override
	public int getSectionForPosition(int position) {
		for (int i = 0; i < headerIndices.length; i++) {
			if (position < headerIndices[i]) {
				return i - 1;
			}
		}
		return headerIndices.length - 1;
	}

	@Override
	public Object[] getSections() {
		return headerTitles;
	}

	public void clear() {
		saveDocketInfos.clear();
		headerIndices = new int[0];
		headerTitles = new String[0];
		notifyDataSetChanged();
	}

	public void restore(ArrayList<SaveDocketInfo> list) {
		clear();
		if (saveDocketInfos != null) {
			for (SaveDocketInfo saveDocketInfo : list) {
				this.saveDocketInfos.add(saveDocketInfo);
			}

			Collections.sort(this.saveDocketInfos,
					new Comparator<SaveDocketInfo>() {
						@Override
						public int compare(SaveDocketInfo o1, SaveDocketInfo o2) {
							return o2.getTimestamp().compareTo(
									o1.getTimestamp());
						}
					});

		}
		this.headerIndices = getHeaderIndices();
		this.headerTitles = getHeaderTitles();

		notifyDataSetChanged();

	}

	class HeaderViewHolder {
		private TextView tv_his_header;

		public HeaderViewHolder(View v) {
			this.tv_his_header = ((TextView) v.findViewById(R.id.tv_his_header));
		}

		public void setHeader(String str) {
			if (this.tv_his_header != null) {
				if (str == null || str.length() == 0)
					str = "";
				this.tv_his_header.setText(str);
			}
		}
	}

	class ViewHolder {
		private Button reprint_btn;
		private TextView dkt_info;
		private TextView tv_dkt_time;

		public ViewHolder(View v) {
			this.reprint_btn = ((Button) v.findViewById(R.id.btn_reprint));
			this.dkt_info = ((TextView) v.findViewById(R.id.tv_dkt_info));
			this.tv_dkt_time = ((TextView) v.findViewById(R.id.tv_dkt_time));
		}

		public void setInfo(String str) {
			if (this.dkt_info != null) {
				if (str == null || str.length() == 0)
					str = "";
				this.dkt_info.setText(str);
			}
		}

		public void setTime(Date date) {
			if (this.tv_dkt_time != null) {
				String str = SmartDateTimeUtil.getFormattedTime(date);
				if (str == null || str.length() == 0)
					str = "";
				this.tv_dkt_time.setText(str);
			}
		}

		public void setReprint(int pos) {
			if (this.reprint_btn != null) {
				this.reprint_btn.setTag(pos + "");
				this.reprint_btn.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						try {
							int pos = Integer.parseInt(v.getTag().toString());
							SaveDocketInfo saveDocketInfo = saveDocketInfos
									.get(pos);
							if (saveDocketInfo != null
									&& saveDocketInfo.getDocketInfo() != null) {
								printHistoryPopup.dismiss();
								main.startReprintDocket(saveDocketInfo);
							}
						} catch (Exception e) {
						}
					}
				});
			}
		}
	}

}