/*
 * 
 * Developed by Reva Tech Solutions (India) Private Limited
 * Date: February 22, 2012
 * All code (c) 2011 Reva Tech Solutions (India) Private Limited
 * All rights reserved
 * 
 */
package com.revamobile.gati.custom;

public class DayInfo {
	private String day;
	private int MonthID;

	public DayInfo(String day, int monthID) {
		super();
		this.day = day;
		MonthID = monthID;
	}

	public String getDay() {
		return day;
	}

	public void setDay(String day) {
		this.day = day;
	}

	public int getMonthID() {
		return MonthID;
	}

	public void setMonthID(int monthID) {
		MonthID = monthID;
	}

	@Override
	public String toString() {
		return "DayInfo [day=" + day + ", MonthID=" + MonthID + "]";
	}

}