package com.revamobile.gatiscan.datacache;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.os.AsyncTask;

import com.revamobile.gatiscan.data.DktHunPkgMappingModel;
import com.revamobile.gatiscan.data.DocketInfo;
import com.revamobile.gatiscan.data.DocketToScan;
import com.revamobile.gatiscan.data.ScanDocket;
import com.revamobile.gatiscan.data.ScannedDocketsDataModel;
import com.revamobile.gatiscan.utils.Constants;
import com.revamobile.gatiscan.utils.Utils;
import com.revamobile.pagescan.R;
import com.revamobile.pagescan.ScanScreen;

public class DataSource {

	private SQLiteDatabase database;
	private DataHelper dbHelper;

	public ShardPreferences shardPreferences;
	public SaveData saveData;

	public DocketsData docketsData;
	public ScannedData scannedData;

	public DataSource(Context context) {
		try {
			dbHelper = new DataHelper(context);
			shardPreferences = new ShardPreferences();
			saveData = new SaveData();
			docketsData = new DocketsData();
			scannedData = new ScannedData();
		} catch (SQLiteException e) {
			Utils.logE(e.toString());
		} catch (VerifyError e) {
			Utils.logE(e.toString());
		}
	}

	@SuppressLint("NewApi")
	public void open() throws SQLException {
		if ((database != null && !database.isOpen()) || database == null) {
			try {
				database = dbHelper.getWritableDatabase();
				// isDatabaseOpen = true;
			}
			// catch (SQLiteDatabaseLockedException e) {
			// Utils.logE(e.toString());
			// }
			catch (Exception e) {
				Utils.logE(e.toString());
			}
		}
	}

	public void openRead() throws SQLException {
		if ((database != null && !database.isOpen()) || database == null) {
			database = dbHelper.getReadableDatabase();
			// isDatabaseOpen = true;
		}
	}

	public void close() {
		if ((database != null && database.isOpen())) {
			dbHelper.close();
			// isDatabaseOpen = false;
		}
	}

	// ************** SHARED_PREFERENCES **************//

	public class ShardPreferences {

		public void set(String key, String value) {
			open();
			try {
				if (!Utils.isValidString(value))
					value = "";

				if (Utils.isValidString(key)) {
					int id = exists(key);
					if (id == -1) {
						create(key, value);
					} else {
						update(key, value);
					}
				}
			} catch (Exception e) {
				Utils.logE(e.toString());
			}
			close();
		}

		public void update(String key, String value) {
			open();
			try {
				ContentValues values = new ContentValues();
				values.put(DataHelper.SHARED_PREF_COLUMN_VALUE, value);

				if (database.update(DataHelper.SHARED_PREF_TABLE_NAME, values,
						DataHelper.SHARED_PREF_COLUMN_KEY + " = '" + key + "'",
						null) > 0) {
				} else {
				}
			} catch (Exception e) {
				Utils.logE(e.toString());
			}
			close();

		}

		public void create(String key, String value) {
			open();
			try {
				ContentValues values = new ContentValues();
				values.put(DataHelper.SHARED_PREF_COLUMN_KEY, key);
				values.put(DataHelper.SHARED_PREF_COLUMN_VALUE, value);

				if (database.insert(DataHelper.SHARED_PREF_TABLE_NAME, null,
						values) > 0) {
				} else {
				}
			} catch (Exception e) {
				Utils.logE(e.toString());
			}
			close();
		}

		public void delete(String key) {
			open();
			try {
				if (database.delete(DataHelper.SHARED_PREF_TABLE_NAME,
						DataHelper.SHARED_PREF_COLUMN_KEY + " = '" + key + "'",
						null) > 0) {
				} else {
				}
			} catch (Exception e) {
				Utils.logE(e.toString());
			}
			close();
		}

		public List<SharedPreferenceItem> getAll() {
			openRead();
			List<SharedPreferenceItem> items = new ArrayList<SharedPreferenceItem>();
			Cursor cursor = null;
			try {

				cursor = database.query(DataHelper.SHARED_PREF_TABLE_NAME,
						DataHelper.SHARED_PREF_COLUMNS, null, null, null, null,
						null);

				cursor.moveToFirst();
				while (!cursor.isAfterLast()) {
					SharedPreferenceItem item = cursorToItem(cursor);
					items.add(item);
					cursor.moveToNext();
				}

			} catch (Exception e) {
				Utils.logE(e.toString());
				cursor = null;
			}
			if (cursor != null)
				cursor.close();
			close();
			return items;
		}

		public String getValue(String key) {
			openRead();

			Cursor cursor = null;
			String value = "";

			try {
				String selectQuery = "SELECT  "
						+ DataHelper.SHARED_PREF_COLUMN_VALUE + " FROM "
						+ DataHelper.SHARED_PREF_TABLE_NAME + " WHERE "
						+ DataHelper.SHARED_PREF_COLUMN_KEY + " = '" + key
						+ "'";

				cursor = database.rawQuery(selectQuery, null);

				if (cursor.moveToFirst()) {
					value = cursor.getString(0);
				}

			} catch (Exception e) {
				Utils.logE(e.toString());
				cursor = null;
			}

			if (cursor != null)
				cursor.close();
			close();
			return value;

		}

		public int exists(String key) {
			openRead();
			int id = -1;
			try {
				String selectQuery = "SELECT  " + DataHelper.SHARED_PREF_KEY_ID
						+ " FROM " + DataHelper.SHARED_PREF_TABLE_NAME
						+ " WHERE " + DataHelper.SHARED_PREF_COLUMN_KEY
						+ " = '" + key + "'";

				Cursor cursor = database.rawQuery(selectQuery, null);
				if (cursor.moveToFirst()) {
					id = cursor.getInt(0);
				}
			} catch (Exception e) {
				Utils.logE(e.toString());
			}
			close();

			return id;
		}

		private SharedPreferenceItem cursorToItem(Cursor cursor) {
			return new SharedPreferenceItem(cursor.getString(0),
					cursor.getString(1));
		}
	}

	/** **/

	public class SaveData {
		public void truncateTable() {
			open();
			database.execSQL("DROP TABLE IF EXISTS "
					+ DataHelper.PRINT_LIST_TABLE_NAME);
			database.execSQL(DataHelper.PRINT_LIST_DATABASE_CREATE);
			close();
		}

		public void clearAll() {
			open();
			String query = "DELETE FROM " + DataHelper.PRINT_LIST_TABLE_NAME;
			database.execSQL(query);

			close();
		}

		public long updateAccessTime(long id) {
			long record = -1;
			if (id == -1)
				return record;
			open();
			try {
				ContentValues values = new ContentValues();
				values.put(DataHelper.PRINT_LIST_COLUMN_TIME_STAMP,
						new Date().getTime());
				record = database.update(DataHelper.PRINT_LIST_TABLE_NAME,
						values, DataHelper.PRINT_LIST_COLUMN_KEY_ID + " = "
								+ id, null);
			} catch (Exception e) {
			}
			close();
			return record;
		}

		public long exists(DocketInfo data) {
			open();
			long record = -1;
			try {
				String selectQuery = "SELECT  "
						+ DataHelper.PRINT_LIST_COLUMN_KEY_ID + " FROM "
						+ DataHelper.PRINT_LIST_TABLE_NAME + " WHERE "

						+ DataHelper.PRINT_LIST_COLUMN_DOCKET_NO + " = '"
						+ data.getDocketno() + "'" + " AND "

						+ DataHelper.PRINT_LIST_COLUMN_DLYSTN + " = '"
						+ data.getDlystn() + "'" + " AND "

						+ DataHelper.PRINT_LIST_COLUMN_ASSURED_DLY_DT + " = '"
						+ data.getAssured_dly_dt() + "'" + " AND "

						+ DataHelper.PRINT_LIST_COLUMN_NO_OF_PKGS + " = "
						+ data.getNo_of_pkgs() + "" + " AND "

						+ DataHelper.PRINT_LIST_COLUMN_SPKGNO + " = "
						+ data.getSpkgno() + "" + " AND "

						+ DataHelper.PRINT_LIST_COLUMN_SRVCODE + " = '"
						+ data.getSrvcode() + "'" + " AND "

						+ DataHelper.PRINT_LIST_COLUMN_PARCEL + " = '"
						+ data.getParcel() + "'" +

						DataHelper.PRINT_LIST_COLUMN_RECEIVER_NAME + " = '"
						+ data.getReceiver_name() + "'" +

						" COLLATE NOCASE;";

				Cursor cursor = database.rawQuery(selectQuery, null);
				if (cursor.moveToFirst()) {
					record = cursor.getInt(0);
				}
			} catch (Exception e) {
			}
			close();

			return record;
		}

		public long saveDocketInfo(DocketInfo data) {
			if (data == null)
				return -1;
			long record = updateAccessTime(exists(data));
			if (record == -1) {
				open();
				ContentValues values = new ContentValues();

				values.put(DataHelper.PRINT_LIST_COLUMN_DOCKET_NO,
						data.getDocketno());
				values.put(DataHelper.PRINT_LIST_COLUMN_DLYSTN,
						data.getDlystn());
				values.put(DataHelper.PRINT_LIST_COLUMN_ASSURED_DLY_DT,
						data.getAssured_dly_dt());
				values.put(DataHelper.PRINT_LIST_COLUMN_NO_OF_PKGS,
						data.getNo_of_pkgs());
				values.put(DataHelper.PRINT_LIST_COLUMN_SPKGNO,
						data.getSpkgno());
				values.put(DataHelper.PRINT_LIST_COLUMN_SRVCODE,
						data.getSrvcode());
				values.put(DataHelper.PRINT_LIST_COLUMN_PARCEL,
						data.getParcel());
				values.put(DataHelper.PRINT_LIST_COLUMN_RECEIVER_NAME,
						data.getReceiver_name());
				values.put(DataHelper.PRINT_LIST_COLUMN_TIME_STAMP,
						new Date().getTime());

				record = database.insert(DataHelper.PRINT_LIST_TABLE_NAME,
						null, values);
				close();
				deleteOldSaveDocketInfo();
			}
			return record;
		}

		public ArrayList<SaveDocketInfo> getDocketInfoList() {
			ArrayList<SaveDocketInfo> list = new ArrayList<SaveDocketInfo>();

			openRead();

			String sql = "SELECT * FROM " + DataHelper.PRINT_LIST_TABLE_NAME;

			Cursor cursor = database.rawQuery(sql, null);

			if (cursor.moveToFirst()) {
				int idxDocketNo = cursor
						.getColumnIndex(DataHelper.PRINT_LIST_COLUMN_DOCKET_NO);
				int idxDLystn = cursor
						.getColumnIndex(DataHelper.PRINT_LIST_COLUMN_DLYSTN);
				int idxAssured = cursor
						.getColumnIndex(DataHelper.PRINT_LIST_COLUMN_ASSURED_DLY_DT);
				int idxNoofpkgs = cursor
						.getColumnIndex(DataHelper.PRINT_LIST_COLUMN_NO_OF_PKGS);
				int idxSPkgNo = cursor
						.getColumnIndex(DataHelper.PRINT_LIST_COLUMN_SPKGNO);
				int idxSrvcode = cursor
						.getColumnIndex(DataHelper.PRINT_LIST_COLUMN_SRVCODE);
				int idxParcel = cursor
						.getColumnIndex(DataHelper.PRINT_LIST_COLUMN_PARCEL);
				int idxReceiver = cursor
						.getColumnIndex(DataHelper.PRINT_LIST_COLUMN_RECEIVER_NAME);
				int idxTimestamp = cursor
						.getColumnIndex(DataHelper.PRINT_LIST_COLUMN_TIME_STAMP);
				do {
					SaveDocketInfo item = new SaveDocketInfo();

					DocketInfo info = new DocketInfo();
					info.setDocketno(cursor.getString(idxDocketNo));
					info.setDlystn(cursor.getString(idxDLystn));
					info.setAssured_dly_dt(cursor.getString(idxAssured));
					info.setNo_of_pkgs(cursor.getInt(idxNoofpkgs));
					info.setSpkgno(cursor.getInt(idxSPkgNo));
					info.setSrvcode(cursor.getString(idxSrvcode));
					info.setParcel(cursor.getString(idxParcel));
					info.setReceiver_name(cursor.getString(idxReceiver));

					item.setTimestamp(new Date(cursor.getLong(idxTimestamp)));
					item.setDocketInfo(info);

					list.add(item);
				} while (cursor.moveToNext());
			}

			close();

			return list;
		}

		public int getNumberOfSaveDocketInfo() {
			int numRecords = 0;
			open();
			Cursor cursor = null;
			try {
				cursor = database.query(DataHelper.PRINT_LIST_TABLE_NAME,
						DataHelper.PRINT_LIST_COLUMNS, null, null, null, null,
						null);
				numRecords = cursor.getCount();
			} catch (Exception e) {
				cursor = null;
			}
			if (cursor != null)
				cursor.close();
			close();
			return numRecords;
		}

		public void delete(int cId) {
			open();
			try {
				String whereClause = DataHelper.PRINT_LIST_COLUMN_KEY_ID
						+ " = " + cId;
				int rowId = database.delete(DataHelper.PRINT_LIST_TABLE_NAME,
						whereClause, null);
				if (rowId != -1) {
				} else {
				}
			} catch (Exception e) {
			}
			close();
		}

		public void deleteOldSaveDocketInfo() {

			int numRecords = 0;
			open();
			Cursor cursor = null;
			try {
				cursor = database.query(DataHelper.PRINT_LIST_TABLE_NAME,
						DataHelper.PRINT_LIST_COLUMNS, null, null, null, null,
						DataHelper.PRINT_LIST_COLUMN_TIME_STAMP + " ASC");
				numRecords = cursor.getCount();

				if (cursor.moveToFirst()
						&& numRecords-- > Constants.MAX_NUMBER_OF_SAVE_DOCKET_INFO) {
					do {
						int cId = cursor.getInt(0);
						delete(cId);
					} while (cursor.moveToNext()
							&& numRecords-- > Constants.MAX_NUMBER_OF_SAVE_DOCKET_INFO);
				}

			} catch (Exception e) {
				cursor = null;
			}
			if (cursor != null)
				cursor.close();
			close();
		}

	}

	/** Dockets Data **/

	public class DocketsData {

		public void truncateTable() {

			open();

			if (database != null) {
				String query = "DELETE FROM " + DataHelper.DOCKETS_TABLE_NAME;
				database.execSQL(query);
			}

			close();
		}

		public void clearAll() {
			open();
			String query = "DELETE FROM " + DataHelper.DOCKETS_TABLE_NAME;
			database.execSQL(query);

			close();
		}

		public void clearDocketsData(String custCode, String custVentCode) {
			open();
			database.delete(DataHelper.DOCKETS_TABLE_NAME,
					DataHelper.DOCKETS_COLUMN_CUST_CODE + " = '" + custCode
							+ "' AND " + DataHelper.DOCKETS_COLUMN_CUST_VENTCODE
							+ " = '" + custVentCode + "'", null);
			close();
		}

		private Context context;
		private ScanScreen scanScreen;

		private ProgressDialog pDialog;
		private boolean isProgress = false;
		private String custcode, custventcode;
		private ArrayList<DocketToScan> dockets;

		public void saveData(Context mContext, String custCode,
				String custVentCode, ArrayList<DocketToScan> data) {
			if (!isProgress) {
				isProgress = true;
				context = mContext;
				scanScreen = (ScanScreen) context;
				custcode = custCode;
				custcode = custVentCode;
				dockets = data;
				new Save().execute();
			}
		}

		private void showProgressDialog() {
			if (pDialog == null) {
				pDialog = new ProgressDialog(context);
				pDialog.setMessage(context
						.getString(R.string.progress_dialog_msg));
				pDialog.setIndeterminate(true);
				pDialog.setCancelable(false);
				pDialog.show();
			}
		}

		public class Save extends AsyncTask<Object, Object, Object> {
			@Override
			protected void onPreExecute() {
				showProgressDialog();
				Utils.logD("Start saving the data");
			}

			@Override
			protected Object doInBackground(Object... arg0) {

				try {
					long c = cacheDocketsData(context, custcode, custventcode,
							dockets);
					Utils.logD("Count : " + c);
				} catch (Exception e) {
					Utils.logE(e.toString());
				}
				return null;

			}

			@Override
			protected void onPostExecute(Object result) {

				if (pDialog != null && pDialog.isShowing()) {
					pDialog.dismiss();
					scanScreen.startScan();
					// scanDockets.resetAllFragmentsData();
					pDialog = null;
					isProgress = false;
					Utils.logD("Loaded data");
				}

				super.onPostExecute(result);
			}
		}

		public long cacheDocketsData(Context mContext, String custCode,
				String custVentCode, ArrayList<DocketToScan> data) {

			int numberOfRecords = 0;

			open();

			ContentValues values = new ContentValues();
			for (DocketToScan docket : data) {
				String docketNum = docket.getDocketNo();
				if (Utils.isValidString(docketNum)) {
					int id = exists(custCode, custVentCode, docketNum);
					if (id == -1) {
						values.put(DataHelper.DOCKETS_COLUMN_CUST_CODE,
								custCode);
						values.put(DataHelper.DOCKETS_COLUMN_CUST_VENTCODE,
								custVentCode);

						values.put(DataHelper.DOCKETS_COLUMN_DOCKET_NO,
								docketNum);

						ArrayList<String> hunList = docket.getHunBarCode();
						ArrayList<String> pktList = docket.getPktNos();
						for (int i = 0; i < hunList.size(); i++) {
							String hun = hunList.get(i);
							if (Utils.isValidString(hun))
								values.put(DataHelper.DOCKETS_COLUMN_HUN_NUM,
										hun);

							String pkt = pktList.get(i);
							if (Utils.isValidString(pkt))
								values.put(DataHelper.DOCKETS_COLUMN_PKT_NUM,
										pkt);

							long retVal = database
									.insert(DataHelper.DOCKETS_TABLE_NAME,
											null, values);
							if (retVal > -1)
								numberOfRecords++;
						}

					}
				}
			}

			close();

			Utils.logD("Records : " + numberOfRecords);
			return numberOfRecords;

		}

		public int exists(String custCode, String custVentCode, String dktNum) {
			int id = -1;
			Cursor cursor = null;
			try {
				String selectQuery = "SELECT  "
						+ DataHelper.DOCKETS_COLUMN_KEY_ID + " FROM "
						+ DataHelper.DOCKETS_TABLE_NAME + " WHERE "
						+ DataHelper.DOCKETS_COLUMN_CUST_CODE + " = '"
						+ custCode + "' AND "
						+ DataHelper.DOCKETS_COLUMN_CUST_VENTCODE + " = '"
						+ custVentCode + "' AND "
						+ DataHelper.DOCKETS_COLUMN_DOCKET_NO + " = '" + dktNum
						+ "'";

				cursor = database.rawQuery(selectQuery, null);
				if (cursor.moveToFirst()) {
					id = cursor.getInt(0);
				}
			} catch (Exception e) {
				Utils.logE(e.toString());
			} finally {
				if (cursor != null)
					cursor.close();
			}

			return id;
		}

		public boolean existsHun(String custCode, String custVentCode,
				String hunNum) {
			boolean found = false;
			openRead();
			Cursor cursor = null;
			try {
				String selectQuery = "SELECT  "
						+ DataHelper.DOCKETS_COLUMN_KEY_ID + " FROM "
						+ DataHelper.DOCKETS_TABLE_NAME + " WHERE "
						+ DataHelper.DOCKETS_COLUMN_CUST_CODE + " = '"
						+ custCode + "' AND "
						+ DataHelper.DOCKETS_COLUMN_CUST_VENTCODE + " = '"
						+ custVentCode + "' AND "
						+ DataHelper.DOCKETS_COLUMN_HUN_NUM + " = '" + hunNum
						+ "'";

				cursor = database.rawQuery(selectQuery, null);
				if (cursor.moveToFirst()) {
					int id = cursor.getInt(0);
					if (id != -1)
						found = true;
					else
						found = false;
				}
			} catch (Exception e) {
				Utils.logE(e.toString());
			} finally {
				if (cursor != null)
					cursor.close();
			}
			close();

			return found;
		}

		public boolean existsPkt(String custCode, String custVentCode,
				String pktNum) {
			boolean found = false;
			openRead();
			Cursor cursor = null;
			try {
				String selectQuery = "SELECT  "
						+ DataHelper.DOCKETS_COLUMN_KEY_ID + " FROM "
						+ DataHelper.DOCKETS_TABLE_NAME + " WHERE "
						+ DataHelper.DOCKETS_COLUMN_CUST_CODE + " = '"
						+ custCode + "' AND "
						+ DataHelper.DOCKETS_COLUMN_CUST_VENTCODE + " = '"
						+ custVentCode + "' AND "
						+ DataHelper.DOCKETS_COLUMN_PKT_NUM + " = '" + pktNum
						+ "'";

				cursor = database.rawQuery(selectQuery, null);
				if (cursor.moveToFirst()) {
					int id = cursor.getInt(0);
					if (id != -1)
						found = true;
					else
						found = false;
				}
			} catch (Exception e) {
				Utils.logE(e.toString());
			} finally {
				if (cursor != null)
					cursor.close();
			}
			close();

			return found;
		}

		public String getDktNum(String custCode, String custVentCode,
				String hunNum, String pktNum, boolean isHun) {
			String dkt = null;
			openRead();
			Cursor cursor = null;
			try {
				String selectQuery;
				if (isHun)
					selectQuery = "SELECT  "
							+ DataHelper.DOCKETS_COLUMN_DOCKET_NO + " FROM "
							+ DataHelper.DOCKETS_TABLE_NAME + " WHERE "
							+ DataHelper.DOCKETS_COLUMN_CUST_CODE + " = '"
							+ custCode + "' AND "
							+ DataHelper.DOCKETS_COLUMN_CUST_VENTCODE + " = '"
							+ custVentCode + "' AND "
							+ DataHelper.DOCKETS_COLUMN_HUN_NUM + " = '"
							+ hunNum + "'";
				else
					selectQuery = "SELECT  "
							+ DataHelper.DOCKETS_COLUMN_DOCKET_NO + " FROM "
							+ DataHelper.DOCKETS_TABLE_NAME + " WHERE "
							+ DataHelper.DOCKETS_COLUMN_CUST_CODE + " = '"
							+ custCode + "' AND "
							+ DataHelper.DOCKETS_COLUMN_CUST_VENTCODE + " = '"
							+ custVentCode + "' AND "
							+ DataHelper.DOCKETS_COLUMN_PKT_NUM + " = '"
							+ pktNum + "'";

				cursor = database.rawQuery(selectQuery, null);
				if (cursor.moveToFirst()) {
					String id = cursor.getString(0);
					if (Utils.isValidString(id))
						dkt = id;
				}
			} catch (Exception e) {
				Utils.logE(e.toString());
			} finally {
				if (cursor != null)
					cursor.close();
			}
			close();

			return dkt;
		}

		public int getDktsSize(String custCode, String custVentCode) {
			openRead();
			String sql = "SELECT DISTINCT "
					+ DataHelper.DOCKETS_COLUMN_DOCKET_NO + " FROM "
					+ DataHelper.DOCKETS_TABLE_NAME + " WHERE "
					+ DataHelper.DOCKETS_COLUMN_CUST_CODE + " = '" + custCode
					+ "' AND " + DataHelper.DOCKETS_COLUMN_CUST_VENTCODE
					+ " = '" + custVentCode + "'";
			Cursor cursor = database.rawQuery(sql, null);
			int count = cursor.getCount();
			cursor.close();
			close();
			return count;
		}

		public int getHunsSize(String custCode, String custVentCode) {
			openRead();
			String sql = "SELECT DISTINCT " + DataHelper.DOCKETS_COLUMN_HUN_NUM
					+ " FROM " + DataHelper.DOCKETS_TABLE_NAME + " WHERE "
					+ DataHelper.DOCKETS_COLUMN_CUST_CODE + " = '" + custCode
					+ "' AND " + DataHelper.DOCKETS_COLUMN_CUST_VENTCODE
					+ " = '" + custVentCode + "'";
			Cursor cursor = database.rawQuery(sql, null);
			int count = cursor.getCount();
			cursor.close();
			close();
			return count;
		}

		public int getPktsSize(String custCode, String custVentCode) {
			openRead();
			String sql = "SELECT DISTINCT " + DataHelper.DOCKETS_COLUMN_PKT_NUM
					+ " FROM " + DataHelper.DOCKETS_TABLE_NAME + " WHERE "
					+ DataHelper.DOCKETS_COLUMN_CUST_CODE + " = '" + custCode
					+ "' AND " + DataHelper.DOCKETS_COLUMN_CUST_VENTCODE
					+ " = '" + custVentCode + "'";
			Cursor cursor = database.rawQuery(sql, null);
			int count = cursor.getCount();
			cursor.close();
			close();
			return count;
		}

		public ArrayList<String> getDocketsList(String custCode,
				String custVentCode) {
			openRead();
			ArrayList<String> dockets = new ArrayList<String>();

			String sql = "SELECT DISTINCT "
					+ DataHelper.DOCKETS_COLUMN_DOCKET_NO + " FROM "
					+ DataHelper.DOCKETS_TABLE_NAME + " WHERE "
					+ DataHelper.DOCKETS_COLUMN_CUST_CODE + " = '" + custCode
					+ "' AND " + DataHelper.DOCKETS_COLUMN_CUST_VENTCODE
					+ " = '" + custVentCode + "'";

			Cursor cursor = database.rawQuery(sql, null);

			if (cursor.moveToFirst()) {
				do {
					String docketNum = cursor.getString(0);
					if (Utils.isValidString(docketNum))
						dockets.add(docketNum);

				} while (cursor.moveToNext());
			}

			cursor.close();
			close();

			return dockets;
		}

	}

	/** Dockets Data **/

	public class ScannedData {

		public void truncateTable() {

			open();

			if (database != null) {
				String query = "DELETE FROM " + DataHelper.SCANNED_TABLE_NAME;
				database.execSQL(query);
			}

			close();
		}

		public void clearAll() {
			open();
			String query = "DELETE FROM " + DataHelper.SCANNED_TABLE_NAME;
			database.execSQL(query);

			close();
		}

		public void clearScanData(String custCode, String custVentCode) {
			open();
			database.delete(DataHelper.SCANNED_TABLE_NAME,
					DataHelper.SCANNED_COLUMN_CUST_CODE + " = '" + custCode
							+ "' AND "
							+ DataHelper.SCANNED_COLUMN_CUST_VENTCODE + " = '"
							+ custVentCode + "'", null);
			close();
		}

		public void cacheScanData(String custCode, String custVentCode,
				ScanDocket scanDocket) {

			open();

			ContentValues values = new ContentValues();
			String docketNum = scanDocket.getDocketNo();
			if (Utils.isValidString(docketNum)) {
				String hun = scanDocket.getHunNo();
				if (Utils.isValidString(hun)) {
					int id = exists(custCode, custVentCode, docketNum, hun);
					if (id == -1) {
						values.put(DataHelper.SCANNED_COLUMN_CUST_CODE,
								custCode);
						values.put(DataHelper.SCANNED_COLUMN_CUST_VENTCODE,
								custVentCode);

						values.put(DataHelper.SCANNED_COLUMN_DOCKET_NO,
								docketNum);
						values.put(DataHelper.SCANNED_COLUMN_HUN_NUM, hun);

						if (scanDocket.isHunScanned())
							values.put(DataHelper.SCANNED_COLUMN_HUN_SCANNED,
									Constants.TRUE);
						else
							values.put(DataHelper.SCANNED_COLUMN_HUN_SCANNED,
									Constants.FALSE);

						String hunScanTime = scanDocket.getHunScanTime();
						if (Utils.isValidString(hunScanTime))
							values.put(DataHelper.SCANNED_COLUMN_HUN_SCANTIME,
									hunScanTime);

						long retVal = database.insert(
								DataHelper.SCANNED_TABLE_NAME, null, values);
						if (retVal > -1)
							Utils.logD("" + retVal);
					} else {
						String pkt = scanDocket.getPktNo();
						if (Utils.isValidString(pkt))
							values.put(DataHelper.SCANNED_COLUMN_PKT_NUM, pkt);

						if (scanDocket.isPktScanned())
							values.put(DataHelper.SCANNED_COLUMN_PKT_SCANNED,
									Constants.TRUE);
						else
							values.put(DataHelper.SCANNED_COLUMN_PKT_SCANNED,
									Constants.FALSE);

						String pktScanTime = scanDocket.getPktScanTime();
						if (Utils.isValidString(pktScanTime))
							values.put(DataHelper.SCANNED_COLUMN_PKT_SCANTIME,
									pktScanTime);

						long retVal = database.update(
								DataHelper.SCANNED_TABLE_NAME, values,
								DataHelper.SCANNED_COLUMN_KEY_ID + " = " + id,
								null);
						Utils.logD("" + retVal);
					}

				}
			}

			close();

		}

		public int exists(String custCode, String custVentCode, String dktNum,
				String hunNum) {
			int id = -1;
			Cursor cursor = null;
			try {
				String selectQuery = "SELECT  "
						+ DataHelper.SCANNED_COLUMN_KEY_ID + " FROM "
						+ DataHelper.SCANNED_TABLE_NAME + " WHERE "
						+ DataHelper.SCANNED_COLUMN_CUST_CODE + " = '"
						+ custCode + "' AND "
						+ DataHelper.SCANNED_COLUMN_CUST_VENTCODE + " = '"
						+ custVentCode + "' AND "
						+ DataHelper.SCANNED_COLUMN_DOCKET_NO + " = '" + dktNum
						+ "' AND " + DataHelper.SCANNED_COLUMN_HUN_NUM + " = '"
						+ hunNum + "'";

				cursor = database.rawQuery(selectQuery, null);
				if (cursor.moveToFirst()) {
					id = cursor.getInt(0);
				}
			} catch (Exception e) {
				Utils.logE(e.toString());
			} finally {
				if (cursor != null)
					cursor.close();
			}

			return id;
		}

		public ArrayList<ScanDocket> getScanDocketsList(String custCode,
				String custVentCode) {
			ArrayList<ScanDocket> dockets = new ArrayList<ScanDocket>();

			openRead();

			String sql = "SELECT * FROM " + DataHelper.SCANNED_TABLE_NAME
					+ " WHERE " + DataHelper.SCANNED_COLUMN_CUST_CODE + " = '"
					+ custCode + "' AND "
					+ DataHelper.SCANNED_COLUMN_CUST_VENTCODE + " = '"
					+ custVentCode + "' AND "
					+ DataHelper.SCANNED_COLUMN_HUN_SCANNED + " = '"
					+ Constants.TRUE + "'";

			Cursor cursor = database.rawQuery(sql, null);

			if (cursor.moveToFirst()) {
				int dktId = cursor
						.getColumnIndex(DataHelper.SCANNED_COLUMN_DOCKET_NO);
				int hunId = cursor
						.getColumnIndex(DataHelper.SCANNED_COLUMN_HUN_NUM);
				int pktId = cursor
						.getColumnIndex(DataHelper.SCANNED_COLUMN_PKT_NUM);

				int hunScanId = cursor
						.getColumnIndex(DataHelper.SCANNED_COLUMN_HUN_SCANNED);
				int pktScanId = cursor
						.getColumnIndex(DataHelper.SCANNED_COLUMN_PKT_SCANNED);
				int hunTimeId = cursor
						.getColumnIndex(DataHelper.SCANNED_COLUMN_HUN_SCANTIME);
				int pktTimeId = cursor
						.getColumnIndex(DataHelper.SCANNED_COLUMN_PKT_SCANTIME);

				do {

					ScanDocket docket = new ScanDocket();

					String dktNum = cursor.getString(dktId);
					if (Utils.isValidString(dktNum))
						docket.setDocketNo(dktNum);

					String hunNum = cursor.getString(hunId);
					if (Utils.isValidString(hunNum))
						docket.setHunNo(hunNum);

					String pktNum = cursor.getString(pktId);
					if (Utils.isValidString(pktNum))
						docket.setPktNo(pktNum);

					String hunScanned = cursor.getString(hunScanId);
					if (Utils.isValidString(hunScanned)
							&& hunScanned.equalsIgnoreCase(Constants.TRUE))
						docket.setHunScanned(true);
					else
						docket.setHunScanned(false);

					String pktScanned = cursor.getString(pktScanId);
					if (Utils.isValidString(pktScanned)
							&& pktScanned.equalsIgnoreCase(Constants.TRUE))
						docket.setPktScanned(true);
					else
						docket.setPktScanned(false);

					String hunTime = cursor.getString(hunTimeId);
					if (Utils.isValidString(hunTime))
						docket.setHunScanTime(hunTime);

					String pktTime = cursor.getString(pktTimeId);
					if (Utils.isValidString(pktTime))
						docket.setPktScanTime(pktTime);

					dockets.add(docket);

				} while (cursor.moveToNext());
			}

			cursor.close();
			close();
			return dockets;
		}

		public ArrayList<ScanDocket> getDocketsList(String custCode,
				String custVentCode) {
			ArrayList<ScanDocket> dockets = new ArrayList<ScanDocket>();

			openRead();

			String sql = "SELECT * FROM " + DataHelper.SCANNED_TABLE_NAME
					+ " WHERE " + DataHelper.SCANNED_COLUMN_CUST_CODE + " = '"
					+ custCode + "' AND "
					+ DataHelper.SCANNED_COLUMN_CUST_VENTCODE + " = '"
					+ custVentCode + "' AND "
					+ DataHelper.SCANNED_COLUMN_HUN_SCANNED + " = '"
					+ Constants.TRUE + "' AND "
					+ DataHelper.SCANNED_COLUMN_PKT_SCANNED + " = '"
					+ Constants.TRUE + "'";

			Cursor cursor = database.rawQuery(sql, null);

			if (cursor.moveToFirst()) {
				int dktId = cursor
						.getColumnIndex(DataHelper.SCANNED_COLUMN_DOCKET_NO);
				int hunId = cursor
						.getColumnIndex(DataHelper.SCANNED_COLUMN_HUN_NUM);
				int pktId = cursor
						.getColumnIndex(DataHelper.SCANNED_COLUMN_PKT_NUM);

				int hunScanId = cursor
						.getColumnIndex(DataHelper.SCANNED_COLUMN_HUN_SCANNED);
				int pktScanId = cursor
						.getColumnIndex(DataHelper.SCANNED_COLUMN_PKT_SCANNED);
				int hunTimeId = cursor
						.getColumnIndex(DataHelper.SCANNED_COLUMN_HUN_SCANTIME);
				int pktTimeId = cursor
						.getColumnIndex(DataHelper.SCANNED_COLUMN_PKT_SCANTIME);

				do {

					ScanDocket docket = new ScanDocket();

					String dktNum = cursor.getString(dktId);
					if (Utils.isValidString(dktNum))
						docket.setDocketNo(dktNum);

					String hunNum = cursor.getString(hunId);
					if (Utils.isValidString(hunNum))
						docket.setHunNo(hunNum);

					String pktNum = cursor.getString(pktId);
					if (Utils.isValidString(pktNum))
						docket.setPktNo(pktNum);

					String hunScanned = cursor.getString(hunScanId);
					if (Utils.isValidString(hunScanned)
							&& hunScanned.equalsIgnoreCase(Constants.TRUE))
						docket.setHunScanned(true);
					else
						docket.setHunScanned(false);

					String pktScanned = cursor.getString(pktScanId);
					if (Utils.isValidString(pktScanned)
							&& pktScanned.equalsIgnoreCase(Constants.TRUE))
						docket.setPktScanned(true);
					else
						docket.setPktScanned(false);

					String hunTime = cursor.getString(hunTimeId);
					if (Utils.isValidString(hunTime))
						docket.setHunScanTime(hunTime);

					String pktTime = cursor.getString(pktTimeId);
					if (Utils.isValidString(pktTime))
						docket.setPktScanTime(pktTime);

					dockets.add(docket);

				} while (cursor.moveToNext());
			}

			cursor.close();
			close();
			return dockets;
		}

		public ArrayList<ScannedDocketsDataModel> getScannedDocketsList(
				String custCode, String custVentCode) {
			ArrayList<ScannedDocketsDataModel> dockets = new ArrayList<ScannedDocketsDataModel>();

			openRead();

			String sql = "SELECT * FROM " + DataHelper.SCANNED_TABLE_NAME
					+ " WHERE " + DataHelper.SCANNED_COLUMN_CUST_CODE + " = '"
					+ custCode + "' AND "
					+ DataHelper.SCANNED_COLUMN_CUST_VENTCODE + " = '"
					+ custVentCode + "' AND "
					+ DataHelper.SCANNED_COLUMN_HUN_SCANNED + " = '"
					+ Constants.TRUE + "' AND "
					+ DataHelper.SCANNED_COLUMN_PKT_SCANNED + " = '"
					+ Constants.TRUE + "'";

			Cursor cursor = database.rawQuery(sql, null);

			if (cursor.moveToFirst()) {
				int dktId = cursor
						.getColumnIndex(DataHelper.SCANNED_COLUMN_DOCKET_NO);
				int hunId = cursor
						.getColumnIndex(DataHelper.SCANNED_COLUMN_HUN_NUM);
				int pktId = cursor
						.getColumnIndex(DataHelper.SCANNED_COLUMN_PKT_NUM);

				int hunTimeId = cursor
						.getColumnIndex(DataHelper.SCANNED_COLUMN_HUN_SCANTIME);
				int pktTimeId = cursor
						.getColumnIndex(DataHelper.SCANNED_COLUMN_PKT_SCANTIME);

				do {

					ScannedDocketsDataModel docket = new ScannedDocketsDataModel();

					String dktNum = cursor.getString(dktId);
					if (Utils.isValidString(dktNum))
						docket.setDocketNo(dktNum);

					DktHunPkgMappingModel m = new DktHunPkgMappingModel();

					String hunNum = cursor.getString(hunId);
					if (Utils.isValidString(hunNum))
						m.setHunBarCode(hunNum);

					String pktNum = cursor.getString(pktId);
					if (Utils.isValidString(pktNum))
						m.setPkgNo(pktNum);

					String hunTime = cursor.getString(hunTimeId);
					if (Utils.isValidString(hunTime))
						m.setScannedTime(hunTime);

					List<DktHunPkgMappingModel> list = new ArrayList<DktHunPkgMappingModel>();
					list.add(m);

					docket.setDktHunPkgMappings(list);

					dockets.add(docket);

				} while (cursor.moveToNext());
			}

			cursor.close();
			close();
			return dockets;
		}

		public boolean isHunScanned(String custCode, String custVentCode,
				String hun) {
			boolean isScanned = false;
			openRead();

			String sql = "SELECT " + DataHelper.SCANNED_COLUMN_HUN_SCANNED
					+ " FROM " + DataHelper.SCANNED_TABLE_NAME + " WHERE "
					+ DataHelper.SCANNED_COLUMN_CUST_CODE + " = '" + custCode
					+ "' AND " + DataHelper.SCANNED_COLUMN_CUST_VENTCODE
					+ " = '" + custVentCode + "' AND "
					+ DataHelper.SCANNED_COLUMN_HUN_NUM + " = '" + hun + "'";

			Cursor cursor = database.rawQuery(sql, null);
			if (cursor.moveToFirst()) {
				String scanned = cursor.getString(0);
				if (Utils.isValidString(scanned)
						&& scanned.equalsIgnoreCase(Constants.TRUE))
					isScanned = true;
			}

			cursor.close();
			close();

			return isScanned;
		}

		public boolean isPktScanned(String custCode, String custVentCode,
				String pkt) {
			boolean isScanned = false;
			openRead();

			String sql = "SELECT " + DataHelper.SCANNED_COLUMN_PKT_SCANNED
					+ " FROM " + DataHelper.SCANNED_TABLE_NAME + " WHERE "
					+ DataHelper.SCANNED_COLUMN_CUST_CODE + " = '" + custCode
					+ "' AND " + DataHelper.SCANNED_COLUMN_CUST_VENTCODE
					+ " = '" + custVentCode + "' AND "
					+ DataHelper.SCANNED_COLUMN_PKT_NUM + " = '" + pkt + "'";

			Cursor cursor = database.rawQuery(sql, null);
			if (cursor.moveToFirst()) {
				String scanned = cursor.getString(0);
				if (Utils.isValidString(scanned)
						&& scanned.equalsIgnoreCase(Constants.TRUE))
					isScanned = true;
			}

			cursor.close();
			close();

			return isScanned;
		}

		public int getDktsSize(String custCode, String custVentCode) {
			openRead();
			String sql = "SELECT DISTINCT "
					+ DataHelper.SCANNED_COLUMN_DOCKET_NO + " FROM "
					+ DataHelper.SCANNED_TABLE_NAME + " WHERE "
					+ DataHelper.SCANNED_COLUMN_CUST_CODE + " = '" + custCode
					+ "' AND " + DataHelper.SCANNED_COLUMN_CUST_VENTCODE
					+ " = '" + custVentCode + "'";
			Cursor cursor = database.rawQuery(sql, null);
			int count = cursor.getCount();
			cursor.close();
			close();
			return count;
		}

		public int getHunsSize(String custCode, String custVentCode) {
			openRead();
			String sql = "SELECT DISTINCT " + DataHelper.SCANNED_COLUMN_HUN_NUM
					+ " FROM " + DataHelper.SCANNED_TABLE_NAME + " WHERE "
					+ DataHelper.SCANNED_COLUMN_CUST_CODE + " = '" + custCode
					+ "' AND " + DataHelper.SCANNED_COLUMN_CUST_VENTCODE
					+ " = '" + custVentCode + "' AND "
					+ DataHelper.SCANNED_COLUMN_HUN_SCANNED + " = '"
					+ Constants.TRUE + "'";
			Cursor cursor = database.rawQuery(sql, null);
			int count = cursor.getCount();
			cursor.close();
			close();
			return count;
		}

		public int getPktsSize(String custCode, String custVentCode) {
			openRead();
			String sql = "SELECT DISTINCT " + DataHelper.SCANNED_COLUMN_PKT_NUM
					+ " FROM " + DataHelper.SCANNED_TABLE_NAME + " WHERE "
					+ DataHelper.SCANNED_COLUMN_CUST_CODE + " = '" + custCode
					+ "' AND " + DataHelper.SCANNED_COLUMN_CUST_VENTCODE
					+ " = '" + custVentCode + "' AND "
					+ DataHelper.SCANNED_COLUMN_PKT_SCANNED + " = '"
					+ Constants.TRUE + "'";
			Cursor cursor = database.rawQuery(sql, null);
			int count = cursor.getCount();
			cursor.close();
			close();
			return count;
		}

	}

}
