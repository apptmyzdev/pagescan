package com.revamobile.gatiscan.datacache;

import java.util.Date;

import android.content.Context;
import com.revamobile.gatiscan.data.DocketInfo;
import com.revamobile.gatiscan.utils.Utils;

public class SaveDocketInfo {
	private DocketInfo docketInfo;
	public Date timestamp;

	public SaveDocketInfo() {
		super();
	}

	public SaveDocketInfo(DocketInfo docketInfo, Date timestamp) {
		super();
		this.docketInfo = docketInfo;
		this.timestamp = timestamp;
	}

	public DocketInfo getDocketInfo() {
		return docketInfo;
	}

	public void setDocketInfo(DocketInfo docketInfo) {
		this.docketInfo = docketInfo;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public static void saveInHistory(Context context, DocketInfo docketInfo) {
		try {
			if (docketInfo != null) {
				DataSource dataSource = new DataSource(context);
				if (dataSource != null) {
					dataSource.saveData.saveDocketInfo(docketInfo);
				}
			}
		} catch (Exception e) {
			Utils.logE("saveInHistory Err : " + e.toString());
		}
	}

	@Override
	public String toString() {
		return "SaveDocketInfo [docketInfo=" + docketInfo + ", timestamp="
				+ timestamp + "]";
	}
}
