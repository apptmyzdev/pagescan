package com.revamobile.gatiscan.datacache;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.revamobile.gatiscan.utils.Utils;

public class DataHelper extends SQLiteOpenHelper {

	public final static int DATABASE_VERSION = 3;
	public final static String DATABASE_NAME = "gati_scan";

	/** ************** SHARED_PREFERENCES ************** **/
	public static final String SHARED_PREF_TABLE_NAME = "sharedpreferences";
	public final static String SHARED_PREF_KEY_ID = "SHARED_PREF_KEY_ID";
	public static final String SHARED_PREF_COLUMN_KEY = "KEY";
	public static final String SHARED_PREF_COLUMN_VALUE = "VALUE";

	public static final String SHARED_PREF_DATABASE_CREATE = "create table "
			+ SHARED_PREF_TABLE_NAME + "(" + SHARED_PREF_KEY_ID
			+ " INTEGER PRIMARY KEY AUTOINCREMENT," + SHARED_PREF_COLUMN_KEY
			+ " text not null, " + SHARED_PREF_COLUMN_VALUE
			+ " text not null);";

	public static final String[] SHARED_PREF_COLUMNS = {
			SHARED_PREF_COLUMN_KEY, SHARED_PREF_COLUMN_VALUE };

	// ************** PRINT LIST **************//
	public final static String PRINT_LIST_TABLE_NAME = "print_list";

	public final static String PRINT_LIST_COLUMN_KEY_ID = "PRINT_LIST_KEY_ID";
	public final static String PRINT_LIST_COLUMN_DOCKET_NO = "DOCKET_NO";
	public final static String PRINT_LIST_COLUMN_DLYSTN = "DLYSTN";
	public final static String PRINT_LIST_COLUMN_ASSURED_DLY_DT = "ASSURED_DLY_DT";
	public final static String PRINT_LIST_COLUMN_NO_OF_PKGS = "NO_OF_PKGS";
	public final static String PRINT_LIST_COLUMN_SPKGNO = "SPKGNO";
	public final static String PRINT_LIST_COLUMN_SRVCODE = "SRVCODE";
	public final static String PRINT_LIST_COLUMN_PARCEL = "PARCEL";
	public final static String PRINT_LIST_COLUMN_RECEIVER_NAME = "RECEIVER_NAME";

	public final static String PRINT_LIST_COLUMN_TIME_STAMP = "TIMESTAMP";

	public final static String PRINT_LIST_DATABASE_CREATE = "create table "
			+ PRINT_LIST_TABLE_NAME + "(" + PRINT_LIST_COLUMN_KEY_ID
			+ " INTEGER PRIMARY KEY AUTOINCREMENT,"
			+ PRINT_LIST_COLUMN_DOCKET_NO + " TEXT," + PRINT_LIST_COLUMN_DLYSTN
			+ " TEXT," + PRINT_LIST_COLUMN_ASSURED_DLY_DT + " TEXT,"
			+ PRINT_LIST_COLUMN_NO_OF_PKGS + " INTEGER,"
			+ PRINT_LIST_COLUMN_SPKGNO + " INTEGER,"
			+ PRINT_LIST_COLUMN_SRVCODE + " TEXT," + PRINT_LIST_COLUMN_PARCEL
			+ " TEXT," + PRINT_LIST_COLUMN_RECEIVER_NAME + " TEXT,"
			+ PRINT_LIST_COLUMN_TIME_STAMP + " LONG )";

	public static final String[] PRINT_LIST_COLUMNS = {
			PRINT_LIST_COLUMN_KEY_ID, PRINT_LIST_COLUMN_DOCKET_NO,
			PRINT_LIST_COLUMN_DLYSTN, PRINT_LIST_COLUMN_ASSURED_DLY_DT,
			PRINT_LIST_COLUMN_NO_OF_PKGS, PRINT_LIST_COLUMN_SPKGNO,
			PRINT_LIST_COLUMN_SRVCODE, PRINT_LIST_COLUMN_PARCEL,
			PRINT_LIST_COLUMN_RECEIVER_NAME, PRINT_LIST_COLUMN_TIME_STAMP };

	// ************** DOCKETS DATA **************//
	public final static String DOCKETS_TABLE_NAME = "dockets";

	public final static String DOCKETS_COLUMN_KEY_ID = "DOCKETS_COLUMN_KEY_ID";

	public final static String DOCKETS_COLUMN_CUST_CODE = "DOCKETS_COLUMN_CUST_CODE";
	public final static String DOCKETS_COLUMN_CUST_VENTCODE = "DOCKETS_COLUMN_CUST_VENTCODE";

	public final static String DOCKETS_COLUMN_DOCKET_NO = "DOCKETS_COLUMN_DOCKET_NO";
	public final static String DOCKETS_COLUMN_HUN_NUM = "DOCKETS_COLUMN_HUN_NUM";
	public final static String DOCKETS_COLUMN_PKT_NUM = "DOCKETS_COLUMN_PKT_NUM";

	public final static String DOCKETS_DATABASE_CREATE = "create table "
			+ DOCKETS_TABLE_NAME + "(" + DOCKETS_COLUMN_KEY_ID
			+ " INTEGER PRIMARY KEY AUTOINCREMENT," + DOCKETS_COLUMN_CUST_CODE
			+ " TEXT," + DOCKETS_COLUMN_CUST_VENTCODE + " TEXT,"
			+ DOCKETS_COLUMN_DOCKET_NO + " TEXT," + DOCKETS_COLUMN_HUN_NUM
			+ " TEXT," + DOCKETS_COLUMN_PKT_NUM + " TEXT )";

	public static final String[] DOCKETS_COLUMNS = { DOCKETS_COLUMN_KEY_ID,
			DOCKETS_COLUMN_CUST_CODE, DOCKETS_COLUMN_CUST_VENTCODE,
			DOCKETS_COLUMN_DOCKET_NO, DOCKETS_COLUMN_HUN_NUM,
			DOCKETS_COLUMN_PKT_NUM };

	// ************** SCANNED DATA **************//
	public final static String SCANNED_TABLE_NAME = "scanned_list";

	public final static String SCANNED_COLUMN_KEY_ID = "SCANNED_COLUMN_KEY_ID";

	public final static String SCANNED_COLUMN_CUST_CODE = "SCANNED_COLUMN_CUST_CODE";
	public final static String SCANNED_COLUMN_CUST_VENTCODE = "SCANNED_COLUMN_CUST_VENTCODE";

	public final static String SCANNED_COLUMN_DOCKET_NO = "SCANNED_COLUMN_DOCKET_NO";
	public final static String SCANNED_COLUMN_HUN_NUM = "SCANNED_COLUMN_HUN_NUM";
	public final static String SCANNED_COLUMN_PKT_NUM = "SCANNED_COLUMN_PKT_NUM";
	public final static String SCANNED_COLUMN_HUN_SCANNED = "SCANNED_COLUMN_HUN_SCANNED";
	public final static String SCANNED_COLUMN_PKT_SCANNED = "SCANNED_COLUMN_PKT_SCANNED";
	public final static String SCANNED_COLUMN_HUN_SCANTIME = "SCANNED_COLUMN_HUN_SCANTIME";
	public final static String SCANNED_COLUMN_PKT_SCANTIME = "SCANNED_COLUMN_PKT_SCANTIME";

	public final static String SCANNED_DATABASE_CREATE = "create table "
			+ SCANNED_TABLE_NAME + "(" + SCANNED_COLUMN_KEY_ID
			+ " INTEGER PRIMARY KEY AUTOINCREMENT," + SCANNED_COLUMN_CUST_CODE
			+ " TEXT," + SCANNED_COLUMN_CUST_VENTCODE + " TEXT,"
			+ SCANNED_COLUMN_DOCKET_NO + " TEXT," + SCANNED_COLUMN_HUN_NUM
			+ " TEXT," + SCANNED_COLUMN_PKT_NUM + " TEXT,"
			+ SCANNED_COLUMN_HUN_SCANNED + " TEXT,"
			+ SCANNED_COLUMN_PKT_SCANNED + " TEXT,"
			+ SCANNED_COLUMN_HUN_SCANTIME + " TEXT,"
			+ SCANNED_COLUMN_PKT_SCANTIME + " TEXT )";

	public static final String[] SCANNED_COLUMNS = { SCANNED_COLUMN_KEY_ID,
			SCANNED_COLUMN_CUST_CODE, SCANNED_COLUMN_CUST_VENTCODE,
			SCANNED_COLUMN_DOCKET_NO, SCANNED_COLUMN_HUN_NUM,
			SCANNED_COLUMN_PKT_NUM, SCANNED_COLUMN_HUN_SCANNED,
			SCANNED_COLUMN_PKT_SCANNED, SCANNED_COLUMN_HUN_SCANTIME,
			SCANNED_COLUMN_PKT_SCANTIME };

	public DataHelper(Context context) {
		super(context, DataHelper.DATABASE_NAME, null,
				DataHelper.DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase database) {

		database.execSQL(DataHelper.SHARED_PREF_DATABASE_CREATE);
		database.execSQL(DataHelper.PRINT_LIST_DATABASE_CREATE);
		database.execSQL(DataHelper.DOCKETS_DATABASE_CREATE);
		database.execSQL(DataHelper.SCANNED_DATABASE_CREATE);

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Utils.logI("Upgrading database from version " + oldVersion + " to "
				+ newVersion + ", which will destroy all old data");

		db.execSQL("DROP TABLE IF EXISTS " + DataHelper.SHARED_PREF_TABLE_NAME);
		db.execSQL("DROP TABLE IF EXISTS " + DataHelper.PRINT_LIST_TABLE_NAME);
		db.execSQL("DROP TABLE IF EXISTS " + DataHelper.DOCKETS_TABLE_NAME);
		db.execSQL("DROP TABLE IF EXISTS " + DataHelper.SCANNED_TABLE_NAME);
		onCreate(db);
	}

}
