package com.revamobile.gatiscan.data;

public class ScanDocket {
	private String docketNo;
	private String hunNo;
	private String pktNo;
	private boolean hunScanned;
	private boolean pktScanned;
	private String hunScanTime;
	private String pktScanTime;

	public ScanDocket() {
		super();
	}

	public ScanDocket(String docketNo, String hunNo, String pktNo) {
		super();
		this.docketNo = docketNo;
		this.hunNo = hunNo;
		this.pktNo = pktNo;
	}

	public ScanDocket(String docketNo, String hunNo, boolean hunScanned,
			boolean pktScanned) {
		super();
		this.docketNo = docketNo;
		this.hunNo = hunNo;
		this.hunScanned = hunScanned;
		this.pktScanned = pktScanned;
	}

	public ScanDocket(String docketNo, String hunNo, String pktNo,
			boolean hunScanned, boolean pktScanned, String hunScanTime,
			String pktScanTime) {
		super();
		this.docketNo = docketNo;
		this.hunNo = hunNo;
		this.pktNo = pktNo;
		this.hunScanned = hunScanned;
		this.pktScanned = pktScanned;
		this.hunScanTime = hunScanTime;
		this.pktScanTime = pktScanTime;
	}

	public String getDocketNo() {
		return docketNo;
	}

	public void setDocketNo(String docketNo) {
		this.docketNo = docketNo;
	}

	public String getHunNo() {
		return hunNo;
	}

	public void setHunNo(String hunNo) {
		this.hunNo = hunNo;
	}

	public String getPktNo() {
		return pktNo;
	}

	public void setPktNo(String pktNo) {
		this.pktNo = pktNo;
	}

	public boolean isHunScanned() {
		return hunScanned;
	}

	public void setHunScanned(boolean hunScanned) {
		this.hunScanned = hunScanned;
	}

	public boolean isPktScanned() {
		return pktScanned;
	}

	public void setPktScanned(boolean pktScanned) {
		this.pktScanned = pktScanned;
	}

	public String getHunScanTime() {
		return hunScanTime;
	}

	public void setHunScanTime(String hunScanTime) {
		this.hunScanTime = hunScanTime;
	}

	public String getPktScanTime() {
		return pktScanTime;
	}

	public void setPktScanTime(String pktScanTime) {
		this.pktScanTime = pktScanTime;
	}

	@Override
	public String toString() {
		return "ScanDocket [docketNo=" + docketNo + ", hunNo=" + hunNo
				+ ", pktNo=" + pktNo + ", hunScanned=" + hunScanned
				+ ", pktScanned=" + pktScanned + ", hunScanTime=" + hunScanTime
				+ ", pktScanTime=" + pktScanTime + "]";
	}

}
