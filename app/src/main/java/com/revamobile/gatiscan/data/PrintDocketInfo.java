package com.revamobile.gatiscan.data;

public class PrintDocketInfo {
	
	private DocketInfo docketInfo;
	private int from;
	private int to;

	public PrintDocketInfo() {
		super();
	}

	public PrintDocketInfo(DocketInfo docketInfo) {
		super();
		this.docketInfo = docketInfo;
		this.from = 0;
		this.to = docketInfo.getNo_of_pkgs();
	}

	public DocketInfo getDocketInfo() {
		return docketInfo;
	}

	public void setDocketInfo(DocketInfo docketInfo) {
		this.docketInfo = docketInfo;
	}

	public int getFrom() {
		return from;
	}

	public void setFrom(int from) {
		this.from = from;
	}

	public int getTo() {
		return to;
	}

	public void setTo(int to) {
		this.to = to;
	}

	@Override
	public String toString() {
		return "PrintDocketInfo [docketInfo=" + docketInfo + ", from=" + from
				+ ", to=" + to + "]";
	}

}
