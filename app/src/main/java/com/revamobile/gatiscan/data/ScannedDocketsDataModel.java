package com.revamobile.gatiscan.data;

import java.util.List;

public class ScannedDocketsDataModel {

	private String docketNo;
	private List<DktHunPkgMappingModel> dktHunPkgMappings;

	public ScannedDocketsDataModel() {
		super();
	}

	public ScannedDocketsDataModel(String docketNo,
			List<DktHunPkgMappingModel> dktHunPkgMappings) {
		super();
		this.docketNo = docketNo;
		this.dktHunPkgMappings = dktHunPkgMappings;
	}

	public String getDocketNo() {
		return docketNo;
	}

	public void setDocketNo(String docketNo) {
		this.docketNo = docketNo;
	}

	public List<DktHunPkgMappingModel> getDktHunPkgMappings() {
		return dktHunPkgMappings;
	}

	public void setDktHunPkgMappings(
			List<DktHunPkgMappingModel> dktHunPkgMappings) {
		this.dktHunPkgMappings = dktHunPkgMappings;
	}

	@Override
	public String toString() {
		return "ScannedDocketsDataModel [docketNo=" + docketNo
				+ ", dktHunPkgMappings=" + dktHunPkgMappings + "]";
	}

}
