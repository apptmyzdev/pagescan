package com.revamobile.gatiscan.data;

public class DocketInfo {
	String docketno;
	String dlystn;
	String assured_dly_dt;
	int no_of_pkgs;
	int spkgno;
	String srvcode;
	String parcel;
	String receiver_name;

	public DocketInfo() {
		super();
	}

	public DocketInfo(String docketno, String dlystn, String assured_dly_dt,
			int no_of_pkgs, int spkgno, String srvcode, String parcel,
			String receiver_name) {
		super();
		this.docketno = docketno;
		this.dlystn = dlystn;
		this.assured_dly_dt = assured_dly_dt;
		this.no_of_pkgs = no_of_pkgs;
		this.spkgno = spkgno;
		this.srvcode = srvcode;
		this.parcel = parcel;
		this.receiver_name = receiver_name;
	}

	public String getDocketno() {
		return docketno;
	}

	public void setDocketno(String docketno) {
		this.docketno = docketno;
	}

	public String getDlystn() {
		return dlystn;
	}

	public void setDlystn(String dlystn) {
		this.dlystn = dlystn;
	}

	public String getAssured_dly_dt() {
		return assured_dly_dt;
	}

	public void setAssured_dly_dt(String assured_dly_dt) {
		this.assured_dly_dt = assured_dly_dt;
	}

	public int getNo_of_pkgs() {
		return no_of_pkgs;
	}

	public void setNo_of_pkgs(int no_of_pkgs) {
		this.no_of_pkgs = no_of_pkgs;
	}

	public int getSpkgno() {
		return spkgno;
	}

	public void setSpkgno(int spkgno) {
		this.spkgno = spkgno;
	}

	public String getSrvcode() {
		return srvcode;
	}

	public void setSrvcode(String srvcode) {
		this.srvcode = srvcode;
	}

	public String getParcel() {
		return parcel;
	}

	public void setParcel(String parcel) {
		this.parcel = parcel;
	}

	public String getReceiver_name() {
		return receiver_name;
	}

	public void setReceiver_name(String receiver_name) {
		this.receiver_name = receiver_name;
	}

	@Override
	public String toString() {
		return "DocketInfo [docketno=" + docketno + ", dlystn=" + dlystn
				+ ", assured_dly_dt=" + assured_dly_dt + ", no_of_pkgs="
				+ no_of_pkgs + ", spkgno=" + spkgno + ", srvcode=" + srvcode
				+ ", parcel=" + parcel + ", receiver_name=" + receiver_name
				+ "]";
	}

}
