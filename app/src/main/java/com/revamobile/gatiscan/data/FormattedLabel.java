package com.revamobile.gatiscan.data;

import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog;
import android.content.DialogInterface;

import com.revamobile.gatiscan.datacache.DataSource;
import com.revamobile.gatiscan.utils.Constants;
import com.revamobile.gatiscan.utils.Globals;
import com.revamobile.pagescan.OnLineScreen;
import com.revamobile.pagescan.R;

public class FormattedLabel {

	public static List<String> getFormattedLabel(OnLineScreen main, DocketInfo d) {
		List<String> labels = new ArrayList<String>();
		checkWhichPriterSelected(main);
		if (Globals.selectedPrinterPos > -1 && Globals.selectedPrinterPos < 3) {
			switch (Globals.selectedPrinterPos) {
			case 0:
				labels = getArgoxFormattedLabel(d);
				break;
			case 1:
				labels = getMaestorsFormattedLabel(d);
				break;
			case 2:
				labels = getZebraFormattedLabel(d);
				break;
			default:

				break;
			}
		} else {
			showPrinterOption(main, d);
		}

		return labels;
	}

	private static void checkWhichPriterSelected(final OnLineScreen main) {
		Globals.selectedPrinterPos = -1;
		DataSource dataSource = new DataSource((OnLineScreen.mContext));
		String lastPrinter = dataSource.shardPreferences
				.getValue(Constants.PRINTER_TYPE);
		if (lastPrinter != null && lastPrinter.length() > 0) {
			for (int i = 0; i < Globals.printerArr.length; i++) {
				if (lastPrinter.equalsIgnoreCase(Globals.printerArr[i])) {
					Globals.selectedPrinterPos = i;
					break;
				}
			}
		}
	}

	public static void showPrinterOption(final OnLineScreen main,
			final DocketInfo d) {

		final DataSource dataSource = new DataSource((OnLineScreen.mContext));
		String lastPrinter = dataSource.shardPreferences
				.getValue(Constants.PRINTER_TYPE);
		if (lastPrinter != null && lastPrinter.length() > 0) {
			for (int i = 0; i < Globals.printerArr.length; i++) {
				if (lastPrinter.equalsIgnoreCase(Globals.printerArr[i])) {
					Globals.selectedPrinterPos = i;
					break;
				}
			}
		}

		AlertDialog.Builder builder = new AlertDialog.Builder(
				OnLineScreen.mContext);
		builder.setTitle(OnLineScreen.mContext.getResources().getString(
				R.string.whichPrinterTitel));
		builder.setCancelable(false);
		builder.setSingleChoiceItems(Globals.printerArr,
				Globals.selectedPrinterPos,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int pos) {
						Globals.selectedPrinterPos = pos;
						dataSource.shardPreferences.set(Constants.PRINTER_TYPE,
								Globals.printerArr[Globals.selectedPrinterPos]);

						dialog.dismiss();
						getFormattedLabel(main, d);
					}
				});

		builder.show();

	}

	private static List<String> getArgoxFormattedLabel(DocketInfo d) {
		int numOfPackages = d.getNo_of_pkgs();
		List<String> labels = new ArrayList<String>();
		int packageFrom = d.getSpkgno();
		String label = "";
		for (int i = 0; i < numOfPackages; i++) {

			label = "<xpml><page quantity='0' pitch='76.2 mm'></xpml>^XA";
			label += "^MCY^PMN";
			label += "^PW576";
			label += "^MNM";
			label += "KI80";
			label += "~JSN^MMT";
			label += "^JZY";
			label += "^LH0,0^LRN";
			label += "^XZ";
			label += "<xpml></page></xpml><xpml><page quantity='1' pitch='76.2 mm'></xpml>~DGR:SSGFX000.GRF,11800,25,K08,,K08,:J014,K0A,J01580,J012,J014,K0A40,I011580,K0A50,I0109AK01540,J04A54I05DB68,I010592I0B36D4,J085AA0036ED40,I0I25480EDDA,J08255415AF45FA8,I0I19527B72176FAI04954AA800188J0I10221140440J041AI56AC5BBD4002B6D4ADI0255B0012AAB556A96AACI011054B6DD1FDFA0016AAB4D5I01IA801D6D56D950AD68:J044556DAADAEEI0B56DAF5AI026D6805355IA9B05B50J09012DB753FB8006AADI056I025B58072AB6D56A0AAD0J02255B6D7F6F00196BK04I066AB40800D550040DB50J0440B6D6D3FA00575CO0A55A8J0AB5J0IA0K0856D6DFEDC00AEDO015AD54J0DADI015B60J02183BB9B3700156A8N01795B4I0156AI01IA0K0A1556A7FE002D96O03416ACI016AAI016D40J01020ED55EC00556CO056156AI02ADAI02AB40K02455AA938005AE8O0A80AD4I02D54I035AC0K0408354AFI0D694N01580D5AI055B4I02D680K01A24A552I0B598I0532802D00AB5I02D54I06AA80K04091IAC001569J0ACD402A00B56I06B68I056D,K0DA23AA52001B568I054B806C0056AI0I5J06AB,J03A086D29100156E8I06B500AA456ADI0B6DJ0ADA,J05651FF5528016D68I05BA815B5A56A800D55I01I5,J06DC2DBAAD401AAE8I0AB681IA65ADI0ADBJ0DAD,I01DBB37DD4D4015568I0D7503I58I5801554I0156A,I02B66FIE51500B69AI0B4D056I036D001B6AI02B54,I0B6DDBB7AAD200AD94001ACA0A8I02AB40155AI01AB4,0015DA5EFB82D500557500154C168I02DA8036AAI036D4,0056ADB76EA92A802B6B512B4A2D8I02AB402AD4I02AAC,00BDD9BDFE0025400A9IADACA2AJ02IA02AB4I05568,:076B7277B8AA2AI02EADAA6BCD68I01DD60F7ACI07758,AAD683BEEA00154I01755BDB011K021100822J0844,3DBD0FDB60550A8K05520,02A03DFF81I0540,J0F7AE004AC5,I0BDDF800200140,003B6FDI012D280,I0FFD8J080140,J04L04D1,Q020080,R02880,Q010080,R0D0,,R028,,R010,,R010,,:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::hO06:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::";
			label += "^XA";
			label += "^FO244,40";
			label += "^BY3^BCN,106,N,N,,A^FD" + (packageFrom + i) + "^FS";
			label += "^FT323,173";
			label += "^CI0";
			label += "^AFN,26,13^FD" + (packageFrom + i) + "^FS";
			label += "^FO33,191";
			label += "^GB427,0,2^FS";
			label += "^FT44,260";
			label += "^A0N,62,59^FD" + d.getSrvcode() + "^FS";
			label += "^FO34,287";
			label += "^GB425,0,2^FS";
			label += "^FO219,192";
			label += "^GB0,96,2^FS";
			label += "^FO476,209";
			label += "^BCB,64,N,N,,A^FD" + (packageFrom + i) + "^FS";
			label += "^FT567,432";
			label += "^AFB,26,13^FD" + (packageFrom + i) + "^FS";
			label += "^FT228,216";
			label += "^A0N,28,30^FDTO:^FS";
			label += "^FT36,314";
			label += "^A0N,20,28^FDDktNo:^FS";
			label += "^FO259,288";
			label += "^GB0,128,2^FS";
			label += "^FO26,415";
			label += "^GB433,0,2^FS";
			label += "^FT260,314";
			label += "^A0N,20,28^FDSeq./No.of Pkgs:^FS";
			label += "^FT28,441";
			// label += "^A0N,20,28^FDAdd:^FS";
			label += "^A0N,20,28^FD" + d.getReceiver_name() + "^FS";
			label += "^FT36,514";
			label += "^A0N,79,40^FD" + d.getAssured_dly_dt() + "^FS";
			label += "^FT324,499";

			if (d.getParcel() == null || d.getParcel().length() == 0)
				d.setParcel("A");
			label += "^A0N,70,88^FD" + d.getParcel() + "^FS";
			label += "^FT268,380";
			label += "^A0N,62,45^FD" + (i + 1) + "/" + d.getNo_of_pkgs()
					+ "^FS";
			label += "^FT28,388";
			label += "^A0N,70,42^FD" + d.getDocketno() + "^FS";
			label += "^FT268,260";
			label += "^A0N,62,59^FD" + d.getDlystn() + "^FS";
			label += "^FO30,58";
			label += "^XGR:SSGFX000.GRF,1,1^FS";
			label += "^PQ1,0,1,Y";
			label += "^XZ";
			label += "<xpml></page></xpml>^XA";
			label += "^IDR:SSGFX000.GRF^XZ";
			label += "<xpml><end/></xpml>";
			labels.add(label);

		}
		return labels;
	}

	private static List<String> getMaestorsFormattedLabel(DocketInfo d) {
		int numOfPackages = d.getNo_of_pkgs();
		int packageFrom = d.getSpkgno();
		List<String> labels = new ArrayList<String>();
		String label = "";
		for (int i = 0; i < numOfPackages; i++) {
			label = "! 0 200 200 525 1\r\nON-OUT-OF-PAPER WAIT 400\r\nPW 551\r\n";
			label += "TONE 0\r\nSPEED 5\r\nON-FEED IGNORE\r\nNO-PACE\r\n";
			label += "LABEL\r\nCONTRAST 0\r\nPCX 2 29 !<glogo1.pcx";
			label += "BAR-SENSE\r\nLINE 2 176 440 176 3\r\nLINE 2 382 440 382 3\r\nLINE 2 267 440 267 3\r\n";
			label += "BT 0 3 8\r\nB 128 2 0 100 235 42 " + (packageFrom + i)
					+ "\r\nBT OFF\r\n";
			label += "LINE 219 176 219 266 3\r\nT 4 1 26 179 " + d.getSrvcode()
					+ "\r\n";
			label += "T 5 0 228 181 TO:\r\nT 4 1 279 179 " + d.getDlystn()
					+ "\r\n";
			label += "BT 0 3 5\r\nVB 128 2 0 70 448 478 " + (packageFrom + i)
					+ "\r\nBT OFF\r\n";
			label += "T 5 0 4 274 DktNo:\r\nT 4 1 12 294 " + d.getDocketno()
					+ "\r\n";
			label += "T 5 0 242 274 Seq./No.of Pkgs:\r\nT 4 1 250 294 "
					+ (i + 1) + "/" + d.getNo_of_pkgs() + "\r\n";
			label += "T 5 0 6 389 " + d.getReceiver_name()
					+ "\r\nT 4 1 12 402 "
					// "T 5 0 6 389 Add:\r\nT 4 1 12 402 "
					+ d.getAssured_dly_dt() + "\r\n";
			label += "LINE 236 268 236 382 3\r\nLINE 227 381 227 487 3\r\n";
			if (d.getParcel() == null || d.getParcel().length() == 0)
				d.setParcel("A");
			label += "T 5 0 234 389 Type:\r\nT 4 1 336 400 " + d.getParcel()
					+ "\r\n";
			label += "PRINT\r\n";
			labels.add(label);
		}
		return labels;

	}

	private static List<String> getZebraFormattedLabel(DocketInfo d) {
		int numOfPackages = d.getNo_of_pkgs();
		int packageFrom = d.getSpkgno();
		List<String> labels = new ArrayList<String>();
		String label = "";
		for (int i = 0; i < numOfPackages; i++) {
			label = "! 0 200 200 525 1\r\nON-OUT-OF-PAPER WAIT 400\r\nPW 551\r\n";
			label += "TONE 0\r\nSPEED 5\r\nON-FEED IGNORE\r\nNO-PACE\r\n";
			label += "LABEL\r\nCONTRAST 0\r\nPCX 2 29 !<glogo1.pcx";
			label += "BAR-SENSE\r\nLINE 2 176 440 176 3\r\nLINE 2 382 440 382 3\r\nLINE 2 267 440 267 3\r\n";
			label += "BT 0 3 8\r\nB 128 2 0 100 235 42 " + (packageFrom + i)
					+ "\r\nBT OFF\r\n";
			label += "LINE 219 176 219 266 3\r\nT 4 1 26 179 " + d.getSrvcode()
					+ "\r\n";
			label += "T 5 0 228 181 TO:\r\nT 4 1 279 179 " + d.getDlystn()
					+ "\r\n";
			label += "BT 0 3 5\r\nVB 128 2 0 70 448 478 " + (packageFrom + i)
					+ "\r\nBT OFF\r\n";
			label += "T 5 0 4 274 DktNo:\r\nT 4 1 12 294 " + d.getDocketno()
					+ "\r\n";
			label += "T 5 0 242 274 Seq./No.of Pkgs:\r\nT 4 1 250 294 "
					+ (i + 1) + "/" + d.getNo_of_pkgs() + "\r\n";
			label += "T 5 0 6 389 " + d.getReceiver_name()
					+ "\r\nT 4 1 12 402 " + d.getAssured_dly_dt() + "\r\n";
			label += "LINE 236 268 236 382 3\r\nLINE 227 381 227 487 3\r\n";
			if (d.getParcel() == null || d.getParcel().length() == 0)
				d.setParcel("A");
			label += "T 5 0 234 389 Type:\r\nT 4 1 336 400 " + d.getParcel()
					+ "\r\n";
			label += "PRINT\r\n";
			labels.add(label);
		}
		return labels;

	}

	public static byte[] getArgoxTestLabel() {

		String label = "<xpml><page quantity='0' pitch='76.2 mm'></xpml>^XA";
		label += "^MCY^PMN";
		label += "^PW576";
		label += "^MNM";
		label += "KI80";
		label += "~JSN^MMT";
		label += "^JZY";
		label += "^LH0,0^LRN";
		label += "^XZ";
		label += "<xpml></page></xpml><xpml><page quantity='1' pitch='76.2 mm'></xpml>~DGR:SSGFX000.GRF,11800,25,K08,,K08,:J014,K0A,J01580,J012,J014,K0A40,I011580,K0A50,I0109AK01540,J04A54I05DB68,I010592I0B36D4,J085AA0036ED40,I0I25480EDDA,J08255415AF45FA8,I0I19527B72176FAI04954AA800188J0I10221140440J041AI56AC5BBD4002B6D4ADI0255B0012AAB556A96AACI011054B6DD1FDFA0016AAB4D5I01IA801D6D56D950AD68:J044556DAADAEEI0B56DAF5AI026D6805355IA9B05B50J09012DB753FB8006AADI056I025B58072AB6D56A0AAD0J02255B6D7F6F00196BK04I066AB40800D550040DB50J0440B6D6D3FA00575CO0A55A8J0AB5J0IA0K0856D6DFEDC00AEDO015AD54J0DADI015B60J02183BB9B3700156A8N01795B4I0156AI01IA0K0A1556A7FE002D96O03416ACI016AAI016D40J01020ED55EC00556CO056156AI02ADAI02AB40K02455AA938005AE8O0A80AD4I02D54I035AC0K0408354AFI0D694N01580D5AI055B4I02D680K01A24A552I0B598I0532802D00AB5I02D54I06AA80K04091IAC001569J0ACD402A00B56I06B68I056D,K0DA23AA52001B568I054B806C0056AI0I5J06AB,J03A086D29100156E8I06B500AA456ADI0B6DJ0ADA,J05651FF5528016D68I05BA815B5A56A800D55I01I5,J06DC2DBAAD401AAE8I0AB681IA65ADI0ADBJ0DAD,I01DBB37DD4D4015568I0D7503I58I5801554I0156A,I02B66FIE51500B69AI0B4D056I036D001B6AI02B54,I0B6DDBB7AAD200AD94001ACA0A8I02AB40155AI01AB4,0015DA5EFB82D500557500154C168I02DA8036AAI036D4,0056ADB76EA92A802B6B512B4A2D8I02AB402AD4I02AAC,00BDD9BDFE0025400A9IADACA2AJ02IA02AB4I05568,:076B7277B8AA2AI02EADAA6BCD68I01DD60F7ACI07758,AAD683BEEA00154I01755BDB011K021100822J0844,3DBD0FDB60550A8K05520,02A03DFF81I0540,J0F7AE004AC5,I0BDDF800200140,003B6FDI012D280,I0FFD8J080140,J04L04D1,Q020080,R02880,Q010080,R0D0,,R028,,R010,,R010,,:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::hO06:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::";
		label += "^XA";
		label += "^FO244,40";
		label += "^BY3^BCN,106,N,N,,A^FD270833556^FS";
		label += "^FT323,173";
		label += "^CI0";
		label += "^AFN,26,13^FD270833556^FS";
		label += "^FO33,191";
		label += "^GB427,0,2^FS";
		label += "^FT44,260";
		label += "^A0N,62,59^FDSTD^FS";
		label += "^FO34,287";
		label += "^GB425,0,2^FS";
		label += "^FO219,192";
		label += "^GB0,96,2^FS";
		label += "^FO476,209";
		label += "^BCB,64,N,N,,A^FD270833556^FS";
		label += "^FT567,432";
		label += "^AFB,26,13^FD270833556^FS";
		label += "^FT228,216";
		label += "^A0N,28,30^FDTO:^FS";
		label += "^FT36,314";
		label += "^A0N,20,28^FDDktNo:^FS";
		label += "^FO259,288";
		label += "^GB0,128,2^FS";
		label += "^FO26,415";
		label += "^GB433,0,2^FS";
		label += "^FT260,314";
		label += "^A0N,20,28^FDSeq./No.of Pkgs:^FS";
		label += "^FT28,441";
		label += "^A0N,20,28^FDAdd:^FS";
		label += "^FT36,514";
		label += "^A0N,79,40^FD07/09/2012^FS";
		label += "^FT324,499";
		label += "^A0N,70,88^FDS^FS";
		label += "^FT268,380";
		label += "^A0N,62,45^FD1/3^FS";
		label += "^FT28,388";
		label += "^A0N,70,42^FD555555555^FS";
		label += "^FT268,260";
		label += "^A0N,62,59^FDGGGG^FS";
		label += "^FO30,58";
		label += "^XGR:SSGFX000.GRF,1,1^FS";
		label += "^PQ1,0,1,Y";
		label += "^XZ";
		label += "<xpml></page></xpml>^XA";
		label += "^IDR:SSGFX000.GRF^XZ";
		label += "<xpml><end/></xpml>";
		byte data[] = label.getBytes();
		return data;
	}

	public static byte[] getZebraTestLabel() {
		String label = "! 0 200 200 551 1\r\nON-OUT-OF-PAPER WAIT 400\r\nPW 551\r\nTONE 0\r\nSPEED 5\r\nON-FEED IGNORE\r\nNO-PACE\r\n";
		label += "LABEL\r\nCONTRAST 0\r\nPCX 2 29 !<glogo1.pcx";
		label += "BAR-SENSE\r\nLINE 2 176 440 176 3\r\nLINE 2 382 440 382 3\r\nLINE 2 267 440 267 3\r\n";
		label += "BT 0 3 8\r\nB 128 2 0 100 235 42 200007576\r\nBT OFF\r\n";
		label += "LINE 219 176 219 266 3\r\nT 4 1 26 179 EXP\r\nT 5 0 228 181 TO:\r\nT 4 1 279 179 BOMS\r\n";
		label += "BT 0 3 5\r\nVB 128 2 0 70 448 478 200007576\r\nBT OFF\r\nT 5 0 4 274 DktNo:\r\n";
		label += "T 4 1 12 294 545845855\r\nT 5 0 242 274 Seq./No.of Pkgs:\r\n";
		label += "T 4 1 250 294 888/888\r\nT 5 0 6 389 Add:\r\nT 4 1 12 402 25/12/2010\r\n";
		label += "LINE 236 268 236 382 3\r\nLINE 227 381 227 487 3\r\nT 5 0 234 389 Type:\r\nT 4 1 336 400 P\r\n";
		label += "PRINT\r\n";

		byte data[] = label.getBytes();
		return data;
	}

}
