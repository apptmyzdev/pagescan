package com.revamobile.gatiscan.data;

import java.util.ArrayList;

public class DocketToScan {
	private String docketNo;
	private ArrayList<String> hunBarCode;
	private ArrayList<String> pktNos;

	public DocketToScan() {
		super();
	}

	public DocketToScan(String docketNo, ArrayList<String> hunBarCode,
			ArrayList<String> pktNos) {
		super();
		this.docketNo = docketNo;
		this.hunBarCode = hunBarCode;
		this.pktNos = pktNos;
	}

	public String getDocketNo() {
		return docketNo;
	}

	public void setDocketNo(String docketNo) {
		this.docketNo = docketNo;
	}

	public ArrayList<String> getHunBarCode() {
		return hunBarCode;
	}

	public void setHunBarCode(ArrayList<String> hunBarCode) {
		this.hunBarCode = hunBarCode;
	}

	public ArrayList<String> getPktNos() {
		return pktNos;
	}

	public void setPktNos(ArrayList<String> pktNos) {
		this.pktNos = pktNos;
	}

	@Override
	public String toString() {
		return "DocketToScan [docketNo=" + docketNo + ", hunBarCode="
				+ hunBarCode + ", pktNos=" + pktNos + "]";
	}

}
