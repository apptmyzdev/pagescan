package com.revamobile.gatiscan.data;

public class DocketDetailsResponse {
	private boolean status;
	private String message;
	private DocketsToScan data;

	public DocketDetailsResponse() {
		super();
	}

	public DocketDetailsResponse(boolean status, String message,
			DocketsToScan data) {
		super();
		this.status = status;
		this.message = message;
		this.data = data;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public DocketsToScan getData() {
		return data;
	}

	public void setData(DocketsToScan data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "DocketDetailsResponse [status=" + status + ", message="
				+ message + ", data=" + data + "]";
	}

}
