package com.revamobile.gatiscan.data;

import java.util.ArrayList;
import java.util.Set;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.revamobile.gatiscan.datacache.DataSource;
import com.revamobile.gatiscan.utils.Constants;
import com.revamobile.gatiscan.utils.Globals;
import com.revamobile.pagescan.OnLineScreen;
import com.revamobile.pagescan.R;

public class DeviceList extends Activity {
	private Context context;

	private BluetoothAdapter mBtAdapter;
	private ArrayAdapter<String> mPairedDevicesArrayAdapter;
	private ArrayAdapter<String> mNewDevicesArrayAdapter;
	private ArrayList<BluetoothDevice> mPairedDevicesArray = new ArrayList<BluetoothDevice>();
	private ArrayList<BluetoothDevice> mNewDevicesArray = new ArrayList<BluetoothDevice>();

	private ListView pairedListView, newDevicesListView;
	private Button scanBtn, closeBtn;
	private String noDevicePaired, noDevicesFound;

	private DataSource dataSource;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		setContentView(R.layout.device_list);

		context = DeviceList.this;

		dataSource = new DataSource(context);

		setResult(Activity.RESULT_CANCELED);

		Globals.selectedDevice = null;

		noDevicePaired = getResources().getText(R.string.none_paired)
				.toString();
		noDevicesFound = getResources().getText(R.string.none_found).toString();

		mPairedDevicesArrayAdapter = new ArrayAdapter<String>(this,
				R.layout.device_name);
		mNewDevicesArrayAdapter = new ArrayAdapter<String>(this,
				R.layout.device_name);

		pairedListView = (ListView) findViewById(R.id.paired_devices);
		pairedListView.setAdapter(mPairedDevicesArrayAdapter);
		pairedListView.setOnItemClickListener(mDeviceClickListener);

		newDevicesListView = (ListView) findViewById(R.id.new_devices);
		newDevicesListView.setAdapter(mNewDevicesArrayAdapter);
		newDevicesListView.setOnItemClickListener(mDeviceClickListener);

		IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
		this.registerReceiver(mReceiver, filter);

		filter = new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
		this.registerReceiver(mReceiver, filter);

		mBtAdapter = BluetoothAdapter.getDefaultAdapter();

		Set<BluetoothDevice> pairedDevices = mBtAdapter.getBondedDevices();

		if (pairedDevices.size() > 0) {
			mPairedDevicesArrayAdapter.clear();
			mPairedDevicesArray.clear();
			findViewById(R.id.title_paired_devices).setVisibility(View.VISIBLE);
			for (BluetoothDevice device : pairedDevices) {
				if (device != null) {
					mPairedDevicesArray.add(device);
					mPairedDevicesArrayAdapter.add(device.getName() + "\n"
							+ device.getAddress());
				}
			}
		} else {
			mPairedDevicesArray.add(null);
			mPairedDevicesArrayAdapter.add(noDevicePaired);
		}

		scanBtn = (Button) findViewById(R.id.scan_btn);
		scanBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				doDiscovery();
				// v.setVisibility(View.GONE);
			}
		});
		closeBtn = (Button) findViewById(R.id.close_btn);
		closeBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				close();
			}
		});

	}

	private void close() {
		if (OnLineScreen.findDeviceList != null)
			OnLineScreen.findDeviceList = null;

		Intent intent = new Intent();
		Globals.selectedDevice = null;
		setResult(Activity.RESULT_OK, intent);
		finish();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (OnLineScreen.findDeviceList != null)
			OnLineScreen.findDeviceList = null;

		if (mBtAdapter != null) {
			mBtAdapter.cancelDiscovery();
		}

		this.unregisterReceiver(mReceiver);
	}

	private void doDiscovery() {
		mNewDevicesArrayAdapter.clear();
		mNewDevicesArray.clear();
		setProgressBarIndeterminateVisibility(true);
		setTitle(R.string.scanning);

		findViewById(R.id.title_new_devices).setVisibility(View.VISIBLE);

		if (mBtAdapter.isDiscovering()) {
			mBtAdapter.cancelDiscovery();
		}

		mBtAdapter.startDiscovery();
	}

	private OnItemClickListener mDeviceClickListener = new OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> listView, View v, int index,
				long arg3) {

			if (listView == pairedListView) {
				selectedDevice = mPairedDevicesArray.get(index);
			} else if (listView == newDevicesListView) {
				selectedDevice = mNewDevicesArray.get(index);
			}

			if (selectedDevice != null) {

				Globals.selectedDevice = selectedDevice;
				String address = selectedDevice.getAddress();
				dataSource.shardPreferences.set(Constants.PRINTER, address);
				mBtAdapter.cancelDiscovery();
				Intent intent = new Intent();
				setResult(Activity.RESULT_OK, intent);

				if (OnLineScreen.findDeviceList != null)
					OnLineScreen.findDeviceList = null;

				finish();
			}
		}
	};
	private BluetoothDevice selectedDevice = null;

	private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();

			if (BluetoothDevice.ACTION_FOUND.equals(action)) {
				BluetoothDevice device = intent
						.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
				if (device != null) {
					if (device.getBondState() != BluetoothDevice.BOND_BONDED) {
						mNewDevicesArray.add(device);
						mNewDevicesArrayAdapter.add(device.getName() + "\n"
								+ device.getAddress());
					}
				}
			} else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED
					.equals(action)) {
				setProgressBarIndeterminateVisibility(false);
				setTitle(R.string.select_device);
				if (mNewDevicesArrayAdapter.getCount() == 0) {
					mNewDevicesArray.add(null);
					mNewDevicesArrayAdapter.add(noDevicesFound);

				}
			}
		}
	};

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			close();
			return true;
		}
		return super.onKeyUp(keyCode, event);
	}

}