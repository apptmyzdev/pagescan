package com.revamobile.gatiscan.data;

import java.util.List;

public class DocketData {
	List<DocketInfo> docketInfoList;

	public DocketData() {
		super();
	}

	public DocketData(List<DocketInfo> docketInfoList) {
		super();
		this.docketInfoList = docketInfoList;
	}

	public List<DocketInfo> getDocketInfoList() {
		return docketInfoList;
	}

	public void setDocketInfoList(List<DocketInfo> docketInfoList) {
		this.docketInfoList = docketInfoList;
	}

	@Override
	public String toString() {
		return "DocketData [docketInfoList=" + docketInfoList + "]";
	}

}
