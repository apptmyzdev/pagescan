package com.revamobile.gatiscan.data;

public class DktHunPkgMappingModel {

	private String hunBarCode;
	private String pkgNo;
	private String scannedTime;

	public DktHunPkgMappingModel() {
		super();
	}

	public DktHunPkgMappingModel(String hunBarCode, String pkgNo,
			String scannedTime) {
		super();
		this.hunBarCode = hunBarCode;
		this.pkgNo = pkgNo;
		this.scannedTime = scannedTime;
	}

	public String getHunBarCode() {
		return hunBarCode;
	}

	public void setHunBarCode(String hunBarCode) {
		this.hunBarCode = hunBarCode;
	}

	public String getPkgNo() {
		return pkgNo;
	}

	public void setPkgNo(String pkgNo) {
		this.pkgNo = pkgNo;
	}

	public String getScannedTime() {
		return scannedTime;
	}

	public void setScannedTime(String scannedTime) {
		this.scannedTime = scannedTime;
	}

	@Override
	public String toString() {
		return "DktHunPkgMappingModel [hunBarCode=" + hunBarCode + ", pkgNo="
				+ pkgNo + ", scannedTime=" + scannedTime + "]";
	}

}
