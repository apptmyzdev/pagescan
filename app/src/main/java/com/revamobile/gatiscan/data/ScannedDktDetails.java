package com.revamobile.gatiscan.data;

import java.util.List;

public class ScannedDktDetails {

	private List<ScannedDocketsDataModel> dktDetails;

	public ScannedDktDetails() {
		super();
	}

	public ScannedDktDetails(List<ScannedDocketsDataModel> dktDetails) {
		super();
		this.dktDetails = dktDetails;
	}

	public List<ScannedDocketsDataModel> getDktDetails() {
		return dktDetails;
	}

	public void setDktDetails(List<ScannedDocketsDataModel> dktDetails) {
		this.dktDetails = dktDetails;
	}

	@Override
	public String toString() {
		return "ScannedDktDetails [dktDetails=" + dktDetails + "]";
	}

}
