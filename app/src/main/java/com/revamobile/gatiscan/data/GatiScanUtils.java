package com.revamobile.gatiscan.data;

public class GatiScanUtils {
	// public static final String baseUrl = "http://119.235.57.47:9080";
	// public static final String baseUrl = "http://172.31.2.50:9080";

	public static final String baseUrl = "http://tracker.gati.com:9080";
	// public static final String baseUrl = "http://119.235.57.108:9080";

	public static final String gatiscannerTag = "/gatiscanner";
	public static final String custdktintTag = "/custdktint";

	public static String newLoginUrlTag = "/gatiScannerLogin?txtuser=";
	public static String passwordTag = "&txtpasswd=";
	public static String imeiTag = "&imei=";
	public static String apkVersionTag = "&apkversion=";

	public static String getDocketDetailsTag = "/getDocketDetails";
	public static String getDktHunPkgDetailsTag = "/getDktHunPkgDetails";
	public static String saveHunPkgScanDetailsTag = "/saveHunPkgScanDetails";

	public static String custCodeTag = "?custCode=";
	public static String custVentCodeTag = "&custVendCode=";

	public static String getDktsUrlTag = "/getdktsforgemspush";

	public static String pushDktsUrl = "/pushdktstogems";

	public static String getNewLoginUrl(String userName, String password,
			String imei, String version) {
		return //"http://119.235.57.47:9080"
				"http://tracker.gati.com:9080"
				+ gatiscannerTag + newLoginUrlTag
				+ userName + passwordTag + password + imeiTag + imei
				+ apkVersionTag + version;//http://119.235.57.47:9080/gatiscanner/gatiScannerLogin?txtuser=name&txtpasswd=&imei=
	}

	public static String getDocketDetailsUrl(String custCode,
			String custVentCode) {
		return baseUrl + custdktintTag + getDocketDetailsTag + custCodeTag
				+ custCode + custVentCodeTag + custVentCode;
	}

	public static String getDktHunPkgDetailsUrl(String custCode,
			String custVentCode) {
		return baseUrl + custdktintTag + getDktHunPkgDetailsTag + custCodeTag
				+ custCode + custVentCodeTag + custVentCode;
	}

	public static String getSubmitUrl() {
		return baseUrl + custdktintTag + saveHunPkgScanDetailsTag;
	}

	public static String getDocketsUrl(String custCode, String custVentCode) {
		return baseUrl + custdktintTag + getDktsUrlTag + custCodeTag + custCode
				+ custVentCodeTag + custVentCode;
	}

	public static String getPushDktsUrl() {
		return baseUrl + custdktintTag + pushDktsUrl;
	}

}
