package com.revamobile.gatiscan.data;

public class LoginResponse {

	private String result;
	private String sErrMsg;
	private String userid;
	private String oucode;

	public LoginResponse() {
		super();
	}

	public LoginResponse(String result, String sErrMsg, String userid,
			String oucode) {
		super();
		this.result = result;
		this.sErrMsg = sErrMsg;
		this.userid = userid;
		this.oucode = oucode;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getsErrMsg() {
		return sErrMsg;
	}

	public void setsErrMsg(String sErrMsg) {
		this.sErrMsg = sErrMsg;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getOucode() {
		return oucode;
	}

	public void setOucode(String oucode) {
		this.oucode = oucode;
	}

	@Override
	public String toString() {
		return "LoginResponse [result=" + result + ", sErrMsg=" + sErrMsg
				+ ", userid=" + userid + ", oucode=" + oucode + "]";
	}

}
