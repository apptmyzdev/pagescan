package com.revamobile.gatiscan.data;

public class DocketsResponse {
	private boolean status;
	private String message;
	private DocketData data;

	public DocketsResponse() {
		super();
	}

	public DocketsResponse(boolean status, String message, DocketData data) {
		super();
		this.status = status;
		this.message = message;
		this.data = data;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public DocketData getData() {
		return data;
	}

	public void setData(DocketData data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "DocketsResponse [status=" + status + ", message=" + message
				+ ", data=" + data + "]";
	}

}
