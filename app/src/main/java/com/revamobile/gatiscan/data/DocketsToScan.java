package com.revamobile.gatiscan.data;

import java.util.ArrayList;

public class DocketsToScan {
	private ArrayList<DocketToScan> dktsToScan;

	public DocketsToScan() {
		super();
	}

	public DocketsToScan(ArrayList<DocketToScan> dktsToScan) {
		super();
		this.dktsToScan = dktsToScan;
	}

	public ArrayList<DocketToScan> getDktsToScan() {
		return dktsToScan;
	}

	public void setDktsToScan(ArrayList<DocketToScan> dktsToScan) {
		this.dktsToScan = dktsToScan;
	}

	@Override
	public String toString() {
		return "DocketsToScan [dktsToScan=" + dktsToScan + "]";
	}

}
