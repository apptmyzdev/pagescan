package com.revamobile.gatiscan.utils;

import java.util.UUID;

public class Constants {

	public static final String LOG_TAG = "Gati";

	public static final boolean isTesting = false;
	public static final boolean isUseingFileEncryption = false;
	public static final boolean isAlwaysExpandable = false;

	public static final int scanStringLength = 9;
	public static final int scanTime = 1000;
	public static final int scanStartTime = 50;

	// public static final int scanStringLengthHunMin = 8;
	// public static final int scanStringLengthHunMax = 12;
	public static final int scanStringLengthHun = 10;
	public static final int scanTimeHun = 1100;
	public static final int scanStartTimeHun = 50;

	public static final int maxProgressRange = 100;
	public static final String SHARED_PREF = "GATI";
	public static final String USERNAME_PREF = "USERNAME";
	public static final String PASSWORD_PREF = "PASSWORD";

	public static final String OUCODE_PREF = "OUCODE";

	public static final int radioButton1Tag = 0;
	public static final int radioButton2Tag = 1;

	public static final String SPLIT = "#GATI#";

	public static String dateFormat = "yyyy-MM-dd HH:mm:ss";

	public static String SCANNING_CHAR_PATTERN = "^[0-9]+";

	public static String USERNAME_ERROR;
	public static String PASSWORD_ERROR;

	public static String SERVER_ERROR;
	public static String NETWORK_UNAVAILABLE;
	public static String PROBLEM_WITH_NETWORK;
	public static String SERVER_NOT_REACHABLE;
	public static String DEVICE_CONNECTIVITY;
	public static String PARSEDISPMSG;
	public static String PARSEDETMSG;
	public static String NETWORK_PROBLEM;

	public static final String BLANK_SPACE = " ";

	public static final String TRUE = "TRUE";
	public static final String FALSE = "FALSE";

	public static final String SUCCESSFUL = "successful";
	public static final String FAILED = "failed";

	/** Printing **/

	public static final int OPTION_BTN_TEXTSIZE = 18;
	public static String OPTION_BTN_COLOR = "#03768A";
	public static String OPTION_BTN_SHADOWCOLOR = "#777777";

	public static String SELECT_ALL_OFF = "#787878";
	public static String SELECT_ALL_ON = "#026702";

	public static String[] MONTHS = { "Jan", "Feb", "Mar", "Apr", "May", "Jun",
			"July", "Aug", "Sep", "Oct", "Nov", "Dec" };
	public static final String DOKET_PRINT_SEPARATOR = "-";

	public static final UUID MY_UUID = UUID
			.fromString("00001101-0000-1000-8000-00805F9B34FB");

	public static final int REQUEST_ENABLE_BT = 1;
	public static final int REQUEST_PAIRED_DEVICE = 2;
	public static final int REQUEST_DISCOVERABLE_BT = 3;

	public static final int REQUEST_SETTING = 4;

	public static final int connAttemptsTym = 1000;
	public static final int maxConnAttemptsTym = 16000;

	public static final int PRINT_SLEEP_TIME = 800;

	public static final String PRINTER = "printer";
	public static final String PRINTER_TYPE = "printerType";

	public static final int ZERO = 0;
	public static final int ONE = 1;
	public static final int TWO = 2;
	public static final int THREE = 3;

	public static final int MAX_NUMBER_OF_SAVE_DOCKET_INFO = 500;

	public static final int REQUEST_REPRINT_DOCKET = 101;
	public static final int REQUEST_BARCODE_REQ = 102;

}
