package com.revamobile.gatiscan.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.http.NameValuePair;
import org.codehaus.jackson.map.ObjectMapper;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.support.v4.content.IntentCompat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.google.gson.Gson;
import com.revamobile.gatiscan.data.DocketInfo;
import com.revamobile.gatiscan.data.PrintDocketInfo;
import com.revamobile.gatiscan.datacache.DataSource;
import com.revamobile.pagescan.LoginScreen;
import com.revamobile.pagescan.R;

public class Utils {

	private static MediaPlayer mPlayer = null;
	private static final ObjectMapper mapper = new ObjectMapper();
	public static ArrayList<Integer> audioList = new ArrayList<Integer>();
	private static int lastPlayAudio = 0;

	private static final Gson gson = new Gson();

	public static Object parse(InputStream is, Class<?> classOfT)
			throws Exception {
		try {
			Reader readr = new InputStreamReader(is);
			return gson.fromJson(readr, classOfT);
		}

		catch (Exception e) {
			Utils.logE(e.toString());
			throw new ADException(Constants.PARSEDISPMSG, Constants.PARSEDETMSG);
			// throw e;
		}
	}

	public static Object parseResp(InputStream is, Class<?> classOfT)
			throws Exception {
		try {
			Reader readr = new InputStreamReader(is);
			// String s = inputStreamToString(is);
			// Utils.logD(s);
			return gson.fromJson(readr, classOfT);
		}

		catch (Exception e) {
			Utils.logE(e.toString());
			throw new ADException(Constants.PARSEDISPMSG, Constants.PARSEDETMSG);
			// throw e;
		}
	}

	public static String inputStreamToString(InputStream is) {
		String str = "";
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();

		try {
			while ((str = br.readLine()) != null) {
				sb.append(str);
			}
			br.close();
		} catch (IOException e) {
			Utils.logD(e.toString());
		}

		str = sb.toString();

		// Utils.printLog("InputStream To String : " + str);
		return str;
	}

	public static int getDisplaySize() {
		if (Globals.screenHeight > Globals.screenWidth)
			return Globals.screenWidth;
		else
			return Globals.screenHeight;
	}

	public static Float convertDpToPixel(float dp, Context context) {
		Resources resources = context.getResources();
		DisplayMetrics metrics = resources.getDisplayMetrics();
		Float px = dp * (metrics.densityDpi / 160f);
		return px;
	}

	public static Float convertPixelsToDp(float px, Context context) {
		Resources resources = context.getResources();
		DisplayMetrics metrics = resources.getDisplayMetrics();
		Float dp = px / (metrics.densityDpi / 160f);
		return dp;
	}

	public static void dissmissKeyboard(View v) {
		InputMethodManager imm = (InputMethodManager) v.getContext()
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
	}

	public static void showKeyboard(View v) {
		InputMethodManager imm = (InputMethodManager) v.getContext()
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.showSoftInput(v, InputMethodManager.SHOW_IMPLICIT);
	}

	public static void savePreferences(Context context, String key, String value) {
		SharedPreferences sharedPreferences = context.getSharedPreferences(
				Constants.SHARED_PREF, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.putString(key, value);
		editor.commit();
	}

	public static void removePreferences(Context context, String key) {
		SharedPreferences sharedPreferences = context.getSharedPreferences(
				Constants.SHARED_PREF, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.clear();
		editor.commit();
	}

	public static String loadPreferences(Context context, String key) {
		SharedPreferences sharedPreferences = ((Activity) context)
				.getSharedPreferences(Constants.SHARED_PREF,
						Context.MODE_PRIVATE);
		String val = "";
		try {
			val = sharedPreferences.getString(key, "");
		} catch (Exception e) {
			val = "";
		}
		return val;
	}

	public static void showToast(Context context, String msg) {
		if (Constants.isTesting)
			Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
	}

	public static void logE(String msg) {
		if (Constants.isTesting)
			Log.e(Constants.LOG_TAG, msg);
	}

	public static void logI(String msg) {
		if (Constants.isTesting)
			Log.i(Constants.LOG_TAG, msg);
	}

	public static void logD(String msg) {
		if (Constants.isTesting)
			Log.d(Constants.LOG_TAG, msg);
	}

	public static String getCurrentTime() {
		return System.currentTimeMillis() + "";
	}

	public static String getStringFromList(List<NameValuePair> data) {
		String value = "";
		boolean first = true;
		for (NameValuePair item : data) {
			if (!first) {
				value += "&";

			}
			first = false;
			value += item.getName() + "=" + item.getValue();
		}
		return value;

	}

	private static void audioManager(Context context) {
		AudioManager audioManager = (AudioManager) ((Activity) context)
				.getSystemService(Context.AUDIO_SERVICE);

		int vol = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
		if (!audioManager.isSpeakerphoneOn())
			;
		audioManager.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
		if (vol != 100) {
			audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, 100,
					AudioManager.FLAG_ALLOW_RINGER_MODES);
		}
	}

	public static void releasePlayer() {
		try {
			if (mPlayer != null) {
				mPlayer.stop();
				mPlayer.release();
				mPlayer = null;
			}
		} catch (Exception e) {
			Utils.logD("Audio Release exception : " + e.toString());
		}
	}

	private static void playAudio(final Context context, int audioId) {
		try {
			lastPlayAudio = audioId;
			mPlayer = MediaPlayer.create(context, audioId);
			mPlayer.setVolume(100, 100);
			mPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
				@Override
				public void onCompletion(MediaPlayer mediaPlayer) {
					releasePlayer();

					int audio = nextAudio();

					if (audio != -1 && lastPlayAudio != audio)
						playAudio(context, audio);
					else {
						audioList.clear();
					}
				}
			});
			mPlayer.start();

		} catch (Exception e) {
			Utils.logD("Audio exception : " + e.toString());
		}
	}

	public static void playAudioList(final Context context, int audioId) {
		if (audioList == null)
			audioList = new ArrayList<Integer>();
		if (audioList.contains(audioId))
			audioList = new ArrayList<Integer>();
		audioList.add(audioId);
		if (mPlayer == null) {
			audioManager(context);
			playAudio(context, audioId);

		}
	}

	public static int nextAudio() {
		int audio = -1;
		int index = 0;
		for (Integer audioId : audioList) {
			if (lastPlayAudio == audioId) {
				audio = index;
				audio = audio + 1;
				break;
			}
			index++;
		}

		try {
			audio = audioList.get(audio);
			if (lastPlayAudio == audio)
				audio = nextAudio();
		} catch (Exception e) {
			audio = -1;
		}

		return audio;
	}

	public static boolean isValidString(String str) {
		if (str != null) {
			str = str.trim();
			if (str.length() > 0)
				return true;
		}
		return false;
	}

	public static boolean isValidArrayList(ArrayList<?> list) {
		if (list != null && list.size() > 0) {
			return true;
		}
		return false;
	}

	public static String getScanTime() {
		String scanTime = "";
		Format formatter = new SimpleDateFormat(Constants.dateFormat);
		scanTime = formatter.format(new Date());
		return scanTime;
	}

	public static String getScanTime(long diff) {
		String scanTime = "";
		Date d = new Date();
		long t = d.getTime();
		long t1 = t + diff;
		Date date = new Date(t1);
		Format formatter = new SimpleDateFormat(Constants.dateFormat);
		scanTime = formatter.format(date);
		logD("ScanTime original :" + d.toString());
		logD("ScanTime diff :" + diff);
		logD("ScanTime :" + scanTime);
		return scanTime;
	}

	public static Date getDate(String time) {
		Date date = null;
		SimpleDateFormat formatter = new SimpleDateFormat(Constants.dateFormat);
		try {
			date = (Date) formatter.parseObject(time);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}

	public static void showSimpleAlert(Context context, String msg) {
		AlertDialog.Builder alert = new AlertDialog.Builder(context);
		alert.setTitle(context.getString(R.string.alert_dialog_title));
		alert.setMessage(msg);
		alert.setPositiveButton(context.getString(R.string.ok_btn),
				new Dialog.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});
		alert.show();
	}

	public static boolean getConnectivityStatus(Context context) {
		ConnectivityManager cm = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
		boolean isConnected = activeNetwork != null
				&& activeNetwork.isConnected();
		return isConnected;
	}

	public static int getLabelCount(ArrayList<DocketInfo> dockets) {
		int numLabels = 0;
		for (DocketInfo docket : dockets) {
			numLabels += docket.getNo_of_pkgs();
		}
		return numLabels;
	}

	private Timer timer = new Timer();
	private TimerTask timerTask;
	private Handler handler;
	private int time = 0;

	public void printInTimerTask(final byte[] byteArr) {

		if (timerTask != null)
			timerTask.cancel();
		time = Constants.connAttemptsTym;
		timer = new Timer();
		handler = new Handler();
		timerTask = new TimerTask() {
			@Override
			public void run() {
				handler.post(new Runnable() {
					@Override
					public void run() {
						if (time > Constants.maxConnAttemptsTym) {
							timer.cancel();
							// Toast.makeText(), "END : "+time,
							// Toast.LENGTH_LONG).show();
						} else
							time = time * 2;
					}

				});

			}

		};
		timer.schedule(timerTask, Constants.ZERO, time);

		// Toast.makeText(mContext, "END : "+time, Toast.LENGTH_LONG).show();
	}

	public static View.OnClickListener logoutListener = new View.OnClickListener() {
		@Override
		public void onClick(View view) {

			Context context = view.getContext();
			Activity activity = ((Activity) context);

			DataSource dataSource = new DataSource(context);

			dataSource.shardPreferences.delete(Constants.USERNAME_PREF);
			dataSource.shardPreferences.delete(Constants.PASSWORD_PREF);
			dataSource.shardPreferences.delete(Constants.OUCODE_PREF);

			Intent intent = new Intent(context, LoginScreen.class);
			ComponentName cn = intent.getComponent();
			Intent mainIntent = IntentCompat.makeRestartActivityTask(cn);
			activity.startActivity(mainIntent);
			activity.finish();

		}

	};

	public static int generatePkgNum(String docket, int noofpkgs) {
		// Docket number is read as character array
		char pwd_val;
		int pwd_value, pwd_val_1 = 0;
		for (int i = 0; i < docket.length(); i++) {
			pwd_val = docket.charAt(i);
			pwd_value = pwd_val;// Read the ASCII value of the number
			if (noofpkgs > 9999 && noofpkgs < 99999) {
				pwd_val_1 = pwd_val_1 + pwd_value * (int) Math.pow(1, i);
			} else {
				pwd_val_1 = pwd_val_1 + pwd_value * (int) Math.pow(2, i);
			}
		}
		String s = String.valueOf(pwd_val_1) + String.valueOf(noofpkgs);// String
																		// concat
																		// of
																		// pwd_val_1
		// and number of packages
		char c = docket.charAt(8);// read into c the last character of docket
		int n = 9;
		int add = n - s.length(); // may overflow int size... should not be a
									// problem in real life
		StringBuffer str = new StringBuffer(s);
		char[] ch = new char[add];

		java.util.Arrays.fill(ch, c);// Set all character of ch array with last
										// character of docket

		str.append(ch);
		// add 1 to the
		// str as
		// starting packagenumber
		return (Integer.parseInt(str.toString()) + 1);

	}

	public static int getLabelCountR(ArrayList<PrintDocketInfo> printDocketInfos) {
		int numLabels = 0;
		for (PrintDocketInfo printDocketInfo : printDocketInfos) {
			numLabels += printDocketInfo.getDocketInfo().getNo_of_pkgs();
		}
		return numLabels;
	}

}