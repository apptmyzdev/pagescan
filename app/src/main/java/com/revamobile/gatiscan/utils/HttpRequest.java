package com.revamobile.gatiscan.utils;

import java.io.InputStream;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.HttpHostConnectException;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;

public class HttpRequest {

	public static InputStream getInputStreamFromUrl(String url) throws ADException {
		// InputStream content = null;
		Utils.logD("URL : " + url);
		try {
			HttpGet httpGet = new HttpGet(url);
			HttpClient httpclient = new DefaultHttpClient();
//			httpGet.addHeader("Accept-Encoding", "gzip");
			HttpResponse response = httpclient.execute(httpGet);
			return response.getEntity().getContent();

		} catch (Exception e) {
			Utils.logD(e.getMessage());
			Globals.lastErrMsg = Constants.DEVICE_CONNECTIVITY;
			throw new ADException(Constants.DEVICE_CONNECTIVITY, "");
		}
	}

	private static final String CONTENT_TYPE = "application/json";

	public static Object postData(String restAPIPath, String data,
			Class classOfT) throws ADException, UnknownHostException {

		HttpResponse response;

		Utils.logD("URL : " + restAPIPath);
		Utils.logD("Data : " + data);

		try {

			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httpPost = new HttpPost(restAPIPath);
			httpPost.setEntity(new StringEntity(data, "UTF-8"));

			httpPost.setHeader("Content-type", CONTENT_TYPE);

			response = httpclient.execute(httpPost);

			int statusCode = response.getStatusLine().getStatusCode();
			Utils.logD("Status Code : " + statusCode);

			if (statusCode != 200) {

				if (statusCode > 400) {
					Globals.lastErrMsg = Constants.SERVER_NOT_REACHABLE;
					throw new ADException("", Constants.SERVER_ERROR);
				} else if (statusCode > 500) {
					Globals.lastErrMsg = Constants.SERVER_ERROR;
					throw new ADException("", Constants.SERVER_NOT_REACHABLE);
				}

				return null;

			} else {
				return Utils.parseResp(response.getEntity().getContent(),
						classOfT);
			}

		} catch (UnknownHostException e) {
			Globals.lastErrMsg = Constants.DEVICE_CONNECTIVITY;
			throw e;
		} catch (SocketTimeoutException e) {
			throw new ADException(Constants.DEVICE_CONNECTIVITY,
					Constants.SERVER_NOT_REACHABLE);
		} catch (HttpHostConnectException e) {
			throw new ADException(Constants.DEVICE_CONNECTIVITY,
					Constants.SERVER_NOT_REACHABLE);
		} catch (Exception e) {
			// Log.d(Constants.LOG_TAG, e.getMessage());
			if (!Utils.isValidString(Globals.lastErrMsg)) {
				Globals.lastErrMsg = Constants.NETWORK_UNAVAILABLE;
			}
			throw new ADException(Constants.NETWORK_UNAVAILABLE, "");
		}

	}

}
