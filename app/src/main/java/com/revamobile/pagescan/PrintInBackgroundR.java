package com.revamobile.pagescan;

import java.util.ArrayList;
import java.util.List;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.revamobile.gatiscan.data.FormattedLabelR;
import com.revamobile.gatiscan.data.PrintDocketInfo;
import com.revamobile.gatiscan.datacache.SaveDocketInfo;
import com.revamobile.gatiscan.utils.Constants;
import com.revamobile.gatiscan.utils.Globals;
import com.revamobile.gatiscan.utils.Utils;

public class PrintInBackgroundR extends AsyncTask<Void, Void, Void> {
	private OnLineScreen main;
	private int printCount = 0;

	public PrintInBackgroundR(OnLineScreen main) {
		super();
		this.main = main;
	}

	@Override
	protected void onPreExecute() {
		Log.d(Constants.LOG_TAG, "Printing Starting");
	}

	@Override
	protected Void doInBackground(Void... arg0) {
		try {

			ArrayList<PrintDocketInfo> printDocketInfos = Globals.printDocketInfos;
			boolean cancel = false;

			for (PrintDocketInfo printDocketInfo : printDocketInfos) {
				List<String> labels = FormattedLabelR.getFormattedLabel(main,
						printDocketInfo);
				for (String label : labels) {
					// Globals.currentPrintStatus = "Printing " +
					// ++currLabel + "/" + numLabels;
					// Log.d("REVA", "Value of Cancel Print: " +
					// Globals.cancelPrint);
					if (!Globals.cancelPrint && !Globals.pausePrint) {
						if (Globals.resumePrint
								&& printCount < Globals.pausePrintAt) {
							// do nothing
						} else {
							Log.d(Constants.LOG_TAG, label);

							int progress = 0;

							try {
								while (progress < Constants.maxConnAttemptsTym
										&& !Globals.cancelPrint
										&& !Globals.pausePrint) {

									try {
										if (progress > 0)
											Thread.sleep(progress);
									} catch (Exception e) {
									}

									progress = progress
											+ Constants.connAttemptsTym;
									Log.d(Constants.LOG_TAG, "progress "
											+ progress);

									if (main.print(label.getBytes())) {
										Log.d(Constants.LOG_TAG, "Print Break");
										break;
									}

								}

							} catch (Exception e) {
							}

							if (progress >= Constants.maxConnAttemptsTym) {
								Globals.isPrint = false;
								Globals.cancelPrint = true;
							}

						}
						printCount++;

					} else if (Globals.cancelPrint) {
						Log.d(Constants.LOG_TAG, "Printing was CANCELLED");
						cancel = true;
						Globals.cancelPrint = false;
						// Globals.pausePrint = false;
						break;
					} else if (Globals.pausePrint) {
						Log.d(Constants.LOG_TAG, "Printing was PAUSED");
						cancel = true;
						Globals.pausePrintAt = printCount;
						// Globals.pausePrint = false;
						break;
					}

					if (Globals.resumePrint
							&& printCount < Globals.pausePrintAt) {
						// do nothing
					} else {
						try {
							Thread.sleep(Constants.PRINT_SLEEP_TIME);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
				}
				if (cancel) {
					Log.d(Constants.LOG_TAG, "Printing was CANCELLED2");
					break;
				}
				// mCommandService.stop();

				SaveDocketInfo.saveInHistory(main.getApplicationContext(),
						printDocketInfo.getDocketInfo());

			}

		} catch (Exception e) {
			Globals.lastErrMsg = e.toString();
		}
		return null;
	}

	@Override
	protected void onPostExecute(Void result) {

		if (Globals.lastErrMsg != null && Globals.lastErrMsg.length() > 0)
			Toast.makeText(main, Globals.lastErrMsg, Toast.LENGTH_SHORT).show();

		Log.d(Constants.LOG_TAG, "Printing FINISHED");
		if (printCount == Utils.getLabelCountR(Globals.printDocketInfos)) {
			Globals.selectedPrint = false;
			Globals.isPrint = false;
			Globals.pausePrint = false;
			Globals.resumePrint = false;
			Log.d(Constants.LOG_TAG, "Printed All Dockets");
		}
		if (printCount < Utils.getLabelCountR(Globals.printDocketInfos)
				&& Globals.pausePrint) {
			Globals.isPrint = true;
			Globals.isPause = false;
			// Globals.pausePrint = false;
		}

		// if (Globals.isReprint) {
		// Globals.selectedPrint = false;
		// Globals.isPrint = false;
		// Globals.pausePrint = false;
		// Globals.resumePrint = false;
		// Globals.isReprint = false;
		// }

		// Globals.printing = false;
		// Globals.currentPrintStatus = "";
		super.onPostExecute(result);
	}
}
