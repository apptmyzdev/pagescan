package com.revamobile.pagescan;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import com.revamobile.gati.custom.ADRadioGroup;
import com.revamobile.gatiscan.data.DocketInfo;
import com.revamobile.gatiscan.data.PrintDocketInfo;
import com.revamobile.gatiscan.data.ScanDocket;
import com.revamobile.gatiscan.utils.Constants;
import com.revamobile.gatiscan.utils.Globals;
import com.revamobile.gatiscan.utils.Utils;

public class ReprintDocket extends Activity implements OnClickListener,
		OnCheckedChangeListener {

	private Context context;

	private TextView tv_dkt_info, tv_re_print_from_lable, tv_re_print_to_lable;
	private Button btn_re_print_from_value, btn_re_print_to_value;
	private Button btn_print, btn_cancel;
	private ImageButton ib_re_print_from_scan, ib_re_print_to_scan;
	private ADRadioGroup ad_re_print_opt;

	private ArrayList<String> packageNumberList = new ArrayList<String>();

	private DocketInfo reprintDocketInfo = null;

	private ScanFromToListAlert scanFromToListAlert;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.reprint_docket);
		context = ReprintDocket.this;

		if (Globals.reprintSaveDocketInfo != null)
			reprintDocketInfo = Globals.reprintSaveDocketInfo.getDocketInfo();

		if (reprintDocketInfo != null) {

			tv_dkt_info = (TextView) findViewById(R.id.tv_dkt_info);

			tv_re_print_from_lable = (TextView) findViewById(R.id.tv_re_print_from_lable);
			tv_re_print_to_lable = (TextView) findViewById(R.id.tv_re_print_to_lable);

			btn_re_print_from_value = (Button) findViewById(R.id.btn_re_print_from_value);
			btn_re_print_to_value = (Button) findViewById(R.id.btn_re_print_to_value);
			ib_re_print_from_scan = (ImageButton) findViewById(R.id.ib_re_print_from_scan);
			ib_re_print_to_scan = (ImageButton) findViewById(R.id.ib_re_print_to_scan);

			btn_print = (Button) findViewById(R.id.btn_print);
			btn_cancel = (Button) findViewById(R.id.btn_cancel);

			ad_re_print_opt = (ADRadioGroup) findViewById(R.id.ad_re_print_opt);
			ad_re_print_opt.check(-1);

			tv_re_print_from_lable.setEnabled(false);
			tv_re_print_to_lable.setEnabled(false);
			btn_re_print_from_value.setEnabled(false);
			btn_re_print_to_value.setEnabled(false);
			ib_re_print_from_scan.setEnabled(false);
			ib_re_print_to_scan.setEnabled(false);

			btn_re_print_from_value.setOnClickListener(this);
			btn_re_print_to_value.setOnClickListener(this);
			btn_print.setOnClickListener(this);
			btn_cancel.setOnClickListener(this);

			ib_re_print_from_scan.setOnClickListener(this);
			ib_re_print_to_scan.setOnClickListener(this);

			ad_re_print_opt.setOnCheckedChangeListener(this);

			setPackageNumberFromAndTo(reprintDocketInfo);

			try {
				new Handler(context.getMainLooper()).postDelayed(
						new Runnable() {
							@Override
							public void run() {
								Utils.dissmissKeyboard(btn_re_print_from_value);
							}
						}, 100);
			} catch (Exception e) {
			}

		} else {
			finish();
		}
	}

	private void setPackageNumberFromAndTo(DocketInfo data) {
		tv_dkt_info.setText(data.getDocketno() + " "
				+ Constants.DOKET_PRINT_SEPARATOR + " " + data.getNo_of_pkgs()
				+ " " + Constants.DOKET_PRINT_SEPARATOR + " "
				+ data.getDlystn());

		int noofpkgs = data.getNo_of_pkgs();

		packageNumberList.clear();
		int pkgNum = Utils.generatePkgNum(data.getDocketno(), noofpkgs);
		for (int pkgNo = pkgNum; pkgNo <= (pkgNum + noofpkgs - 1); pkgNo++) {
			packageNumberList.add(String.valueOf(pkgNo));
		}

		ad_re_print_opt.check(R.id.re_print_opt_one);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_re_print_from_value:
			rePrintFromValue(false);
			break;
		case R.id.btn_re_print_to_value:
			rePrintFromTo(false);
			break;
		case R.id.btn_print:

			Globals.printDocketInfos.clear();

			// int id = ad_re_print_opt.getCheckedRadioButtonId();
			// if (id != -1) {
			// switch (id) {
			// case R.id.re_print_opt_one:
			// GatiGlobals.printDocketInfos.add(new PrintDocketInfo(
			// reprintDocketInfo));
			// break;
			//
			// case R.id.re_print_opt_two:
			// PrintDocketInfo info = new PrintDocketInfo();
			// info.setDocketInfo(reprintDocketInfo);
			//
			// String fromValue = btn_re_print_from_value.getText()
			// .toString();
			//
			// String toValue = btn_re_print_to_value.getText().toString();
			//
			// ArrayList<String> arr = new ArrayList<String>();
			// CharSequence[] items = null;
			//
			// for (int pos = 0; pos < packageNumberList.size(); pos++) {
			// arr.add(String.valueOf(pos));
			// }
			// if (arr.size() > 0) {
			// items = new CharSequence[arr.size()];
			// for (int pos = 0; pos < arr.size(); pos++) {
			// items[pos] = arr.get(pos);
			// if (fromValue.equalsIgnoreCase(arr.get(pos))) {
			// from = pos;
			// break;
			// }
			// if (toValue.equalsIgnoreCase(arr.get(pos))) {
			// to = pos;
			// break;
			// }
			// }
			// }
			//
			// // if (to != packageNumberList.size())
			// // to = to + 1;
			//
			// info.setFrom(from);
			// info.setTo(to);
			// GatiGlobals.printDocketInfos.add(info);
			// break;
			//
			// case R.id.re_print_opt_three:
			//
			// PrintDocketInfo printDocketInfo = new PrintDocketInfo();
			// printDocketInfo.setDocketInfo(reprintDocketInfo);
			//
			// String fromVal = btn_re_print_from_value.getText()
			// .toString();
			//
			// String toVal = btn_re_print_to_value.getText().toString();
			//
			// ArrayList<String> array = new ArrayList<String>();
			// CharSequence[] cItems = null;
			//
			// for (int pos = 0; pos < packageNumberList.size(); pos++) {
			// array.add(packageNumberList.get(pos));
			// }
			// if (array.size() > 0) {
			// cItems = new CharSequence[array.size()];
			// for (int pos = 0; pos < array.size(); pos++) {
			// cItems[pos] = array.get(pos);
			// if (fromVal.equalsIgnoreCase(array.get(pos))) {
			// from = pos;
			// break;
			// }
			// if (toVal.equalsIgnoreCase(array.get(pos))) {
			// to = pos;
			// break;
			// }
			// }
			// }
			// if (to != packageNumberList.size())
			// to = to + 1;
			// printDocketInfo.setFrom(from);
			// printDocketInfo.setTo(to);
			// GatiGlobals.printDocketInfos.add(printDocketInfo);
			//
			// break;
			//
			// default:
			// break;
			// }
			//
			// }

			// from = 0;
			// to = packageNumberList.size() - 1;
			// int id = ad_re_print_opt.getCheckedRadioButtonId();
			// PrintDocketInfo info = new PrintDocketInfo(reprintDocketInfo);
			//
			// if (id == R.id.re_print_opt_two || id == R.id.re_print_opt_three)
			// {
			// String fromValue = btn_re_print_from_value.getText().toString();
			// String toValue = btn_re_print_to_value.getText().toString();
			//
			// ArrayList<String> arr = new ArrayList<String>();
			// CharSequence[] items = null;
			//
			// if (id == R.id.re_print_opt_two) {
			// for (int pos = 0; pos < packageNumberList.size(); pos++) {
			// arr.add(String.valueOf(pos));
			// }
			// } else {
			// for (int pos = 0; pos < packageNumberList.size(); pos++) {
			// arr.add(packageNumberList.get(pos));
			// }
			// }
			//
			// if (arr.size() > 0) {
			// items = new CharSequence[arr.size()];
			// for (int pos = 0; pos < arr.size(); pos++) {
			// items[pos] = arr.get(pos);
			// if (fromValue.equalsIgnoreCase(arr.get(pos))) {
			// from = pos;
			// break;
			// }
			// if (toValue.equalsIgnoreCase(arr.get(pos))) {
			// to = pos;
			// break;
			// }
			// }
			// }
			// }
			//
			// if (to + 1 <= packageNumberList.size())
			// to = to + 1;
			//
			// info.setFrom(from);
			// info.setTo(to);
			//
			// GatiGlobals.printDocketInfos.add(info);
			//
			// Intent intent = new Intent();
			// setResult(Activity.RESULT_OK, intent);
			// finish();

			int id = ad_re_print_opt.getCheckedRadioButtonId();
			PrintDocketInfo info = new PrintDocketInfo(reprintDocketInfo);

			if (id == R.id.re_print_opt_two || id == R.id.re_print_opt_three) {

				int from = -1;
				int to = -1;

				String fromValue = btn_re_print_from_value.getText().toString();
				String toValue = btn_re_print_to_value.getText().toString();

				for (int pos = 0; pos < packageNumberList.size(); pos++) {
					String posStr = String.valueOf(pos);
					String value = String.valueOf(packageNumberList.get(pos));
					if (from == -1
							&& (fromValue.equalsIgnoreCase(value) || fromValue
									.equalsIgnoreCase(posStr))) {
						from = pos;
					}

					if (to == -1
							&& (toValue.equalsIgnoreCase(value) || toValue
									.equalsIgnoreCase(posStr))) {
						to = pos;
					}
				}
				if (to > -1 && from > -1 && to >= from) {
					info.setTo(to + 1);
					info.setFrom(from);

				}
			}

			Log.d("Ashish", info.toString());

			Globals.printDocketInfos.add(info);

			Intent intent = new Intent();
			setResult(Activity.RESULT_OK, intent);
			finish();

			break;
		case R.id.btn_cancel:
			finish();
			break;
		case R.id.ib_re_print_from_scan:
			rePrintFromValue(true);
			break;
		case R.id.ib_re_print_to_scan:
			rePrintFromTo(true);
			break;
		default:
			break;
		}
	}

	@Override
	public void onCheckedChanged(RadioGroup group, int checkedId) {

		tv_re_print_from_lable.setEnabled(true);
		tv_re_print_to_lable.setEnabled(true);
		btn_re_print_from_value.setEnabled(true);
		btn_re_print_to_value.setEnabled(true);
		ib_re_print_from_scan.setEnabled(true);
		ib_re_print_to_scan.setEnabled(true);

		tv_re_print_from_lable.setVisibility(View.VISIBLE);
		tv_re_print_to_lable.setVisibility(View.VISIBLE);
		btn_re_print_from_value.setVisibility(View.VISIBLE);
		btn_re_print_to_value.setVisibility(View.VISIBLE);

		switch (checkedId) {
		case R.id.re_print_opt_one:

			// tv_re_print_from_lable.setEnabled(false);
			// tv_re_print_to_lable.setEnabled(false);
			// btn_re_print_from_value.setEnabled(false);
			// btn_re_print_to_value.setEnabled(false);
			// ib_re_print_from_scan.setEnabled(false);
			// ib_re_print_to_scan.setEnabled(false);

			tv_re_print_from_lable.setVisibility(View.INVISIBLE);
			tv_re_print_to_lable.setVisibility(View.INVISIBLE);
			btn_re_print_from_value.setVisibility(View.INVISIBLE);
			btn_re_print_to_value.setVisibility(View.INVISIBLE);
			ib_re_print_from_scan.setVisibility(View.INVISIBLE);
			ib_re_print_to_scan.setVisibility(View.INVISIBLE);

			break;
		case R.id.re_print_opt_two:

			ib_re_print_from_scan.setVisibility(View.GONE);
			ib_re_print_to_scan.setVisibility(View.GONE);

			tv_re_print_from_lable.setText(R.string.seq_number_from);
			tv_re_print_to_lable.setText(R.string.seq_number_to);

			if (packageNumberList != null && packageNumberList.size() > 0) {
				btn_re_print_from_value.setText(String.valueOf(0));
				btn_re_print_to_value.setText(String.valueOf(packageNumberList
						.size() - 1));
			}

			break;
		case R.id.re_print_opt_three:

			ib_re_print_from_scan.setVisibility(View.VISIBLE);
			ib_re_print_to_scan.setVisibility(View.VISIBLE);

			tv_re_print_from_lable.setText(R.string.package_number_from);
			tv_re_print_to_lable.setText(R.string.package_number_to);

			if (packageNumberList != null && packageNumberList.size() > 0) {
				btn_re_print_from_value.setText(packageNumberList.get(0));
				btn_re_print_to_value.setText(packageNumberList
						.get(packageNumberList.size() - 1));
			}

			break;
		default:
			break;
		}

		// btn_re_print_from_value.setSelection(btn_re_print_from_value.length());
		// btn_re_print_to_value.setSelection(btn_re_print_to_value.length());
		// GatiGlobals.dissmissKeybord(btn_re_print_from_value);

	}

	private void rePrintFromValue(boolean isScan) {
		String title = "";
		CharSequence[] items = null;
		String selectedValue = btn_re_print_from_value.getText().toString();
		int selectionPos = -1;
		ArrayList<String> arr = new ArrayList<String>();
		switch (ad_re_print_opt.getCheckedRadioButtonId()) {
		case R.id.re_print_opt_one:
			title = "";
			break;

		case R.id.re_print_opt_two:
			title = "Sequence Number From : ";

			for (int pos = 0; pos < packageNumberList.size(); pos++) {
				arr.add(String.valueOf(pos));
			}
			if (arr.size() > 0) {
				items = new CharSequence[arr.size()];
				for (int pos = 0; pos < arr.size(); pos++) {
					items[pos] = arr.get(pos);
					if (selectedValue.equalsIgnoreCase(arr.get(pos))) {
						selectionPos = pos;
					}
				}
			}
			break;
		case R.id.re_print_opt_three:
			title = "Package Number From : ";
			for (int pos = 0; pos < packageNumberList.size(); pos++) {
				arr.add(packageNumberList.get(pos));
			}
			if (arr.size() > 0) {
				items = new CharSequence[arr.size()];
				for (int pos = 0; pos < arr.size(); pos++) {
					items[pos] = arr.get(pos);
					if (selectedValue.equalsIgnoreCase(arr.get(pos))) {
						selectionPos = pos;
					}
				}
			}
			break;
		default:
			break;
		}
		if (items != null) {
			if (isScan) {
				scanFromToListAlert = new ScanFromToListAlert(
						btn_re_print_from_value, title, items, selectionPos,
						this, "",
						new ScanFromToListAlert.DoScanClickListener() {
							@Override
							public void doScan(
									ScanFromToListAlert scanFromToListAlert) {
								doScanText();
							}
						});
				// scanFromToListAlert.show();
				doScanText();

			} else {
				showSelection(btn_re_print_from_value, title, items,
						selectionPos);
			}
		}
	}

	private void rePrintFromTo(boolean isScan) {
		String title = "";
		CharSequence[] items = null;
		String selectedFromValue = btn_re_print_from_value.getText().toString();
		String selectedValue = btn_re_print_to_value.getText().toString();
		int selectionPos = -1;
		boolean found = false;
		ArrayList<String> arr = new ArrayList<String>();
		switch (ad_re_print_opt.getCheckedRadioButtonId()) {
		case R.id.re_print_opt_one:
			title = "";
			break;

		case R.id.re_print_opt_two:
			title = "Sequence Number To : ";
			for (int pos = 0; pos < packageNumberList.size(); pos++) {
				if (!found) {
					found = selectedFromValue.equalsIgnoreCase(String
							.valueOf(pos));
				}
				if (found) {
					arr.add(String.valueOf(pos));
				}
			}
			if (arr.size() > 0) {
				items = new CharSequence[arr.size()];
				for (int pos = 0; pos < arr.size(); pos++) {
					items[pos] = arr.get(pos);
					if (selectedValue.equalsIgnoreCase(arr.get(pos))) {
						selectionPos = pos;
					}
				}
			}
			break;
		case R.id.re_print_opt_three:
			title = "Package Number To : ";
			for (int pos = 0; pos < packageNumberList.size(); pos++) {
				if (!found) {
					found = selectedFromValue
							.equalsIgnoreCase(packageNumberList.get(pos));
				}
				if (found) {
					arr.add(String.valueOf(packageNumberList.get(pos)));
				}
			}
			if (arr.size() > 0) {
				items = new CharSequence[arr.size()];
				for (int pos = 0; pos < arr.size(); pos++) {
					items[pos] = arr.get(pos);
					if (selectedValue.equalsIgnoreCase(arr.get(pos))) {
						selectionPos = pos;
					}
				}
			}
			break;
		default:
			break;
		}
		if (isScan) {
			scanFromToListAlert = new ScanFromToListAlert(
					btn_re_print_to_value, title, items, selectionPos, this,
					"", new ScanFromToListAlert.DoScanClickListener() {
						@Override
						public void doScan(
								ScanFromToListAlert scanFromToListAlert) {
							doScanText();
						}
					});
			// scanFromToListAlert.show();
			doScanText();
		} else {
			showSelection(btn_re_print_to_value, title, items, selectionPos);
		}
	}

	private void showSelection(final Button btn, final String title,
			final CharSequence[] items, int selectionPos) {
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setTitle(title);
		builder.setSingleChoiceItems(items, selectionPos,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int item) {
						btn.setText(items[item]);
						dialog.dismiss();
					}
				});
		builder.setNegativeButton(
				getResources().getString(R.string.cancel_btn),
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int id) {
						dialog.dismiss();
					}
				});
		AlertDialog alert = builder.create();
		alert.show();
	}

	public void doScanText() {
		if (isCameraAvailable()) {
			try {
				Intent intent = new Intent(context, ScanDocket.class);
				if (isCallable(intent))
					startActivityForResult(intent,
							Constants.REQUEST_BARCODE_REQ);
			} catch (Exception e) {
				// Utils.logE(e.toString());
			}
		} else {
			Toast.makeText(
					context,
					"Rear Facing Camera Unavailable. Docket Scan is not possible with this device",
					Toast.LENGTH_LONG).show();
		}
	}

	public boolean isCameraAvailable() {
		PackageManager pm = getPackageManager();
		return pm.hasSystemFeature(PackageManager.FEATURE_CAMERA);
	}

	private boolean isCallable(Intent intent) {
		List<ResolveInfo> list = context.getPackageManager()
				.queryIntentActivities(intent,
						PackageManager.MATCH_DEFAULT_ONLY);
		return list.size() > 0;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == RESULT_OK
				&& requestCode == Constants.REQUEST_BARCODE_REQ) {
			if (scanFromToListAlert != null) {
				scanFromToListAlert.show();
				scanFromToListAlert.setScanResult(data
						.getStringExtra(com.revamobile.pagescan.ScanDocket.scannedTag));
			}
		} else if (resultCode == RESULT_CANCELED) {
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

}
