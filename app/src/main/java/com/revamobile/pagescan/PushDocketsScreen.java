package com.revamobile.pagescan;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.map.ObjectMapper;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.revamobile.gatiscan.data.DocketData;
import com.revamobile.gatiscan.data.DocketInfo;
import com.revamobile.gatiscan.data.DocketsResponse;
import com.revamobile.gatiscan.data.GatiScanUtils;
import com.revamobile.gatiscan.data.GeneralResponse;
import com.revamobile.gatiscan.datacache.DataSource;
import com.revamobile.gatiscan.utils.Constants;
import com.revamobile.gatiscan.utils.Globals;
import com.revamobile.gatiscan.utils.HttpRequest;
import com.revamobile.gatiscan.utils.Utils;

public class PushDocketsScreen extends Activity implements OnClickListener {
	private ListView mainDisplay;

	private Button selectAllButton;
	public static Context mContext;
	private ArrayAdapter<String> adapter;

	private String title_str;
	private String msg_str;
	private String cancel_str;
	private String ok_str, alert_title, setting_str;

	private String valueArr[] = new String[0];
	private int selectedBtnIconSize = 14;

	private AsyncTask getDocket;

	private AlertDialog errDlg;
	private ProgressDialog progressDialog;

	private DataSource dataSource;

	private Button doneBtn;

	private TextView logout;

	private TextView versionTopTv, userNameTv;

	private String versionVal = "";

	private View blurView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.push_dockets_screen);
		mContext = PushDocketsScreen.this;

		dataSource = new DataSource(mContext);

		mainDisplay = (ListView) findViewById(R.id.main_ListView);
		mainDisplay.setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE);
		selectAllButton = (Button) findViewById(R.id.select_allBtn);

		doneBtn = (Button) findViewById(R.id.btn_done);

		setStrVal();
		setSelectedImgBtn();
		selectAllButton.setTextColor(Color.parseColor(Constants.SELECT_ALL_ON));

		selectAllButton.setOnClickListener(this);
		doneBtn.setOnClickListener(this);

		adapter = new ArrayAdapter<String>(mContext,
				R.layout.multiple_selections, valueArr);
		mainDisplay.setAdapter(adapter);
		mainDisplay.setOnItemClickListener(new ListView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				refresh();
			}
		});

		logout = (TextView) findViewById(R.id.tv_logout);
		logout.setVisibility(View.VISIBLE);
		logout.setOnClickListener(Utils.logoutListener);

		versionTopTv = (TextView) findViewById(R.id.tv_version_top);

		try {
			versionVal = getPackageManager()
					.getPackageInfo(getPackageName(), 0).versionName;
			if (Utils.isValidString(versionVal))
				versionTopTv.setText("Ver : " + versionVal);
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}

		userNameTv = (TextView) findViewById(R.id.tv_username);
		String user = dataSource.shardPreferences
				.getValue(Constants.USERNAME_PREF);
		if (Utils.isValidString(user))
			userNameTv.setText(user);

		getDocketInfoList();
	}

	private void setSelectedImgBtn() {
		Drawable img = mContext.getResources().getDrawable(
				R.drawable.is_elected);

		img.setBounds(0, 0, selectedBtnIconSize, selectedBtnIconSize);
		selectAllButton.setCompoundDrawables(img, null, null, null);
		selectAllButton.setTextColor(Color.parseColor(Constants.SELECT_ALL_ON));
	}

	private void setUnselectedImgBtn() {
		Drawable img = mContext.getResources()
				.getDrawable(R.drawable.ic_delete);
		img.setBounds(0, 0, selectedBtnIconSize, selectedBtnIconSize);
		selectAllButton.setCompoundDrawables(img, null, null, null);
		selectAllButton
				.setTextColor(Color.parseColor(Constants.SELECT_ALL_OFF));

	}

	private void setStrVal() {
		selectedBtnIconSize = Integer
				.parseInt(getString(R.string.selectedBtnIconSize));
		title_str = getString(R.string.progress_dialog_title);
		msg_str = getString(R.string.progress_dialog_msg);
		cancel_str = getString(R.string.cancel_btn);
		ok_str = getString(R.string.ok_btn);
		alert_title = getString(R.string.alert_dialog_title);
		setting_str = getString(R.string.setting);
	}

	@Override
	public void onClick(View view) {
		if (view == selectAllButton) {
			if (isAllSelected() == true)
				unselectAll();
			else
				selectAll();
			refresh();
		} else if (view == doneBtn) {
			Globals.selectedDocketList.clear();
			for (int position = 0; position < valueArr.length; position++) {
				if (mainDisplay.isItemChecked(position) == true) {
					Globals.selectedDocketList.add(Globals.docketData
							.getDocketInfoList().get(position));
				}
			}
			if (Utils.isValidArrayList(Globals.selectedDocketList)) {
				new PushDockets().execute();
			} else {
				Utils.showSimpleAlert(mContext,
						"Please select dockets to push to GEMS");
			}
		}
	}

	private void getDocketInfoList() {

		valueArr = new String[0];
		adapter = new ArrayAdapter<String>(mContext,
				R.layout.multiple_selections, valueArr);
		mainDisplay.setAdapter(adapter);
		getDocket = new GetDocket().execute();
	}

	private boolean isAllSelected() {
		if (mainDisplay != null && mainDisplay.getCount() > 0) {
			boolean isSelected = true;
			for (int position = 0; position < mainDisplay.getCount(); position++) {
				if (!mainDisplay.isItemChecked(position)) {
					isSelected = false;
					break;
				}
			}
			return isSelected;
		}
		return false;
	}

	private void selectAll() {
		if (mainDisplay != null && mainDisplay.getCount() > 0) {
			for (int position = 0; position < mainDisplay.getCount(); position++) {
				mainDisplay.setItemChecked(position, true);
			}
		}
	}

	private void unselectAll() {
		if (mainDisplay != null && mainDisplay.getCount() > 0) {
			for (int position = 0; position < mainDisplay.getCount(); position++) {
				mainDisplay.setItemChecked(position, false);
			}
		}
	}

	private void refresh() {
		adapter.notifyDataSetChanged();
		if (selectAllButton != null) {
			if (isAllSelected() == true && mainDisplay.getCount() > 0) {
				setUnselectedImgBtn();
			} else {
				setSelectedImgBtn();
			}
		}
	}

	class GetDocket extends AsyncTask {
		private ProgressDialog dialog;

		@SuppressWarnings("deprecation")
		@Override
		protected void onPreExecute() {
			Globals.lastErrMsg = "";

			dialog = new ProgressDialog(mContext);
			dialog.setTitle(title_str);
			dialog.setMessage(msg_str);
			dialog.setIndeterminate(true);
			dialog.setCancelable(false);
			dialog.setButton(cancel_str, new DialogInterface.OnClickListener() {
				// @Override
				@Override
				public void onClick(DialogInterface dialog, int which) {
					getDocket.cancel(true);
					dialog.dismiss();
					finish();
				}

			});
			dialog.show();
		}

		@Override
		protected Object doInBackground(Object... arg0) {
			try {
				String url = GatiScanUtils.getDocketsUrl(Globals.custCode,
						Globals.custVentCode);
				InputStream is = HttpRequest.getInputStreamFromUrl(url);
				if (is != null) {
					DocketsResponse response = (DocketsResponse) Utils.parse(
							is, DocketsResponse.class);
					if (response != null) {
						Utils.logD(response.toString());
						if (response.isStatus()) {
							DocketData data = response.getData();
							if (data != null) {
								Globals.docketData = data;
								return true;
							} else {
								Globals.lastErrMsg = "No Data Found";
								return false;
							}
						} else {
							Globals.lastErrMsg = response.getMessage();
							return false;
						}
					} else {
						Globals.lastErrMsg = "Response is null";
						return false;
					}
				} else {
					Globals.lastErrMsg = "Response is null";
					return false;
				}

			} catch (Exception e) {
				if (!Utils.isValidString(Globals.lastErrMsg))
					Globals.lastErrMsg = e.getMessage();
				return false;
			}

		}

		@Override
		protected void onPostExecute(Object result) {
			boolean value = (Boolean) result;

			if (value == false) {
				String err = Constants.PARSEDISPMSG + ","
						+ Constants.PARSEDETMSG;
				if (Globals.lastErrMsg != null
						&& Globals.lastErrMsg.length() > 0)
					err = Globals.lastErrMsg;
				Globals.lastErrMsg = "";

				AlertDialog.Builder builder = new AlertDialog.Builder(mContext);

				builder.setTitle(R.string.alert_dialog_title);
				builder.setCancelable(true);
				builder.setMessage(err);
				builder.setNegativeButton(ok_str,
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								errDlg.dismiss();
								finish();
							}
						});
				if (err.contains(Constants.NETWORK_UNAVAILABLE)) {
					builder.setPositiveButton(setting_str,
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									errDlg.dismiss();
									Intent intent = new Intent();
									intent.setAction(android.provider.Settings.ACTION_WIRELESS_SETTINGS);
									mContext.startActivity(intent);
								}
							});
				}
				errDlg = builder.create();
				dialog.dismiss();
				errDlg.show();
			} else {
				valueArr = new String[Globals.docketData.getDocketInfoList()
						.size()];
				int i = 0;
				for (DocketInfo data : Globals.docketData.getDocketInfoList()) {
					valueArr[i] = data.getDocketno()
							+ Constants.DOKET_PRINT_SEPARATOR
							+ data.getNo_of_pkgs()
							+ Constants.DOKET_PRINT_SEPARATOR
							+ data.getDlystn();
					i++;
				}
				adapter = new ArrayAdapter<String>(mContext,
						R.layout.multiple_selections, valueArr);
				mainDisplay.setAdapter(adapter);
				dialog.dismiss();

			}
			super.onPostExecute(result);
		}

	}

	class PushDockets extends AsyncTask<Object, Object, Object> {

		@Override
		protected void onPreExecute() {
			showProgressDialog();

		}

		@Override
		protected Object doInBackground(Object... params) {
			try {

				String submitUrl = GatiScanUtils.getPushDktsUrl();

				ObjectMapper mapper = new ObjectMapper();

				List<String> list = new ArrayList<String>();

				if (Utils.isValidArrayList(Globals.selectedDocketList)) {
					for (DocketInfo d : Globals.selectedDocketList) {
						String dnum = d.getDocketno();
						if (Utils.isValidString(dnum))
							list.add(dnum);
					}
				}

				String data = mapper.writeValueAsString(list);

				if (Utils.isValidString(data) && Utils.isValidString(submitUrl)) {

					Utils.logD("Submit Data : " + data);
					GeneralResponse response = (GeneralResponse) HttpRequest
							.postData(submitUrl, data, GeneralResponse.class);
					// PostData.getRespose(Globals.username, data);
					if (response != null) {
						if (response.isStatus()) {
							Utils.logD(response.toString());
						} else {
							Globals.lastErrMsg = response.getMessage();
						}
					} else {
						Globals.lastErrMsg = "Server Response is null";
					}

				}

			} catch (Exception e) {
				if (!Utils.isValidString(Globals.lastErrMsg))
					Globals.lastErrMsg = e.toString();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Object result) {

			if (showErrorDialog() && progressDialog != null
					&& progressDialog.isShowing()) {
				if (Utils.isValidArrayList(Globals.selectedDocketList))
					Globals.selectedDocketList.clear();
				Globals.custCode = null;
				Globals.custVentCode = null;
				progressDialog.dismiss();
				Utils.showToast(mContext, "Successful");
				goToHome();
			}

			super.onPostExecute(result);
		}
	}

	private void goToHome() {
		Globals.isBack = true;
		finish();

	}

	@SuppressWarnings("deprecation")
	private void showProgressDialog() {
		progressDialog = new ProgressDialog(mContext);
		progressDialog.setTitle(getString(R.string.loading));
		progressDialog.setMessage(getString(R.string.progress_dialog_title));
		progressDialog.setIndeterminate(true);
		progressDialog.setCancelable(false);
		progressDialog.show();
	}

	@SuppressWarnings("deprecation")
	private boolean showErrorDialog() {
		boolean isNotErr = true;
		if (Globals.lastErrMsg != null && Globals.lastErrMsg.length() > 0) {

			errDlg = new AlertDialog.Builder(mContext).create();
			errDlg.setTitle(getString(R.string.alert_dialog_title));
			errDlg.setCancelable(false);
			errDlg.setMessage(Globals.lastErrMsg);
			Globals.lastErrMsg = "";
			errDlg.setButton(getString(R.string.ok_btn),
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							errDlg.dismiss();
						}
					});
			if (progressDialog != null && progressDialog.isShowing())
				progressDialog.dismiss();
			isNotErr = false;
			errDlg.show();
		}
		return isNotErr;
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			finish();
			return true;
		}
		return super.onKeyUp(keyCode, event);
	}

}
