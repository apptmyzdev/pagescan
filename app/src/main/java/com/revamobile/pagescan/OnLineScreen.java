package com.revamobile.pagescan;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.revamobile.gati.custom.MyOptionItem;
import com.revamobile.gati.custom.MyPrintOptionMenu;
import com.revamobile.gati.custom.PrintHistoryPopup;
import com.revamobile.gatiscan.data.DeviceList;
import com.revamobile.gatiscan.data.DocketData;
import com.revamobile.gatiscan.data.DocketInfo;
import com.revamobile.gatiscan.data.DocketsResponse;
import com.revamobile.gatiscan.data.FormattedLabel;
import com.revamobile.gatiscan.data.GatiScanUtils;
import com.revamobile.gatiscan.data.PrintDocketInfo;
import com.revamobile.gatiscan.datacache.DataSource;
import com.revamobile.gatiscan.datacache.SaveDocketInfo;
import com.revamobile.gatiscan.utils.BluetoothService;
import com.revamobile.gatiscan.utils.Constants;
import com.revamobile.gatiscan.utils.Globals;
import com.revamobile.gatiscan.utils.HttpRequest;
import com.revamobile.gatiscan.utils.Utils;

public class OnLineScreen extends Activity implements OnClickListener {
	private ListView mainDisplay;

	private ImageButton printBtn, refreshBtn;
	private Button selectAllButton, dateButton;
	public static Context mContext;
	private ArrayAdapter<String> adapter;
	private static final int REQUEST_ENABLE_BT = 4;
	private static final int REQUEST_PAIRED_DEVICE = 5;
	private static final int REQUEST_DISCOVERABLE_BT = 6;
	private BluetoothAdapter bluetoothAdapter;

	private int mYear;
	private int mMonth;
	private int mDay;
	private static final int DATE_DIALOG_ID = 1;
	private static final int ERROR_DIALOG_ID = 2;

	private String dateVal;

	private String title_str;
	private String msg_str;
	private String cancel_str;
	private String ok_str, alert_title, setting_str;
	private String not_connected, connected_to, connecting_to_printer,
			connect_devices;

	private AsyncTask getDocket;
	private AsyncTask printInBackground;

	private AlertDialog errDlg;
	private String valueArr[] = new String[0];
	private int selectedBtnIconSize = 14;

	private MyPrintOptionMenu action = null;
	private MyOptionItem printItem, cancelItem, pauseOrResumeItem, historyItem;
	private boolean states = false;
	private boolean printStates = false;

	private Timer timer = new Timer();
	private TimerTask timerTask;
	private Handler handler;
	private int time = 0;
	private ProgressDialog progressDialog;

	public static final int MESSAGE_STATE_CHANGE = 1;
	public static final int MESSAGE_READ = 2;
	public static final int MESSAGE_WRITE = 3;
	public static final int MESSAGE_DEVICE_NAME = 4;
	public static final int MESSAGE_TOAST = 5;
	public static final String DEVICE_NAME = "device_name";
	public static final String TOAST = "toast";

	private BluetoothService mCommandService = null;
	private String mConnectedDeviceName = null;

	public static Intent findDeviceList = null;

	private DataSource dataSource;

	private Button doneBtn;

	private TextView logout;

	private TextView versionTopTv, userNameTv;

	private String versionVal = "";

	private View blurView;
	private PrintHistoryPopup printHistoryPopup;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.online);
		mContext = OnLineScreen.this;

		dataSource = new DataSource(mContext);

		Globals.printerArr = getResources().getStringArray(
				R.array.whichPrinterArr);

		mainDisplay = (ListView) findViewById(R.id.main_ListView);
		mainDisplay.setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE);
		printBtn = (ImageButton) findViewById(R.id.printBtn);
		selectAllButton = (Button) findViewById(R.id.select_allBtn);

		refreshBtn = (ImageButton) findViewById(R.id.refresh_btn);
		dateButton = (Button) findViewById(R.id.date_button);

		doneBtn = (Button) findViewById(R.id.btn_done);

		setStrVal();
		setSelectedImgBtn();
		selectAllButton.setTextColor(Color.parseColor(Constants.SELECT_ALL_ON));

		printBtn.setOnClickListener(this);
		refreshBtn.setOnClickListener(this);
		dateButton.setOnClickListener(this);
		selectAllButton.setOnClickListener(this);
		doneBtn.setOnClickListener(this);

		adapter = new ArrayAdapter<String>(mContext,
				R.layout.multiple_selections, valueArr);
		mainDisplay.setAdapter(adapter);
		mainDisplay.setOnItemClickListener(new ListView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				refresh();
			}
		});

		logout = (TextView) findViewById(R.id.tv_logout);
		logout.setVisibility(View.VISIBLE);
		logout.setOnClickListener(Utils.logoutListener);

		blurView = (View) findViewById(R.id.view_blur);
		blurView.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				return true;
			}
		});
		hideBlurView();

		versionTopTv = (TextView) findViewById(R.id.tv_version_top);

		try {
			versionVal = getPackageManager()
					.getPackageInfo(getPackageName(), 0).versionName;
			if (Utils.isValidString(versionVal))
				versionTopTv.setText("Ver : " + versionVal);
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}

		userNameTv = (TextView) findViewById(R.id.tv_username);
		String user = dataSource.shardPreferences
				.getValue(Constants.USERNAME_PREF);
		if (Utils.isValidString(user))
			userNameTv.setText(user);

		final Calendar c = Calendar.getInstance();
		mYear = c.get(Calendar.YEAR);
		mMonth = c.get(Calendar.MONTH);
		mDay = c.get(Calendar.DAY_OF_MONTH);
		setDate();

		getDocketInfoList();
	}

	private void setSelectedImgBtn() {
		Drawable img = mContext.getResources().getDrawable(
				R.drawable.is_elected);

		img.setBounds(0, 0, selectedBtnIconSize, selectedBtnIconSize);
		selectAllButton.setCompoundDrawables(img, null, null, null);
		selectAllButton.setTextColor(Color.parseColor(Constants.SELECT_ALL_ON));
	}

	private void setUnselectedImgBtn() {
		Drawable img = mContext.getResources()
				.getDrawable(R.drawable.ic_delete);
		img.setBounds(0, 0, selectedBtnIconSize, selectedBtnIconSize);
		selectAllButton.setCompoundDrawables(img, null, null, null);
		selectAllButton
				.setTextColor(Color.parseColor(Constants.SELECT_ALL_OFF));

	}

	private void setStrVal() {
		selectedBtnIconSize = Integer
				.parseInt(getString(R.string.selectedBtnIconSize));
		title_str = getString(R.string.progress_dialog_title);
		msg_str = getString(R.string.progress_dialog_msg);
		cancel_str = getString(R.string.cancel_btn);
		ok_str = getString(R.string.ok_btn);
		alert_title = getString(R.string.alert_dialog_title);
		not_connected = getString(R.string.title_not_connected);
		connected_to = getString(R.string.title_connected_to);
		connecting_to_printer = getString(R.string.connecting_to_printer);
		connect_devices = getString(R.string.connect_devices);
		setting_str = getString(R.string.setting);
	}

	@Override
	public void onClick(View view) {
		if (view == printBtn) {
			print(view);
		} else if (view == selectAllButton) {
			if (isAllSelected() == true)
				unselectAll();
			else
				selectAll();
			refresh();
		} else if (view == dateButton) {
			showDialog(DATE_DIALOG_ID);
		} else if (view == refreshBtn) {
			getDocketInfoList();
		} else if (view == doneBtn) {
			Intent intent = new Intent(mContext, PreScanScreen.class);
			startActivity(intent);
			finish();
		}
	}

	@SuppressWarnings("deprecation")
	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case DATE_DIALOG_ID:
			DatePickerDialog datePickerDialog = new DatePickerDialog(this,
					mDateSetListener, mYear, mMonth, mDay);
			return datePickerDialog;
		case ERROR_DIALOG_ID:
			errDlg = new AlertDialog.Builder(mContext).create();
			errDlg.setTitle(alert_title);
			errDlg.setCancelable(true);
			errDlg.setMessage(Globals.lastErrMsg);
			errDlg.setButton(ok_str, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					Globals.lastErrMsg = "";
					errDlg.dismiss();
				}
			});
			// errDlg.setIcon(R.drawable.failure);
			return errDlg;

		}
		return null;
	}

	private void setDate() {
		dateVal = mDay + Constants.DOKET_PRINT_SEPARATOR
				+ Constants.MONTHS[mMonth] + Constants.DOKET_PRINT_SEPARATOR
				+ mYear;
		dateButton.setText(dateVal);
	}

	@Override
	protected void onPrepareDialog(int id, Dialog dialog) {
		switch (id) {
		case DATE_DIALOG_ID:
			String str = getResources().getString(R.string.date_dialog_title);
			((DatePickerDialog) dialog).setTitle(str);
			((DatePickerDialog) dialog).updateDate(mYear, mMonth, mDay);
			break;
		}
	}

	private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {

		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			mYear = year;
			mMonth = monthOfYear;
			mDay = dayOfMonth;
			setDate();
			getDocketInfoList();
		}
	};

	private void getDocketInfoList() {

		valueArr = new String[0];
		adapter = new ArrayAdapter<String>(mContext,
				R.layout.multiple_selections, valueArr);
		mainDisplay.setAdapter(adapter);
		setDate();
		getDocket = new GetDocket().execute();
	}

	private boolean isAllSelected() {
		if (mainDisplay != null && mainDisplay.getCount() > 0) {
			boolean isSelected = true;
			for (int position = 0; position < mainDisplay.getCount(); position++) {
				if (!mainDisplay.isItemChecked(position)) {
					isSelected = false;
					break;
				}
			}
			return isSelected;
		}
		return false;
	}

	private void selectAll() {
		if (mainDisplay != null && mainDisplay.getCount() > 0) {
			for (int position = 0; position < mainDisplay.getCount(); position++) {
				mainDisplay.setItemChecked(position, true);
			}
		}
	}

	private void unselectAll() {
		if (mainDisplay != null && mainDisplay.getCount() > 0) {
			for (int position = 0; position < mainDisplay.getCount(); position++) {
				mainDisplay.setItemChecked(position, false);
			}
		}
	}

	private void refresh() {
		adapter.notifyDataSetChanged();
		if (selectAllButton != null) {
			if (isAllSelected() == true && mainDisplay.getCount() > 0) {
				setUnselectedImgBtn();
			} else {
				setSelectedImgBtn();
			}
		}
	}

	class GetDocket extends AsyncTask {
		private ProgressDialog dialog;

		@SuppressWarnings("deprecation")
		@Override
		protected void onPreExecute() {
			Globals.lastErrMsg = "";

			dialog = new ProgressDialog(mContext);
			dialog.setTitle(title_str);
			dialog.setMessage(msg_str);
			dialog.setIndeterminate(true);
			dialog.setCancelable(false);
			dialog.setButton(cancel_str, new DialogInterface.OnClickListener() {
				// @Override
				@Override
				public void onClick(DialogInterface dialog, int which) {
					getDocket.cancel(true);
					dialog.dismiss();
					finish();
				}

			});
			dialog.show();
		}

		@Override
		protected Object doInBackground(Object... arg0) {
			try {
				String url = GatiScanUtils.getDocketDetailsUrl(
						Globals.custCode, Globals.custVentCode);
				InputStream is = HttpRequest.getInputStreamFromUrl(url);
				if (is != null) {
					DocketsResponse response = (DocketsResponse) Utils.parse(
							is, DocketsResponse.class);
					if (response != null) {
						Utils.logD(response.toString());
						if (response.isStatus()) {
							DocketData data = response.getData();
							if (data != null) {
								Globals.docketData = data;
								return true;
							} else {
								Globals.lastErrMsg = "No Data Found";
								return false;
							}
						} else {
							Globals.lastErrMsg = response.getMessage();
							return false;
						}
					} else {
						Globals.lastErrMsg = "Response is null";
						return false;
					}
				} else {
					Globals.lastErrMsg = "Response is null";
					return false;
				}

			} catch (Exception e) {
				if (!Utils.isValidString(Globals.lastErrMsg))
					Globals.lastErrMsg = e.getMessage();
				return false;
			}

		}

		@Override
		protected void onPostExecute(Object result) {
			boolean value = (Boolean) result;

			if (value == false) {
				String err = Constants.PARSEDISPMSG + ","
						+ Constants.PARSEDETMSG;
				if (Globals.lastErrMsg != null
						&& Globals.lastErrMsg.length() > 0)
					err = Globals.lastErrMsg;
				Globals.lastErrMsg = "";

				AlertDialog.Builder builder = new AlertDialog.Builder(mContext);

				builder.setTitle(R.string.alert_dialog_title);
				builder.setCancelable(true);
				builder.setMessage(err);
				builder.setNegativeButton(ok_str,
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								errDlg.dismiss();
							}
						});
				if (err.contains(Constants.NETWORK_UNAVAILABLE)) {
					builder.setPositiveButton(setting_str,
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									errDlg.dismiss();
									Intent intent = new Intent();
									intent.setAction(android.provider.Settings.ACTION_WIRELESS_SETTINGS);
									mContext.startActivity(intent);
								}
							});
				}
				errDlg = builder.create();
				dialog.dismiss();
				errDlg.show();
			} else {
				valueArr = new String[Globals.docketData.getDocketInfoList()
						.size()];
				int i = 0;
				for (DocketInfo data : Globals.docketData.getDocketInfoList()) {
					valueArr[i] = data.getDocketno()
							+ Constants.DOKET_PRINT_SEPARATOR
							+ data.getNo_of_pkgs()
							+ Constants.DOKET_PRINT_SEPARATOR
							+ data.getDlystn();
					i++;
				}
				adapter = new ArrayAdapter<String>(mContext,
						R.layout.multiple_selections, valueArr);
				mainDisplay.setAdapter(adapter);
				dialog.dismiss();

			}
			super.onPostExecute(result);
		}

	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			finish();
			return true;
		}
		return super.onKeyUp(keyCode, event);
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		try {
			if (action != null)
				action.dismiss();
		} catch (Exception e) {
		}
	}

	private void print(View view) {
		if (Globals.isPrint == true) {
			states = true;
			printStates = false;
		} else {
			printStates = true;
			states = false;
		}

		action = new MyPrintOptionMenu(view);

		printItem = new MyOptionItem(getResources().getString(R.string.print),
				printStates, new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						Globals.selectedDocketList.clear();
						for (int position = 0; position < valueArr.length; position++) {
							if (mainDisplay.isItemChecked(position) == true) {
								Globals.selectedDocketList
										.add(Globals.docketData
												.getDocketInfoList().get(
														position));
							}
						}
						if (Globals.selectedDocketList.size() > 0) {

							startPrinting();

						} else {
							String sOptions = "";
							if (mainDisplay.getCount() > 0)
								sOptions = mContext.getResources().getString(
										R.string.select_options);
							String nullData = mContext.getResources()
									.getString(R.string.printing_data_null);
							Globals.lastErrMsg = sOptions + nullData;
							showDialog(ERROR_DIALOG_ID);
						}
						action.dismiss();
					}
				});

		if (Globals.isPause) {
			pauseOrResumeItem = new MyOptionItem(getResources().getString(
					R.string.pause), states, new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					Globals.pausePrint = true;
					Globals.isPrint = false;
					action.dismiss();
				}
			});
		} else {
			pauseOrResumeItem = new MyOptionItem(getResources().getString(
					R.string.resume), states, new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					Globals.isPrint = true;
					Globals.pausePrint = false;
					Globals.cancelPrint = false;
					Globals.resumePrint = true;
					Globals.selectedPrint = true;
					printInBackground = new PrintInBackground().execute();
					action.dismiss();
				}
			});
		}

		cancelItem = new MyOptionItem(getResources().getString(
				R.string.cancel_btn), states, new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Globals.isPrint = false;
				Globals.cancelPrint = true;
				action.dismiss();
			}
		});

		historyItem = new MyOptionItem("History", true,
				new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						action.dismiss();
						showPrintHistoryPopup();
					}
				});

		action.addActionItem(printItem);
		action.addActionItem(pauseOrResumeItem);
		action.addActionItem(cancelItem);
		action.addActionItem(historyItem);
		action.show();

	}

	private View showBlurView() {
		if (blurView != null && blurView.getVisibility() != View.VISIBLE) {
			blurView.setVisibility(View.VISIBLE);
			return blurView;
		}
		return null;
	}

	private void hideBlurView() {
		if (blurView != null && blurView.getVisibility() != View.GONE)
			blurView.setVisibility(View.GONE);
	}

	public void showPrintHistoryPopup() {
		DataSource dataSource = new DataSource(mContext);
		ArrayList<SaveDocketInfo> list = dataSource.saveData
				.getDocketInfoList();
		if (list != null && list.size() > 0) {
			dismissPrintHistoryPopup();
			printHistoryPopup = new PrintHistoryPopup(this, list,
					new PopupWindow.OnDismissListener() {
						@Override
						public void onDismiss() {
							hideBlurView();
							printHistoryPopup = null;
						}
					});
			printHistoryPopup.show(showBlurView());
		} else {
			Toast.makeText(mContext, "History not found", Toast.LENGTH_SHORT)
					.show();
		}
	}

	public boolean dismissPrintHistoryPopup() {
		try {
			if (printHistoryPopup != null && printHistoryPopup.isVisible()) {
				hideBlurView();
				printHistoryPopup.dismiss();
				return false;
			}
		} catch (Exception e) {
		}
		return true;
	}

	public void startReprintDocket(SaveDocketInfo saveDocketInfo) {
		Globals.reprintSaveDocketInfo = saveDocketInfo;
		Intent intent = new Intent(mContext, ReprintDocket.class);
		startActivityForResult(intent, Constants.REQUEST_REPRINT_DOCKET);
	}

	private void startPrinting() {
		Globals.BLUETOOTH_STATE = "";
		checkBluetoothState();
		progressDialog = ProgressDialog.show(mContext, "",
				connecting_to_printer);

		timer = new Timer();
		handler = new Handler();
		time = Constants.connAttemptsTym;
		timerTask = new TimerTask() {

			@Override
			public void run() {
				handler.post(new Runnable() {
					@Override
					public void run() {

						if (!Globals.BLUETOOTH_STATE
								.equalsIgnoreCase(connecting_to_printer)) {

							if (isDeviceConnected()) {
								Globals.selectedPrint = true;
								Globals.isPrint = true;
								Globals.isPause = true;
								printInBackground = new PrintInBackground()
										.execute();
								time += Constants.maxConnAttemptsTym;
							}

							if (time > Constants.maxConnAttemptsTym) {
								if (progressDialog != null) {
									progressDialog.dismiss();
									progressDialog = null;
								}
								timer.cancel();

							} else
								time = time * 2;

						}

					}
				});

			}

		};
		timer.schedule(timerTask, Constants.PRINT_SLEEP_TIME, time);

	}

	class PrintInBackground extends AsyncTask {
		private int printCount = 0;

		@Override
		protected void onPreExecute() {
			Log.d(Constants.LOG_TAG, "Printing Starting");
		}

		@Override
		protected Object doInBackground(Object... arg0) {
			try {

				ArrayList<DocketInfo> dockets = Globals.selectedDocketList;
				boolean cancel = false;
				for (DocketInfo docket : dockets) {
					List<String> labels = FormattedLabel.getFormattedLabel(
							OnLineScreen.this, docket);
					for (String label : labels) {
						// Globals.currentPrintStatus = "Printing " +
						// ++currLabel + "/" + numLabels;
						// Log.d("REVA", "Value of Cancel Print: " +
						// Globals.cancelPrint);
						if (!Globals.cancelPrint && !Globals.pausePrint) {
							if (Globals.resumePrint
									&& printCount < Globals.pausePrintAt) {
								// do nothing
							} else {
								Log.d(Constants.LOG_TAG, label);

								while (!Globals.cancelPrint
										&& !Globals.pausePrint) {
									if (print(label.getBytes())) {
										Log.d(Constants.LOG_TAG, "Print Break");
										break;

									}
								}

							}
							printCount++;
						} else if (Globals.cancelPrint) {
							Log.d(Constants.LOG_TAG, "Printing was CANCELLED");
							cancel = true;
							Globals.cancelPrint = false;
							// Globals.pausePrint = false;
							break;
						} else if (Globals.pausePrint) {
							Log.d(Constants.LOG_TAG, "Printing was PAUSED");
							cancel = true;
							Globals.pausePrintAt = printCount;
							// Globals.pausePrint = false;
							break;
						}
						try {
							Thread.sleep(Constants.PRINT_SLEEP_TIME);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					if (cancel) {
						Log.d(Constants.LOG_TAG, "Printing was CANCELLED2");
						break;
					}
					// mCommandService.stop();
					SaveDocketInfo.saveInHistory(mContext, docket);
				}

			} catch (Exception e) {

			}
			return null;
		}

		@Override
		protected void onPostExecute(Object result) {
			Log.d(Constants.LOG_TAG, "Printing FINISHED");
			if (printCount == Utils.getLabelCount(Globals.selectedDocketList)) {
				Globals.selectedPrint = false;
				Globals.isPrint = false;
				Globals.pausePrint = false;
				Globals.resumePrint = false;
				Log.d(Constants.LOG_TAG, "Printed All Dockets");
			}
			if (printCount < Utils.getLabelCount(Globals.selectedDocketList)
					&& Globals.pausePrint) {
				Globals.isPrint = true;
				Globals.isPause = false;
				// Globals.pausePrint = false;
			}
			// Globals.printing = false;
			// Globals.currentPrintStatus = "";
			super.onPostExecute(result);
		}

	}

	public void startDeviceList() {
		if (findDeviceList == null) {
			findDeviceList = new Intent(mContext, DeviceList.class);
			startActivityForResult(findDeviceList,
					Constants.REQUEST_PAIRED_DEVICE);
		} else {
			Log.d(Constants.LOG_TAG, "already show device list");
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == Constants.REQUEST_ENABLE_BT) {
			checkBluetoothState();
		}
		if (requestCode == Constants.REQUEST_PAIRED_DEVICE) {
			if (resultCode == RESULT_OK) {
				if (Globals.selectedDevice != null) {
					Log.d(Constants.LOG_TAG, Globals.selectedDevice.toString());
					mCommandService.connect(Globals.selectedDevice);
					showPrinterOption();
				} else {
					String state = getApplicationContext().getResources()
							.getString(R.string.title_not_connected);
					Globals.BLUETOOTH_STATE = state;
				}

			}
		}
		if (requestCode == Constants.REQUEST_DISCOVERABLE_BT) {
			if (resultCode == RESULT_OK) {
				checkBluetoothState();
			} else {

			}
		}

		if (requestCode == Constants.REQUEST_REPRINT_DOCKET) {
			if (resultCode == RESULT_OK) {
				// GatiGlobals.isReprint = true;
				checkAndPrint(null);
			} else {

			}
		}

	}

	private void checkAndPrint(final PrintDocketInfo printDocketInfo) {
		Globals.pausePrintAt = 0;
		Globals.BLUETOOTH_STATE = "";
		checkBluetoothState();
		final String connecting_to_printer = mContext.getResources().getString(
				R.string.connecting_to_printer);

		progressDialog = ProgressDialog.show(mContext, "",
				connecting_to_printer);

		timer = new Timer();
		handler = new Handler();
		time = Constants.connAttemptsTym;
		timerTask = new TimerTask() {
			@Override
			public void run() {
				handler.post(new Runnable() {
					@Override
					public void run() {
						if (!Globals.BLUETOOTH_STATE
								.equalsIgnoreCase(connecting_to_printer)) {

							if (isDeviceConnected()) {
								Globals.isPrint = true;
								Globals.pausePrint = false;
								Globals.cancelPrint = false;
								Globals.resumePrint = true;
								Globals.selectedPrint = true;

								if (printDocketInfo != null) {
									Globals.printDocketInfos.clear();
									Globals.printDocketInfos
											.add(printDocketInfo);
								}

								printInBackground();

								time += Constants.maxConnAttemptsTym;
							}

							if (time > Constants.maxConnAttemptsTym) {
								if (progressDialog != null) {
									progressDialog.dismiss();
									progressDialog = null;
								}
								timer.cancel();

							} else
								time = time + Constants.connAttemptsTym;
						}
					}
				});

			}

		};
		timer.schedule(timerTask, Constants.PRINT_SLEEP_TIME, time);

	}

	public void printInBackground() {
		new PrintInBackgroundR(this).execute();
	}

	public void showPrinterOption() {

		String lastPrinter = dataSource.shardPreferences
				.getValue(Constants.PRINTER_TYPE);
		if (lastPrinter != null && lastPrinter.length() > 0) {
			for (int i = 0; i < Globals.printerArr.length; i++) {
				if (lastPrinter.equalsIgnoreCase(Globals.printerArr[i])) {
					Globals.selectedPrinterPos = i;
					break;
				}
			}
		}

		AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
		builder.setTitle(getResources().getString(R.string.whichPrinterTitel));
		builder.setCancelable(false);
		builder.setSingleChoiceItems(Globals.printerArr,
				Globals.selectedPrinterPos,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int pos) {
						Globals.selectedPrinterPos = pos;
						dataSource.shardPreferences.set(Constants.PRINTER_TYPE,
								Globals.printerArr[Globals.selectedPrinterPos]);
						dialog.dismiss();
					}
				});

		builder.show();

	}

	public void setSelectedDevice(BluetoothAdapter mBtAdapter) {

		if (Globals.selectedDevice == null) {
			String dev = dataSource.shardPreferences
					.getValue(Constants.PRINTER_TYPE);
			Log.d(Constants.LOG_TAG, "Device " + dev);
			if (dev == null || dev.length() == 0)
				return;

			Set<BluetoothDevice> pairedDevices = mBtAdapter.getBondedDevices();

			if (pairedDevices.size() > 0) {
				for (BluetoothDevice device : pairedDevices) {
					if (device != null) {
						if (device.getAddress().equals(dev))
							Globals.selectedDevice = device;
					}
				}
			}

			// Log.d(Constants.LOG_TAG, "Selected Device "
			// + Globals.selectedDevice.getAddress());

		}

	}

	public boolean isBluetoothEnabled() {
		boolean isEnabled = false;

		bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		if (mCommandService == null) {
			mCommandService = new BluetoothService(mContext, mHandler);
		} else if (mCommandService != null) {
			if (mCommandService.getState() == BluetoothService.STATE_NONE) {
				mCommandService.start();
			}
		}
		setSelectedDevice(bluetoothAdapter);

		if (bluetoothAdapter == null) {
			Toast.makeText(mContext,
					getResources().getString(R.string.bt_not_support),
					Toast.LENGTH_LONG).show();
		} else if (!bluetoothAdapter.isEnabled()) {
			Intent enableBtIntent = new Intent(
					BluetoothAdapter.ACTION_REQUEST_ENABLE);
			startActivityForResult(enableBtIntent, Constants.REQUEST_ENABLE_BT);
		} else
			isEnabled = true;

		return isEnabled;
	}

	public boolean isBluetoothDiscovering() {
		boolean isDiscovering = false;

		if (bluetoothAdapter.isDiscovering()) {
			// Toast.makeText(
			// mContext,
			// getResources().getString(
			// R.string.bt_device_discovery),
			// Toast.LENGTH_LONG).show();

			Toast.makeText(mContext,
					getResources().getString(R.string.bt_enabled),
					Toast.LENGTH_SHORT).show();
			Intent enableBtIntent = new Intent(
					BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
			startActivityForResult(enableBtIntent,
					Constants.REQUEST_DISCOVERABLE_BT);
			isDiscovering = true;
		}

		return isDiscovering;

	}

	public void checkBluetoothState() {
		String state = getResources().getString(R.string.connecting_to_printer);
		Globals.BLUETOOTH_STATE = state;
		if (isBluetoothEnabled()) {
			if (!isBluetoothDiscovering()) {
				// Toast.makeText(mContext,
				// getResources().getString(R.string.bt_enabled),
				// Toast.LENGTH_LONG).show();

				if (Globals.selectedDevice != null) {
					if (BluetoothService.STATE_CONNECTED != mCommandService
							.getState()) {
						mCommandService.connect(Globals.selectedDevice);
						Log.d(Constants.LOG_TAG, state);
					} else {
						// mCommandService.connected(mCommandService.getBluetoothSocket(Globals.selectedDevice),Globals.selectedDevice);
						state = getApplicationContext().getResources()
								.getString(R.string.title_connected_to);
						Globals.BLUETOOTH_STATE = state;
					}
				} else {
					startDeviceList();
					Globals.BLUETOOTH_STATE = mContext.getResources()
							.getString(R.string.connect_devices);
				}
			}
		}
	}

	private final Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			String state = "";
			switch (msg.what) {
			case MESSAGE_STATE_CHANGE:
				switch (msg.arg1) {
				case BluetoothService.STATE_CONNECTED:
					// mTitle.setText(R.string.title_connected_to);
					// mTitle.append(mConnectedDeviceName);
					state = getApplicationContext().getResources().getString(
							R.string.title_connected_to);

					break;
				case BluetoothService.STATE_CONNECTING:
					// mTitle.setText(R.string.title_connecting);
					state = getApplicationContext().getResources().getString(
							R.string.title_connecting);

					break;
				case BluetoothService.STATE_LISTEN:
				case BluetoothService.STATE_NONE:
					state = getApplicationContext().getResources().getString(
							R.string.title_not_connected);

					Toast.makeText(getApplicationContext(),
							state + mConnectedDeviceName, Toast.LENGTH_SHORT)
							.show();
					break;
				}
				break;
			case MESSAGE_DEVICE_NAME:
				// save the connected device's name
				state = getApplicationContext().getResources().getString(
						R.string.title_connected_to);

				mConnectedDeviceName = msg.getData().getString(DEVICE_NAME);
				Toast.makeText(getApplicationContext(),
						state + mConnectedDeviceName, Toast.LENGTH_SHORT)
						.show();
				break;
			case MESSAGE_TOAST:
				Toast.makeText(getApplicationContext(),
						msg.getData().getString(TOAST), Toast.LENGTH_SHORT)
						.show();
				break;
			}

			Globals.BLUETOOTH_STATE = state;
		}
	};

	@Override
	protected void onDestroy() {
		super.onDestroy();
		try {
			if (mCommandService != null)
				mCommandService.stop();
		} catch (Exception e) {
		}
	}

	protected boolean isDeviceConnected() {
		if (mCommandService.isConnected())
			return true;
		else
			return false;
	}

	protected boolean print(byte[] data) {

		Log.d(Constants.LOG_TAG, "Print try");

		if (mCommandService.getState() != BluetoothService.STATE_CONNECTED
				&& mCommandService.getState() != BluetoothService.STATE_CONNECTING) {
			// // mCommandService.connect(GatiGlobals.selectedDevice);
			// Log.d(GatiConstants.LOG_TAG, "Connecting to printer  ");
			// Toast.makeText(mContext,
			// getResources().getString(R.string.connToprinter),
			// Toast.LENGTH_SHORT).show();
			// try {
			// Thread.sleep(GatiConstants.PRINT_SLEEP_TIME * 3);
			// } catch (Exception e) {
			//
			// }
		} else {
			Log.d(Constants.LOG_TAG, "Printer was already connected");
		}
		if (mCommandService.write(data))
			return true;
		else
			return false;

	}

}
