package com.revamobile.pagescan;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.revamobile.gatiscan.datacache.DataSource;
import com.revamobile.gatiscan.utils.Constants;
import com.revamobile.gatiscan.utils.Utils;

@SuppressLint("all")
public class SplashScreen extends Activity {
	private Context context;
	private ImageView splashImage;

	private AsyncTask task;
	private boolean isAnimEnded = false;

	private DataSource dataSource;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash_screen);

		context = SplashScreen.this;

		dataSource = new DataSource(context);

		setAllStrings();

		Utils.getDisplaySize();

		splashImage = (ImageView) findViewById(R.id.iv_splash);

		Animation animation = AnimationUtils.loadAnimation(context,
				R.anim.splash_screen);
		splashImage.setVisibility(View.VISIBLE);
		splashImage.setAnimation(animation);

		task = new AsyncTaskProgress().execute();
		isAnimEnded = true;
		animation.setAnimationListener(new AnimationListener() {

			@Override
			public void onAnimationStart(Animation animation) {
				isAnimEnded = false;
			}

			@Override
			public void onAnimationRepeat(Animation animation) {

			}

			@Override
			public void onAnimationEnd(Animation animation) {
				isAnimEnded = true;
				if (task == null)
					goNext();
			}
		});

	}

	public class AsyncTaskProgress extends AsyncTask<Void, Integer, Void> {
		int progress;

		@Override
		protected void onPreExecute() {
			progress = 0;
		}

		@Override
		protected Void doInBackground(Void... params) {
			try {

			} catch (Exception e) {
			}

			while (progress < Constants.maxProgressRange) {
				progress = progress + 5;
				publishProgress(progress);
				SystemClock.sleep(100);
			}

			return null;
		}

		@SuppressWarnings("deprecation")
		@Override
		protected void onPostExecute(Void result) {
			goNext();
			task = null;
		}
	}

	private void goNext() {
		if (isAnimEnded) {
			Intent intent;
			String user = dataSource.shardPreferences
					.getValue(Constants.USERNAME_PREF);
			String pwd = dataSource.shardPreferences
					.getValue(Constants.PASSWORD_PREF);

			if (Utils.isValidString(user) && Utils.isValidString(pwd)) {
				intent = new Intent(context, HomeScreen.class);
			} else
				intent = new Intent(context, LoginScreen.class);

			startActivity(intent);
			finish();
		}
	}

	public void setAllStrings() {

		Resources res = getResources();

		Constants.NETWORK_UNAVAILABLE = res.getString(R.string.network_error);

		Constants.PROBLEM_WITH_NETWORK = res
				.getString(R.string.problem_with_network);

		Constants.SERVER_ERROR = res.getString(R.string.server_error);
		Constants.SERVER_NOT_REACHABLE = res
				.getString(R.string.server_not_reachable);

		Constants.PARSEDISPMSG = res.getString(R.string.parse_display_msg);
		Constants.PARSEDETMSG = res.getString(R.string.parse_detailed_msg);

		Constants.DEVICE_CONNECTIVITY = res
				.getString(R.string.device_connectivity);

	}
}
