package com.revamobile.pagescan;

import net.sourceforge.zbar.Config;
import net.sourceforge.zbar.Image;
import net.sourceforge.zbar.ImageScanner;
import net.sourceforge.zbar.Symbol;
import net.sourceforge.zbar.SymbolSet;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.hardware.Camera;
import android.hardware.Camera.AutoFocusCallback;
import android.hardware.Camera.PreviewCallback;
import android.hardware.Camera.Size;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ToggleButton;

import com.revamobile.gati.custom.CameraPreview;

public class ScanDocket extends Activity implements SensorEventListener {

	public static final String scannedTag = "ScannedTag";

	private static final String y888 = "Y800";

	private Camera camera;
	private CameraPreview preview;
	private Handler autoFocusHandler;

	private String scannedVal;

	private ImageScanner scanner;

	private boolean previewing = true;

	private Button scanCancel;

	private ToggleButton flash_tbtn;

	private boolean isProcessing;

	private SensorManager mSensorManager;
	private Sensor mLight;
	public static final int LIGHT_THRESHOLD = 15;

	private boolean isFlash;

	static {
		System.loadLibrary("iconv");
	}

	@SuppressWarnings("deprecation")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

		setContentView(R.layout.scan_dkt);

		previewing = false;
		scanner = null;

		preview = new CameraPreview(this,
				(SurfaceView) findViewById(R.id.cameraPreview));
		preview.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT,
				LayoutParams.FILL_PARENT));
		((FrameLayout) findViewById(R.id.preview)).addView(preview);
		preview.setKeepScreenOn(true);
		setCamera();

		// isFlash = getPackageManager().hasSystemFeature(
		// PackageManager.FEATURE_CAMERA_FLASH);

		// flash_tbtn = (ToggleButton) findViewById(R.id.flash_tbtn);
		// if (isFlash)
		// flash_tbtn.setVisibility(View.VISIBLE);
		// else
		// flash_tbtn.setVisibility(View.GONE);
		//
		// flash_tbtn
		// .setOnCheckedChangeListener(new
		// CompoundButton.OnCheckedChangeListener() {
		// @Override
		// public void onCheckedChanged(CompoundButton buttonView,
		// boolean isChecked) {
		// if (preview != null) {
		// previewing = false;
		// preview.flash(isChecked);
		// previewing = true;
		// }
		//
		// }
		// });

		scanCancel = (Button) findViewById(R.id.scan_cancel_btn);
		scanCancel.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				close(false);
			}
		});

		mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		mLight = mSensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
		if (mLight != null)
			Log.d("IMAGECAPTURE",
					"sensor max range " + mLight.getMaximumRange());

	}

	@Override
	public final void onSensorChanged(SensorEvent event) {
		float lux = event.values[0];
		if (lux < LIGHT_THRESHOLD) {
			if (preview != null && isFlash) {
				previewing = false;
				preview.flash(true);
				previewing = true;
				flash_tbtn.setChecked(true);
			}
		} else {
			if (preview != null && isFlash) {
				previewing = false;
				preview.flash(false);
				previewing = true;
				flash_tbtn.setChecked(false);
			}
		}
	}

	private void setCamera() {
		if (preview != null && camera == null) {
			releaseCamera();
			camera = Camera.open();
			if (camera != null) {
				preview.setCamera(camera, previewCb, autoFocusCB);
				autoFocusHandler = new Handler();
				previewing = true;

			} else
			// Utils.logE("Camera null");

			/* Instance barcode scanner */
			if (scanner == null) {
				scanner = new ImageScanner();
				scanner.setConfig(0, Config.X_DENSITY, 3);
				scanner.setConfig(0, Config.Y_DENSITY, 3);
			}

		}
	}

	private void releaseCamera() {
		if (camera != null) {
			previewing = false;
			autoFocusHandler = null;
			scanner = null;
			preview.setCamera(camera, null, null);
			camera.stopPreview();
			preview.setCamera(null, null, null);
			camera.release();
			camera = null;
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		setCamera();
		mSensorManager.registerListener(this, mLight,
				SensorManager.SENSOR_DELAY_NORMAL);
	}

	@Override
	protected void onPause() {
		releaseCamera();
		super.onPause();
		mSensorManager.unregisterListener(this);
	}

	// private CameraPreview.FlashSupport flashSupport = new
	// CameraPreview.FlashSupport() {
	// @Override
	// public void flash(boolean isFlash) {
	// if (isFlash)
	// flash_tbtn.setVisibility(View.VISIBLE);
	// else
	// flash_tbtn.setVisibility(View.GONE);
	// }
	// };

	private AutoFocusCallback autoFocusCB = new AutoFocusCallback() {
		@Override
		public void onAutoFocus(boolean success, Camera camera) {
			if (previewing && autoFocusHandler != null)
				autoFocusHandler.postDelayed(doAutoFocus, 1000);
		}
	};

	private Runnable doAutoFocus = new Runnable() {
		@Override
		public void run() {
			if (previewing) {
				if (camera != null && autoFocusCB != null)
					camera.autoFocus(autoFocusCB);
			}
		}
	};

	private PreviewCallback previewCb = new PreviewCallback() {
		@Override
		public void onPreviewFrame(byte[] data, Camera camera) {
			// Utils.logD("Test 1");
			if (previewing && !isProcessing) {
				isProcessing = true;
				// Utils.logD("Test 2");
				Camera.Parameters parameters = camera.getParameters();

				if (parameters != null) {
					Size size = parameters.getPreviewSize();

					if (size != null) {
						Image barcode = new Image(size.width, size.height, y888);
						barcode.setData(data);

						if (scanner != null) {
							int result = scanner.scanImage(barcode);
							if (result != 0) {
								previewing = false;
								scannedVal = "";
								SymbolSet syms = scanner.getResults();
								for (Symbol sym : syms) {
									scannedVal = sym.getData();
									close(true);
									break;
								}

							}
						} else {
							scanner = new ImageScanner();
							scanner.setConfig(0, Config.X_DENSITY, 3);
							scanner.setConfig(0, Config.Y_DENSITY, 3);
						}
					}
				}
				isProcessing = false;
			}
		}
	};

	private void close(boolean barcodeScanned) {
		releaseCamera();
		Intent intent = new Intent();
		intent.putExtra(ScanDocket.scannedTag, scannedVal);
		if (barcodeScanned)
			setResult(RESULT_OK, intent);
		else
			setResult(RESULT_CANCELED, intent);
		finish();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {

		if (keyCode == KeyEvent.KEYCODE_BACK) {
			close(false);
			return true;
		} else
			return super.onKeyDown(keyCode, event);
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {

	}

}
