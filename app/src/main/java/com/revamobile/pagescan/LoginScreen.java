package com.revamobile.pagescan;

import java.io.InputStream;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.InputFilter;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.revamobile.gatiscan.data.GatiScanUtils;
import com.revamobile.gatiscan.data.LoginResponse;
import com.revamobile.gatiscan.datacache.DataSource;
import com.revamobile.gatiscan.utils.Constants;
import com.revamobile.gatiscan.utils.Globals;
import com.revamobile.gatiscan.utils.HttpRequest;
import com.revamobile.gatiscan.utils.Utils;

public class LoginScreen extends Activity {
	private Context context;

	private EditText usernameEt, passwordEt;
	private Button clearBtn, loginBtn;

	private ProgressDialog pDialog;
	private AlertDialog errDlg;

	@SuppressWarnings("rawtypes")
	private AsyncTask login;

	private TextView versionTopTv;

	private DataSource dataSource;

	private String versionVal = "";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login_screen);

		context = LoginScreen.this;

		dataSource = new DataSource(context);

		clearBtn = (Button) findViewById(R.id.clear);
		loginBtn = (Button) findViewById(R.id.login);

		usernameEt = (EditText) findViewById(R.id.et_username);
		usernameEt.setFilters(new InputFilter[] { new InputFilter.AllCaps() });

		passwordEt = (EditText) findViewById(R.id.et_password);

		// usernameEt.addTextChangedListener(usernameWatcher);
		// passwordEt.addTextChangedListener(passwordWatcher);

		getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

		clearBtn.setOnClickListener(onClickListener);
		loginBtn.setOnClickListener(onClickListener);

		versionTopTv = (TextView) findViewById(R.id.tv_version_top);

		try {
			versionVal = getPackageManager()
					.getPackageInfo(getPackageName(), 0).versionName;
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}

		if (Utils.isValidString(versionVal)) {
			versionTopTv.setText("Ver : " + versionVal);
		}

	}

	private View.OnClickListener onClickListener = new View.OnClickListener() {

		@Override
		public void onClick(View v) {
			int id = v.getId();
			switch (id) {
			case R.id.clear:
				usernameEt.setText("");
				passwordEt.setText("");

				break;
			case R.id.login:

				Globals.username = usernameEt.getText().toString().trim();
				Globals.password = passwordEt.getText().toString().trim();

				// if (Globals.username == null || Globals.username.length() ==
				// 0) {
				// usernameEt.setText("");
				// return;
				// }
				// if (Globals.password == null || Globals.password.length() ==
				// 0) {
				// passwordEt.setText("");
				// return;
				// }

				if (Utils.isValidString(Globals.username)
						&& Utils.isValidString(Globals.password)) {

					login = new Login().execute();
				} else {
					if (!Utils.isValidString(Globals.username)
							&& Utils.isValidString(Globals.password))
						Utils.showSimpleAlert(context, "Please enter username");
					else if (Utils.isValidString(Globals.username)
							&& !Utils.isValidString(Globals.password))
						Utils.showSimpleAlert(context, "Please enter password");
					else
						Utils.showSimpleAlert(context,
								"Please enter username and password");
				}

				break;

			default:
				break;
			}
		}
	};

	// private TextWatcher usernameWatcher = new TextWatcher() {
	// @Override
	// public void onTextChanged(CharSequence s, int start, int before,
	// int count) {
	// if (s == null || s.length() == 0) {
	// usernameEt
	// .setError(URLDecoder.decode(Constants.USERNAME_ERROR));
	// }
	// }
	//
	// @Override
	// public void beforeTextChanged(CharSequence s, int start, int count,
	// int after) {
	//
	// }
	//
	// @Override
	// public void afterTextChanged(Editable s) {
	//
	// }
	// };
	//
	// private TextWatcher passwordWatcher = new TextWatcher() {
	// @Override
	// public void onTextChanged(CharSequence s, int start, int before,
	// int count) {
	// if (s == null || s.length() == 0)
	// passwordEt
	// .setError(URLDecoder.decode(Constants.PASSWORD_ERROR));
	//
	// }
	//
	// @Override
	// public void beforeTextChanged(CharSequence s, int start, int count,
	// int after) {
	//
	// }
	//
	// @Override
	// public void afterTextChanged(Editable s) {
	//
	// }
	// };

	class Login extends AsyncTask<Object, Object, Object> {
		@Override
		protected void onPreExecute() {
			Globals.lastErrMsg = "";
			showProgressDialog();
		}

		@Override
		protected Object doInBackground(Object... arg0) {

			try {

				Globals.lastErrMsg = "";

				TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
				String imei = telephonyManager.getDeviceId();

				String url = GatiScanUtils.getNewLoginUrl(Globals.username,
						Globals.password, imei, versionVal);
				InputStream is = HttpRequest.getInputStreamFromUrl(url);

				if (is != null) {
					LoginResponse response = (LoginResponse) Utils.parse(is,
							LoginResponse.class);
					if (response != null
							&& response.getResult().contains(
									Constants.SUCCESSFUL)) {
						dataSource.shardPreferences.set(
								Constants.USERNAME_PREF, Globals.username);
						dataSource.shardPreferences.set(
								Constants.PASSWORD_PREF, Globals.password);
						dataSource.shardPreferences.set(Constants.OUCODE_PREF,
								response.getOucode());

					} else if (response.getResult().contains(Constants.FAILED)) {
						Globals.lastErrMsg = response.getsErrMsg();
					}

				}

			} catch (Exception e) {
				if (!Utils.isValidString(Globals.lastErrMsg))
					Globals.lastErrMsg = e.toString();
			}
			return null;

		}

		@Override
		protected void onPostExecute(Object result) {

			if (showErrorDialog() && pDialog != null && pDialog.isShowing()) {
				pDialog.dismiss();
				goNext();

			}

			super.onPostExecute(result);
		}

	}

	@SuppressWarnings("deprecation")
	private void showProgressDialog() {
		pDialog = new ProgressDialog(context);
		pDialog.setTitle(getString(R.string.loading));
		pDialog.setMessage(getString(R.string.progress_dialog_title));
		pDialog.setIndeterminate(true);
		pDialog.setCancelable(false);
		pDialog.setButton(getString(R.string.cancel_btn),
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						login.cancel(true);
						dialog.dismiss();
					}

				});
		pDialog.show();
	}

	@SuppressWarnings("deprecation")
	private boolean showErrorDialog() {
		boolean isNotErr = true;
		if (Globals.lastErrMsg != null && Globals.lastErrMsg.length() > 0) {
			errDlg = new AlertDialog.Builder(context).create();
			errDlg.setTitle(getString(R.string.alert_dialog_title));
			errDlg.setCancelable(false);
			errDlg.setMessage(Globals.lastErrMsg);
			Globals.lastErrMsg = "";
			errDlg.setButton(getString(R.string.ok_btn),
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							errDlg.dismiss();
						}
					});
			if (pDialog != null && pDialog.isShowing())
				pDialog.dismiss();
			isNotErr = false;
			errDlg.show();
		}
		return isNotErr;
	}

	private void goNext() {

		Intent intent = new Intent(context, HomeScreen.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
				| Intent.FLAG_ACTIVITY_NEW_TASK);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
				| Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);

		finish();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		return true;
	}

	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			finish();
			return true;
		}
		return super.onKeyUp(keyCode, event);
	}

}
