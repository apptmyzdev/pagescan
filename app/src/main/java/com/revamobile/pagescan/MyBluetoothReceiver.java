package com.revamobile.pagescan;

import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.revamobile.gatiscan.utils.Utils;

public class MyBluetoothReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		String action = intent.getAction();

		// When discovery finds a device
		if (BluetoothDevice.ACTION_ACL_CONNECTED.equals(action)) {
			// Get the BluetoothDevice object from the Intent
			BluetoothDevice device = intent
					.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

			Utils.logD("Connected : " +device.getAddress());
		} else if (BluetoothDevice.ACTION_ACL_DISCONNECTED.equals(action)) {
			BluetoothDevice device = intent
					.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
			Utils.logD("DisConnected : " +device.getAddress());
			Utils.logD(device.getAddress());
		}
	}

}
