package com.revamobile.pagescan;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.revamobile.gatiscan.data.DocketDetailsResponse;
import com.revamobile.gatiscan.data.DocketToScan;
import com.revamobile.gatiscan.data.DocketsToScan;
import com.revamobile.gatiscan.data.GatiScanUtils;
import com.revamobile.gatiscan.data.ScanDocket;
import com.revamobile.gatiscan.datacache.DataSource;
import com.revamobile.gatiscan.utils.Constants;
import com.revamobile.gatiscan.utils.Globals;
import com.revamobile.gatiscan.utils.HttpRequest;
import com.revamobile.gatiscan.utils.Utils;

public class ScanScreen extends Activity {
	private Context context;

	public EditText scannedHunEt, scannedPktEt;
	private Button autoHunBtn, autoPktBtn, doneBtn;
	private Button manualHunBtn, manualPktBtn;
	private ListView scannedLv;

	private TextView logout;

	private Timer timerHun = null, timerPkt = null;
	private TimerTask timerTaskHun, timerTaskPkt;
	private Handler handlerHun, handlerPkt;
	private int timeHun, timePkt;
	private int repetitionHun, repetitionPkt;

	private MyAdapter adapter;

	public ArrayList<String> scannedHunList = new ArrayList<String>();
	public ArrayList<String> scannedPktList = new ArrayList<String>();

	public ArrayList<ScanDocket> scanDockets = new ArrayList<ScanDocket>();

	private ProgressDialog pDialog;
	private AlertDialog errDlg;

	// private int hunPos = -1;

	private TextView versionTopTv, userNameTv;

	private String versionVal = "";

	private DataSource dataSource;

	private String lastHun = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.scan_screen);

		context = ScanScreen.this;

		dataSource = new DataSource(context);

		scannedHunEt = (EditText) findViewById(R.id.et_scanned_hun);
		setScanTextChangedListenerHun(false);

		scannedPktEt = (EditText) findViewById(R.id.et_scanned_pkt);
		setScanTextChangedListenerPkt(true);

		// scanToFocus();

		autoHunBtn = (Button) findViewById(R.id.btn_auto_hun);
		autoHunBtn.setOnClickListener(listener);

		autoPktBtn = (Button) findViewById(R.id.btn_auto_pkt);
		autoPktBtn.setOnClickListener(listener);

		manualHunBtn = (Button) findViewById(R.id.btn_manual_hun);
		manualHunBtn.setOnClickListener(listener);

		manualPktBtn = (Button) findViewById(R.id.btn_manual_pkt);
		manualPktBtn.setOnClickListener(listener);

		doneBtn = (Button) findViewById(R.id.btn_done);
		doneBtn.setOnClickListener(listener);

		scannedLv = (ListView) findViewById(R.id.lv_scanned);

		logout = (TextView) findViewById(R.id.tv_logout);
		logout.setVisibility(View.VISIBLE);
		logout.setOnClickListener(Utils.logoutListener);

		getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

		adapter = new MyAdapter(context, R.layout.scanned_item_child,
				scanDockets);
		scannedLv.setAdapter(adapter);

		versionTopTv = (TextView) findViewById(R.id.tv_version_top);

		try {
			versionVal = getPackageManager()
					.getPackageInfo(getPackageName(), 0).versionName;
			if (Utils.isValidString(versionVal))
				versionTopTv.setText("Ver : " + versionVal);
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}

		userNameTv = (TextView) findViewById(R.id.tv_username);
		String user = dataSource.shardPreferences
				.getValue(Constants.USERNAME_PREF);
		if (Utils.isValidString(user))
			userNameTv.setText(user);

		int size = dataSource.docketsData.getDktsSize(Globals.custCode,
				Globals.custVentCode);
		if (size == 0)
			new DataTask().execute();
		else {
			reloadData();
			startScan();
		}

	}

	private View.OnClickListener listener = new View.OnClickListener() {

		@Override
		public void onClick(View v) {

			switch (v.getId()) {
			case R.id.btn_auto_hun:
				scannedHunEt.setText("");
				setScanTextChangedListenerHun(false);
				Utils.playAudioList(context, R.raw.ready_to_scan);
				break;

			case R.id.btn_auto_pkt:
				scannedPktEt.setText("");
				setScanTextChangedListenerPkt(false);
				Utils.playAudioList(context, R.raw.ready_to_scan);
				break;

			case R.id.btn_done:
				int h1 = dataSource.docketsData.getHunsSize(Globals.custCode,
						Globals.custVentCode);
				int p1 = dataSource.docketsData.getPktsSize(Globals.custCode,
						Globals.custVentCode);

				int h2 = dataSource.scannedData.getHunsSize(Globals.custCode,
						Globals.custVentCode);
				int p2 = dataSource.scannedData.getPktsSize(Globals.custCode,
						Globals.custVentCode);

				// if ((scannedHunList.size() == Globals.hunList.size())
				// && (scannedPktList.size() == Globals.pktList.size())) {
				// if ((h1 == h2) && (p1 == p2)) {
				if(h2 != 0 && p2 != 0) {
				if (h2 == p2) {
					Intent intent = new Intent(context, Reconcile.class);
					startActivity(intent);
				} else {
//					Utils.showSimpleAlert(context,
//							"Please scan all the HUN's and Packets to submit");
					Utils.showSimpleAlert(context,
							"Please scan respective Packets of HUN's to submit");
				}
				} else {
					Utils.showSimpleAlert(context,
							"Please scan atleast one packet and HUN to submit");
				}
				break;

			case R.id.btn_manual_hun:
				isHun = true;
				enterDocketNumber();
				break;

			case R.id.btn_manual_pkt:
				isHun = false;
				enterDocketNumber();
				break;

			default:
				break;
			}

		}
	};

	private View inputDocketView;
	private EditText docketNum;
	private TextView inputDktMsg;
	private boolean isHun = true;

	public void enterDocketNumber() {

		// setScanTextChangedListener(true);

		LayoutInflater factory = LayoutInflater.from(context);

		inputDocketView = factory.inflate(R.layout.input_packet, null);
		docketNum = (EditText) inputDocketView
				.findViewById(R.id.et_input_packet);
		docketNum.addTextChangedListener(scanDktWatcher);
		int maxLength;
		if (isHun)
			maxLength = 10;
		else
			maxLength = 9;
		InputFilter[] fArray = new InputFilter[1];
		fArray[0] = new InputFilter.LengthFilter(maxLength);
		docketNum.setFilters(fArray);

		inputDktMsg = (TextView) inputDocketView
				.findViewById(R.id.tv_input_packet);

		inputDktMsg.setText("Enter");

		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setView(inputDocketView);

		builder.setTitle("Popup");

		builder.setPositiveButton(getString(R.string.cancel_btn),
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						if (isHun)
							checkScanHun("", true);
						else
							checkScanPacket("", true);
					}
				});
		builder.setNegativeButton(getString(R.string.ok_btn),
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						String scannedValue = docketNum.getText().toString();
						if (isHun)
							checkScanHun(scannedValue, false);
						else
							checkScanPacket(scannedValue, false);
					}
				});
		AlertDialog alertDialog = builder.create();
		alertDialog.setCancelable(false);
		alertDialog.show();

	}

	private TextWatcher scanDktWatcher = new TextWatcher() {
		boolean isVaildPattern = true;

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			String scannedValue = s.toString();
			if (scannedValue != null && scannedValue.length() > 0) {
				if (scannedValue.charAt(0) == '0') {
					docketNum.setText("");
				}
				if (isHun) {
					if (scannedValue.length() < Constants.scanStringLengthHun)
						scannedHunEt.setText(scannedValue.substring(0,
								scannedValue.length()));
				} else {
					if (scannedValue.length() < Constants.scanStringLength)
						scannedPktEt.setText(scannedValue.substring(0,
								scannedValue.length()));
				}

				Pattern ps = Pattern.compile(Constants.SCANNING_CHAR_PATTERN);
				Matcher ms = ps.matcher(scannedValue.toString());
				isVaildPattern = ms.matches();
			}
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			if (isHun)
				setScanTextChangedListenerHun(true);
			else
				setScanTextChangedListenerPkt(true);
		}

		@Override
		public void afterTextChanged(Editable s) {
			String scannedValue = docketNum.getText().toString();
			if (scannedValue != null) {
				scannedValue = scannedValue.trim();
				if (isHun) {
					if (scannedValue.length() != Constants.scanStringLengthHun)
						docketNum.setError("Length should be 10");
					else if (!isVaildPattern) {
						docketNum
								.setError("Entered value is not in correct format");
					}
				} else {
					if (scannedValue.length() != Constants.scanStringLength)
						docketNum.setError("Length should be 9");
					else if (!isVaildPattern) {
						docketNum
								.setError("Entered value is not in correct format");
					}
				}

			}
		}
	};

	private void scanToFocusHun() {
		if (scannedHunEt != null) {

			Utils.dissmissKeyboard(scannedHunEt);
			scannedHunEt.requestFocus();
			scannedHunEt.setFocusable(true);
			scannedHunEt.setFocusableInTouchMode(true);

		}
	}

	private void scanToFocusPkt() {
		if (scannedPktEt != null) {

			Utils.dissmissKeyboard(scannedPktEt);
			scannedPktEt.requestFocus();
			scannedPktEt.setFocusable(true);
			scannedPktEt.setFocusableInTouchMode(true);

		}
	}

	private boolean isAddScanTextChangedListenerHun = false;

	public void setScanTextChangedListenerHun(boolean remove) {
		if (scannedHunEt != null) {
			if (remove) {
				if (isAddScanTextChangedListenerHun) {
					scannedHunEt.removeTextChangedListener(scanHunWatcher);
					isAddScanTextChangedListenerHun = false;
				}
			} else {
				if (!isAddScanTextChangedListenerHun) {
					scannedHunEt.addTextChangedListener(scanHunWatcher);
					isAddScanTextChangedListenerHun = true;
				}
			}

			scanToFocusHun();
		} else if (remove)
			isAddScanTextChangedListenerHun = false;
		else
			isAddScanTextChangedListenerHun = true;
	}

	private boolean isAddScanTextChangedListenerPkt = false;

	public void setScanTextChangedListenerPkt(boolean remove) {
		if (scannedPktEt != null) {
			if (remove) {
				if (isAddScanTextChangedListenerPkt) {
					scannedPktEt.removeTextChangedListener(scanPktWatcher);
					isAddScanTextChangedListenerPkt = false;
				}
			} else {
				if (!isAddScanTextChangedListenerPkt) {
					scannedPktEt.addTextChangedListener(scanPktWatcher);
					isAddScanTextChangedListenerPkt = true;
				}
			}

			scanToFocusPkt();
		} else if (remove)
			isAddScanTextChangedListenerPkt = false;
		else
			isAddScanTextChangedListenerPkt = true;
	}

	private TextWatcher scanHunWatcher = new TextWatcher() {
		@Override
		public void afterTextChanged(Editable s) {
			Utils.dissmissKeyboard(scannedHunEt);
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			setScanTextChangedListenerHun(false);
			Utils.dissmissKeyboard(scannedHunEt);
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			if (s.length() > 0 && isAddScanTextChangedListenerHun) {
				if (timerHun == null) {
					timerHun = new Timer();
					handlerHun = new Handler();
					timeHun = Constants.scanStartTimeHun;
					repetitionHun = (Constants.scanTimeHun / Constants.scanStartTimeHun);
					if (repetitionHun < 1)
						repetitionHun = 1;
					timerTaskHun = new TimerTask() {
						@Override
						public void run() {
							handlerHun.post(new Runnable() {
								@Override
								public void run() {

									if (timeHun == Constants.scanTimeHun
											|| timeHun > (Constants.scanTimeHun * repetitionHun)) {

										String scannedValue = scannedHunEt
												.getText().toString();

										checkScanHun(scannedValue, false);

										timeHun = 0;
										if (timerHun != null)
											timerHun.cancel();
										timerHun = null;
									} else
										timeHun += timeHun;

								}
							});

						}

					};
					timerHun.schedule(timerTaskHun, Constants.scanStartTimeHun,
							timeHun);
				}
			}
		}
	};

	private TextWatcher scanPktWatcher = new TextWatcher() {
		@Override
		public void afterTextChanged(Editable s) {
			Utils.dissmissKeyboard(scannedPktEt);
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			setScanTextChangedListenerPkt(false);
			Utils.dissmissKeyboard(scannedPktEt);
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			if (s.length() > 0 && isAddScanTextChangedListenerPkt) {
				if (timerPkt == null) {
					timerPkt = new Timer();
					handlerPkt = new Handler();
					timePkt = Constants.scanStartTime;
					repetitionPkt = (Constants.scanTime / Constants.scanStartTime);
					if (repetitionPkt < 1)
						repetitionPkt = 1;
					timerTaskPkt = new TimerTask() {
						@Override
						public void run() {
							handlerPkt.post(new Runnable() {
								@Override
								public void run() {

									if (timePkt == Constants.scanTime
											|| timePkt > (Constants.scanTime * repetitionPkt)) {

										String scannedValue = scannedPktEt
												.getText().toString();

										checkScanPacket(scannedValue, false);

										timePkt = 0;
										if (timerPkt != null)
											timerPkt.cancel();
										timerPkt = null;
									} else
										timePkt += timePkt;

								}
							});

						}

					};
					timerPkt.schedule(timerTaskPkt, Constants.scanStartTime,
							timePkt);
				}
			}
		}
	};

	private boolean checkScanHun(String scannedValue, boolean error) {

		boolean isVaildScannedValue = false;
		if (scannedValue == null) {
			Utils.logE("Scan value is null");
			return isVaildScannedValue;
		} else {

			Utils.releasePlayer();
			String errorMsg = "";
			if (error) {
				errorMsg = scannedValue;
			} else {

				Utils.logD("Scan value : " + scannedValue);
				long scanHun = -1;
				String scanTime = "";

				scannedValue = scannedValue.trim();

				if (scannedValue.length() > 0) {
					Utils.logD("Scan CharSequence : " + scannedValue);

					if (scannedValue.charAt(0) == '0') {
						scannedValue = scannedValue.substring(1,
								scannedValue.length());
					}

					if (scannedValue.length() == Constants.scanStringLengthHun) {
						Pattern ps = Pattern
								.compile(Constants.SCANNING_CHAR_PATTERN);
						Matcher ms = ps.matcher(scannedValue);

						if (ms.matches()) {
							try {
								scanTime = Utils.getScanTime();
								scanHun = Long.parseLong(scannedValue);
							} catch (Exception e) {
								scanHun = -1;
							}

							if (scanHun != -1) {
								if (dataSource.docketsData.existsHun(
										Globals.custCode, Globals.custVentCode,
										scannedValue)) {
									if (!dataSource.scannedData.isHunScanned(
											Globals.custCode,
											Globals.custVentCode, scannedValue)) {
										addHun(scannedValue);
										addScanHun(scannedValue, scanTime);
									} else {
										Utils.playAudioList(context,
												R.raw.its_already_scanned);
									}

								} else {
									Utils.playAudioList(context, R.raw.beep);
								}
								isVaildScannedValue = true;
							} else {
								errorMsg = "Scanned value invalid";
							}
						} else {
							errorMsg = "Scanned value invalid";
						}
					} else if (scannedValue.length() > Constants.scanStringLengthHun) {
						errorMsg = "Scanned value length is greater than 10";
					} else if (scannedValue.length() < Constants.scanStringLengthHun) {
						errorMsg = "Scanned value length is lesser than 10";
					}
				} else {
					errorMsg = "Scanned value length is zero";
				}
			}

			if (errorMsg.length() > 0) {
				Utils.playAudioList(context, R.raw.beep);
				Utils.showToast(context, errorMsg);
			}
			// Utils.showToast(context, "Scan : " + scannedValue);
			scannedHunEt.setText("");
			setScanTextChangedListenerHun(true);
			setScanTextChangedListenerPkt(false);
			scanToFocusPkt();
			// scanToFocus();
		}

		return isVaildScannedValue;
	}

	private boolean checkScanPacket(String scannedValue, boolean error) {

		boolean isVaildScannedValue = false;
		if (scannedValue == null) {
			Utils.logE("Scan value is null");
			return isVaildScannedValue;
		} else {

			Utils.releasePlayer();
			String errorMsg = "";
			if (error) {
				errorMsg = scannedValue;
			} else {

				Utils.logD("Scan value : " + scannedValue);
				int scanDkt = -1;
				String scanTime = "";

				scannedValue = scannedValue.trim();

				if (scannedValue.length() > 0) {
					Utils.logD("Scan CharSequence : " + scannedValue);

					if (scannedValue.charAt(0) == '0') {
						scannedValue = scannedValue.substring(1,
								scannedValue.length());
					}

					if (scannedValue.length() == Constants.scanStringLength) {
						Pattern ps = Pattern
								.compile(Constants.SCANNING_CHAR_PATTERN);
						Matcher ms = ps.matcher(scannedValue);

						if (ms.matches()) {
							try {
								scanTime = Utils.getScanTime();
								scanDkt = Integer.parseInt(scannedValue);
							} catch (Exception e) {
								scanDkt = -1;
							}

							if (scanDkt != -1) {
								if (dataSource.docketsData.existsPkt(
										Globals.custCode, Globals.custVentCode,
										scannedValue)) {
									if (!dataSource.scannedData.isPktScanned(
											Globals.custCode,
											Globals.custVentCode, scannedValue)) {
										addPkt(scannedValue);
										addScanPkt(scannedValue, scanTime);
									} else {
										Utils.playAudioList(context,
												R.raw.its_already_scanned);
									}

								} else {
									Utils.playAudioList(context, R.raw.beep);
								}
								isVaildScannedValue = true;
							} else {
								errorMsg = "Scanned value invalid";
							}
						} else {
							errorMsg = "Scanned value invalid";
						}
					} else if (scannedValue.length() > Constants.scanStringLength) {
						errorMsg = "Scanned value length is greater than 9";
					} else if (scannedValue.length() < Constants.scanStringLength) {
						errorMsg = "Scanned value length is lesser than 9";
					}
				} else {
					errorMsg = "Scanned value length is zero";
				}
			}

			if (errorMsg.length() > 0) {
				Utils.playAudioList(context, R.raw.beep);
				Utils.showToast(context, errorMsg);
			}

			scannedPktEt.setText("");
			setScanTextChangedListenerPkt(true);
			setScanTextChangedListenerHun(false);
			scanToFocusHun();
			// scanToFocus();
		}

		return isVaildScannedValue;
	}

	private void addHun(String item) {
		if (scannedHunList == null)
			scannedHunList = new ArrayList<String>();

		Collections.reverse(scannedHunList);

		scannedHunList.add(item);

	}

	private void addPkt(String item) {
		if (scannedPktList == null)
			scannedPktList = new ArrayList<String>();

		Collections.reverse(scannedPktList);

		scannedPktList.add(item);

	}

	// private boolean itemExistsHun(String s) {
	// if (Utils.isValidArrayList(Globals.hunList)) {
	// for (String d : Globals.hunList) {
	// if (s.equalsIgnoreCase(d)) {
	// return true;
	// }
	// }
	// }
	//
	// return false;
	// }
	//
	// private boolean itemExistsPkt(String s) {
	// if (Utils.isValidArrayList(Globals.pktList)) {
	// for (String d : Globals.pktList) {
	// if (s.equalsIgnoreCase(d)) {
	// return true;
	// }
	// }
	// }
	//
	// return false;
	// }
	//
	// private boolean isHunScanned(String hun) {
	// if (Utils.isValidArrayList(Globals.scanDockets)) {
	// for (ScanDocket d : Globals.scanDockets) {
	// if (d.getHunNo().equalsIgnoreCase(hun)) {
	// if (d.isHunScanned())
	// return true;
	// else
	// return false;
	// }
	// }
	// }
	// return false;
	// }
	//
	// private boolean isPktScanned(String pkt) {
	// if (Utils.isValidArrayList(Globals.scanDockets)) {
	// for (ScanDocket d : Globals.scanDockets) {
	// if (d.getPktNo() != null) {
	// if (d.getPktNo().equalsIgnoreCase(pkt)) {
	// if (d.isPktScanned())
	// return true;
	// else
	// return false;
	// }
	// }
	// }
	// }
	// return false;
	// }
	//
	// private int getHunPos(String hun) {
	// int pos = -1;
	// int index = 0;
	// if (Utils.isValidArrayList(Globals.scanDockets)) {
	// for (ScanDocket d : Globals.scanDockets) {
	// if (d.getHunNo().equalsIgnoreCase(hun)) {
	// pos = index;
	// break;
	// }
	// index++;
	// }
	// }
	// return pos;
	// }

	public void addScanHun(String hunNo, String scanTime) {
		// int pos = getHunPos(hunNo);
		// hunPos = pos;
		// if (pos != -1) {
		String dkt = dataSource.docketsData.getDktNum(Globals.custCode,
				Globals.custVentCode, hunNo, null, true);
		if (Utils.isValidString(dkt)) {
			ScanDocket docket = new ScanDocket(); // Globals.scanDockets.get(pos);
			docket.setDocketNo(dkt);
			docket.setHunScanned(true);
			docket.setHunScanTime(scanTime);
			docket.setHunNo(hunNo);
			lastHun = hunNo;
			dataSource.scannedData.cacheScanData(Globals.custCode,
					Globals.custVentCode, docket);
			// Globals.scanDockets.set(pos, docket);
			Utils.playAudioList(context, R.raw.scanned);
		}
		// ScanDocket docket = new ScanDocket(); //Globals.scanDockets.get(pos);
		// docket.setDocketNo(docketNo)
		// docket.setHunScanned(true);
		// docket.setHunScanTime(scanTime);
		// Globals.scanDockets.set(pos, docket);
		// Utils.playAudioList(context, R.raw.scanned);
		// }

		scannedHunEt.setText("");
		setScanTextChangedListenerHun(true);
		setScanTextChangedListenerPkt(false);
		scanToFocusPkt();
		reloadData();

	}

	public void addScanPkt(String pktNo, String scanTime) {
		// if (hunPos != -1) {
		// ScanDocket docket = Globals.scanDockets.get(hunPos);
		// docket.setPktNo(pktNo);
		// docket.setPktScanned(true);
		// docket.setPktScanTime(scanTime);
		// Globals.scanDockets.set(hunPos, docket);
		// Utils.playAudioList(context, R.raw.scanned);
		// }

		String dkt = dataSource.docketsData.getDktNum(Globals.custCode,
				Globals.custVentCode, null, pktNo, false);
		if (Utils.isValidString(dkt)) {
			ScanDocket docket = new ScanDocket(); // Globals.scanDockets.get(pos);
			docket.setDocketNo(dkt);
			docket.setHunNo(lastHun);
			docket.setPktScanned(true);
			docket.setPktScanTime(scanTime);
			docket.setPktNo(pktNo);
			dataSource.scannedData.cacheScanData(Globals.custCode,
					Globals.custVentCode, docket);
			// Globals.scanDockets.set(pos, docket);
			Utils.playAudioList(context, R.raw.scanned);
		}

		scannedPktEt.setText("");
		setScanTextChangedListenerHun(false);
		setScanTextChangedListenerPkt(true);
		scanToFocusHun();
		reloadData();
	}

	private void reloadData() {
		if (scanDockets == null)
			scanDockets = new ArrayList<ScanDocket>();
		else
			scanDockets.clear();

		ArrayList<ScanDocket> list = dataSource.scannedData.getScanDocketsList(
				Globals.custCode, Globals.custVentCode);

		if (Utils.isValidArrayList(list)) {
			Utils.logD(list.toString());
			for (ScanDocket d : list) {
				if (d.isHunScanned())
					scanDockets.add(d);
			}
		}

		Collections.sort(scanDockets, new Comparator<ScanDocket>() {
			@Override
			public int compare(ScanDocket o1, ScanDocket o2) {
				return o1.getHunScanTime().compareTo(o2.getHunScanTime());
			}
		});
		Collections.reverse(scanDockets);

		adapter.notifyDataSetChanged();

	}

	public class ViewHolder {
		public TextView scannedHunTv;
		public TextView scannedPktTv;
	}

	public class MyAdapter extends ArrayAdapter<ScanDocket> {
		private LayoutInflater inflater;
		private int layoutId;

		public MyAdapter(Context context, int layoutId, List<ScanDocket> objects) {
			super(context, 0, objects);
			this.layoutId = layoutId;
			this.inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder;
			convertView = inflater.inflate(layoutId, parent, false);
			holder = new ViewHolder();
			holder.scannedHunTv = (TextView) convertView
					.findViewById(R.id.tv_scnned_hun);
			holder.scannedPktTv = (TextView) convertView
					.findViewById(R.id.tv_scnned_pkt);
			convertView.setTag(holder);

			ScanDocket dkt = getItem(position);

			if (dkt != null) {
				String hun = dkt.getHunNo();
				if (Utils.isValidString(hun))
					holder.scannedHunTv.setText(hun);

				String pkt = dkt.getPktNo();
				if (Utils.isValidString(pkt))
					holder.scannedPktTv.setText(pkt);
			}

			return convertView;
		}

	}

	class DataTask extends AsyncTask<Object, Object, Object> {

		@Override
		protected void onPreExecute() {
			Globals.lastErrMsg = "";
			showProgressDialog();
		}

		@Override
		protected Object doInBackground(Object... params) {
			try {

				// if (Globals.docketsList != null)
				// Globals.docketsList.clear();
				// else
				// Globals.docketsList = new ArrayList<DocketToScan>();
				//
				// if (Globals.scanDockets != null)
				// Globals.scanDockets.clear();
				// else
				// Globals.scanDockets = new ArrayList<ScanDocket>();
				//
				// if (Globals.hunList != null)
				// Globals.hunList.clear();
				// else
				// Globals.hunList = new ArrayList<String>();
				//
				// if (Globals.pktList != null)
				// Globals.pktList.clear();
				// else
				// Globals.pktList = new ArrayList<String>();

				String url = GatiScanUtils.getDktHunPkgDetailsUrl(
						Globals.custCode, Globals.custVentCode);
				InputStream is = HttpRequest.getInputStreamFromUrl(url);
				if (is != null) {
					DocketDetailsResponse detailsResponse = (DocketDetailsResponse) Utils
							.parse(is, DocketDetailsResponse.class);
					if (detailsResponse != null) {
						if (detailsResponse.isStatus()) {
							DocketsToScan docketsToScan = detailsResponse
									.getData();
							if (docketsToScan != null) {
								ArrayList<DocketToScan> list = docketsToScan
										.getDktsToScan();
								if (Utils.isValidArrayList(list)) {
									Globals.docketsList = list;
									// for (DocketToScan d : list) {
									// String num = d.getDocketNo();
									// for (String s : d.getHunBarCode()) {
									// ScanDocket scanDocket = new ScanDocket(
									// num, s, false, false);
									// Globals.scanDockets.add(scanDocket);
									// Globals.hunList.add(s);
									// }
									//
									// for (String s : d.getPktNos()) {
									// Globals.pktList.add(s);
									// }
									// }
								}
							} else {
								Globals.lastErrMsg = "No Data found";
							}
						} else {
							Globals.lastErrMsg = detailsResponse.getMessage();
						}
					} else {
						Globals.lastErrMsg = "Response is null";
					}
				} else {
					Globals.lastErrMsg = "Input Stream is null";
				}

			} catch (Exception e) {
				if (!Utils.isValidString(Globals.lastErrMsg))
					Globals.lastErrMsg = e.toString();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Object result) {

			if (showErrorDialog() && pDialog != null && pDialog.isShowing()) {
				pDialog.dismiss();
				dataSource.docketsData.cacheDocketsData(context,
						Globals.custCode, Globals.custVentCode,
						Globals.docketsList);
			}

			super.onPostExecute(result);
		}
	}

	@SuppressWarnings("deprecation")
	private void showProgressDialog() {
		pDialog = new ProgressDialog(context);
		pDialog.setTitle(getString(R.string.loading));
		pDialog.setMessage(getString(R.string.progress_dialog_title));
		pDialog.setIndeterminate(true);
		pDialog.setCancelable(false);
		pDialog.show();
	}

	@SuppressWarnings("deprecation")
	private boolean showErrorDialog() {
		boolean isNotErr = true;
		if (Globals.lastErrMsg != null && Globals.lastErrMsg.length() > 0) {

			errDlg = new AlertDialog.Builder(context).create();
			errDlg.setTitle(getString(R.string.alert_dialog_title));
			errDlg.setCancelable(false);
			errDlg.setMessage(Globals.lastErrMsg);
			Globals.lastErrMsg = "";
			errDlg.setButton(getString(R.string.ok_btn),
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							errDlg.dismiss();
							goBack();
						}
					});
			if (pDialog != null && pDialog.isShowing())
				pDialog.dismiss();
			isNotErr = false;
			errDlg.show();
		}
		return isNotErr;
	}

	public void startScan() {
		setScanTextChangedListenerHun(false);
	}

	private void goBack() {
		finish();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			goBack();
			return true;
		}
		return true;
	}

}
