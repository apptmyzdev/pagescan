package com.revamobile.pagescan;

import java.util.ArrayList;
import java.util.List;

import com.revamobile.gatiscan.utils.Utils;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

public class ScanFromToListAlert extends Dialog {
	public interface DoScanClickListener {
		public void doScan(ScanFromToListAlert scanFromToListAlert);
	}

	private ScanFromToListAlert scanFromToListAlert;
	private ListView lv_scan_from_to;
	private EditText filterText = null;
	private List<String> list = new ArrayList<String>();
	private List<String> docketsList = new ArrayList<String>();
	private DoScanClickListener scanClickListener;

	public DoScanClickListener getScanClickListener() {
		return scanClickListener;
	}

	public void setScanClickListener(DoScanClickListener scanClickListener) {
		this.scanClickListener = scanClickListener;
	}

	private ArrayAdapter<String> dktsAdapter = null;

	public ScanFromToListAlert(final Button btn, final String title,
			final CharSequence[] scanFromToList, int selectionPos,
			Context context, String scanResult,
			DoScanClickListener scanClickListener) {
		super(context);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.scan_from_to_list);
		list.clear();
		if (scanFromToList != null && scanFromToList.length > 0) {
			for (CharSequence c : scanFromToList) {
				list.add(String.valueOf(c));
			}
		}

		this.scanFromToListAlert = this;
		this.setCancelable(false);

		if (scanClickListener != null) {
			setScanClickListener(scanClickListener);
		}

		filterText = (EditText) findViewById(R.id.et_seatch);

		lv_scan_from_to = (ListView) findViewById(R.id.lv_scan_from_to);
		dktsAdapter = new ArrayAdapter<String>(context,
				R.layout.list_single_choice, docketsList);
		lv_scan_from_to.setAdapter(dktsAdapter);
		lv_scan_from_to.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> a, View v, int position,
					long id) {
				btn.setText(String.valueOf(lv_scan_from_to
						.getItemAtPosition(position)));
				scanFromToListAlert.dismiss();
			}
		});
		lv_scan_from_to.setSelection(selectionPos);
		filterText.addTextChangedListener(filterTextWatcher);

		setScanResult(scanResult);
		reloadData(list);

		((TextView) findViewById(R.id.tv_title)).setText(title);
		((ImageButton) findViewById(R.id.ib_scan))
				.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						if (getScanClickListener() != null) {
							getScanClickListener().doScan(scanFromToListAlert);
						}
					}
				});
		((Button) findViewById(R.id.btn_cancel))
				.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						scanFromToListAlert.dismiss();
					}
				});

		try {
			new Handler(context.getMainLooper()).postDelayed(new Runnable() {
				@Override
				public void run() {
					Utils.dissmissKeyboard(filterText);
				}
			}, 100);
		} catch (Exception e) {
		}

	}

	@Override
	public void onStop() {
		filterText.removeTextChangedListener(filterTextWatcher);
	}

	private TextWatcher filterTextWatcher = new TextWatcher() {
		@Override
		public void afterTextChanged(Editable s) {
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			String str = s.toString().trim();
			if (str.length() > 0) {
				List<String> filteredList = filter(list, str);
				reloadData(filteredList);
			} else {
				reloadData(list);
			}
		}
	};

	public void setScanResult(String scanResult) {
		if (scanResult != null && scanResult.trim().length() > 0) {
			filterText.setText(scanResult);
			List<String> filterList = filter(list, scanResult);
			reloadData(filterList);
		}

		filterText.addTextChangedListener(filterTextWatcher);
		filterText.setSelection(filterText.length());
		Utils.dissmissKeyboard(filterText);
	}

	@SuppressLint("DefaultLocale")
	public static List<String> filter(List<String> list, String txt) {

		List<String> resultList = new ArrayList<String>();

		for (int i = 0; i < list.size(); i++) {
			if (txt.length() <= list.get(i).length()) {
				if (txt.equalsIgnoreCase((String) list.get(i).subSequence(0,
						txt.length())))
					resultList.add(list.get(i));
			}
		}

		for (int i = 0; i < list.size(); i++) {
			if (txt.length() <= list.get(i).length()) {
				if (list.get(i).toLowerCase().contains(txt.toLowerCase())
						&& !txt.equalsIgnoreCase((String) list.get(i)
								.subSequence(0, txt.length())))
					resultList.add(list.get(i));
			}
		}

		return resultList;
	}

	private void clearList() {
		if (docketsList != null)
			docketsList.clear();
		refreshDkts();
	}

	private void reloadData(List<String> dktsList) {
		clearList();
		for (String d : dktsList)
			docketsList.add(d);
		refreshDkts();
	}

	private void refreshDkts() {
		if (dktsAdapter != null)
			dktsAdapter.notifyDataSetChanged();
	}

}