package com.revamobile.pagescan;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.map.ObjectMapper;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.IntentCompat;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.revamobile.gatiscan.data.DktHunPkgMappingModel;
import com.revamobile.gatiscan.data.GatiScanUtils;
import com.revamobile.gatiscan.data.GeneralResponse;
import com.revamobile.gatiscan.data.ScanDocket;
import com.revamobile.gatiscan.data.ScannedDktDetails;
import com.revamobile.gatiscan.data.ScannedDocketsDataModel;
import com.revamobile.gatiscan.datacache.DataSource;
import com.revamobile.gatiscan.utils.Constants;
import com.revamobile.gatiscan.utils.Globals;
import com.revamobile.gatiscan.utils.HttpRequest;
import com.revamobile.gatiscan.utils.Utils;

public class Reconcile extends Activity {

	private Context context;
	private AsyncTask submit;

	private ProgressDialog pDialog;
	private AlertDialog errDlg;

	private String data = "";

	private TextView noOfDocketsTv, noOfHunsTv, noOfPktsTv, scannedHunsTv,
			scannedPktsTv, logout;
	private Button btn_submit, btn_cancel;
	private int totalNoOfDockets = 0, totalNoOfHuns = 0, totalNoOfPackets = 0,
			scannedHuns = 0, scannedPkts = 0;

	private ArrayList<ScannedDocketsDataModel> list = new ArrayList<ScannedDocketsDataModel>();

	private TextView versionTopTv, userNameTv;

	private String versionVal = "";

	private DataSource dataSource;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.reconcile);

		context = Reconcile.this;

		dataSource = new DataSource(context);

		noOfDocketsTv = (TextView) findViewById(R.id.tv_total_dockets);
		noOfHunsTv = (TextView) findViewById(R.id.tv_total_huns);
		noOfPktsTv = (TextView) findViewById(R.id.tv_total_packets);
		scannedHunsTv = (TextView) findViewById(R.id.tv_huns_scanned);
		scannedPktsTv = (TextView) findViewById(R.id.tv_pkts_scanned);

		logout = (TextView) findViewById(R.id.tv_logout);
		logout.setVisibility(View.VISIBLE);
		logout.setOnClickListener(Utils.logoutListener);

		btn_submit = (Button) findViewById(R.id.btn_submit);
		btn_submit.setOnClickListener(onClickListener);

		btn_cancel = (Button) findViewById(R.id.btn_cancel_submit);
		btn_cancel.setOnClickListener(onClickListener);

		getAllValues();

		noOfDocketsTv.setText(String.valueOf(totalNoOfDockets));
		noOfHunsTv.setText(String.valueOf(totalNoOfHuns));
		noOfPktsTv.setText(String.valueOf(totalNoOfPackets));
		scannedHunsTv.setText(String.valueOf(scannedHuns));
		scannedPktsTv.setText(String.valueOf(scannedPkts));

		versionTopTv = (TextView) findViewById(R.id.tv_version_top);

		try {
			versionVal = getPackageManager()
					.getPackageInfo(getPackageName(), 0).versionName;
			if (Utils.isValidString(versionVal))
				versionTopTv.setText("Ver : " + versionVal);
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}

		userNameTv = (TextView) findViewById(R.id.tv_username);
		String user = dataSource.shardPreferences
				.getValue(Constants.USERNAME_PREF);
		if (Utils.isValidString(user))
			userNameTv.setText(user);

	}

	private void getAllValues() {
		totalNoOfDockets = 0;
		totalNoOfHuns = 0;
		totalNoOfPackets = 0;
		scannedHuns = 0;
		scannedPkts = 0;

		totalNoOfDockets = dataSource.docketsData.getDktsSize(Globals.custCode,
				Globals.custVentCode); // Globals.docketsList.size();
		totalNoOfHuns = dataSource.docketsData.getHunsSize(Globals.custCode,
				Globals.custVentCode);
		totalNoOfPackets = dataSource.docketsData.getPktsSize(Globals.custCode,
				Globals.custVentCode);

		scannedHuns = dataSource.scannedData.getHunsSize(Globals.custCode,
				Globals.custVentCode); // Globals.hunList.size();
		scannedPkts = dataSource.scannedData.getPktsSize(Globals.custCode,
				Globals.custVentCode); // Globals.pktList.size();

		// if (Utils.isValidArrayList(Globals.scanDockets)) {
		// for (ScanDocket d : Globals.scanDockets) {
		// if (d.isHunScanned())
		// scannedHuns++;
		// if (d.isPktScanned())
		// scannedPkts++;
		// }
		// }

	}

	private View.OnClickListener onClickListener = new View.OnClickListener() {

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.btn_submit:
				// if ((scannedHuns == totalNoOfHuns)
				// && (scannedPkts == totalNoOfPackets)) {
				if ((scannedHuns > 0 && scannedPkts > 0)
						&& (scannedHuns == scannedPkts)) {
					getSubmitData();
					// list = dataSource.scannedData.getScannedDocketsList(
					// Globals.custCode, Globals.custVentCode);
					if (Utils.isValidArrayList(list)) {
						Utils.logD(list.toString());
						new Submit().execute();
					}
				} else {
					Utils.showSimpleAlert(context,
							"Please scan atleast one HUN and Packet to submit");
//					Utils.showSimpleAlert(context,
//							"Please scan all the HUN's and Packets to submit");
				}

				break;

			case R.id.btn_cancel_submit:
				finish();
				break;

			default:
				break;
			}

		}
	};

	private void getSubmitData() {
		list = new ArrayList<ScannedDocketsDataModel>();
		ArrayList<String> docketsList = dataSource.docketsData.getDocketsList(
				Globals.custCode, Globals.custVentCode);
		ArrayList<ScanDocket> scanDockets = dataSource.scannedData
				.getDocketsList(Globals.custCode, Globals.custVentCode);
		if (Utils.isValidArrayList(docketsList)
				&& Utils.isValidArrayList(scanDockets)) {

			for (int i = 0; i < docketsList.size(); i++) {
				for (ScanDocket dkt : scanDockets) {
					if (dkt.isHunScanned()) {
						String s1 = docketsList.get(i);
						String s2 = dkt.getDocketNo();

						if (s1.equalsIgnoreCase(s2)) {
							ScannedDocketsDataModel scannedDocketsDataModel = null;
							if (!Utils.isValidArrayList(list)) {
								scannedDocketsDataModel = new ScannedDocketsDataModel();
								scannedDocketsDataModel.setDocketNo(s1);
								List<DktHunPkgMappingModel> hunPktsList = scannedDocketsDataModel
										.getDktHunPkgMappings();
								if (!Utils
										.isValidArrayList((ArrayList<?>) hunPktsList)) {
									hunPktsList = new ArrayList<DktHunPkgMappingModel>();
								}
								hunPktsList.add(new DktHunPkgMappingModel(dkt
										.getHunNo(), dkt.getPktNo(), dkt
										.getHunScanTime()));
								scannedDocketsDataModel
										.setDktHunPkgMappings(hunPktsList);
								list.add(scannedDocketsDataModel);
							} else {
								int pos = getDocket(s1);
								if (pos != -1)
									scannedDocketsDataModel = list.get(pos);
								if (scannedDocketsDataModel != null) {
									List<DktHunPkgMappingModel> hunPktsList = scannedDocketsDataModel
											.getDktHunPkgMappings();
									if (!Utils
											.isValidArrayList((ArrayList<?>) hunPktsList)) {
										hunPktsList = new ArrayList<DktHunPkgMappingModel>();
									}
									hunPktsList.add(new DktHunPkgMappingModel(
											dkt.getHunNo(), dkt.getPktNo(), dkt
													.getHunScanTime()));
									scannedDocketsDataModel
											.setDktHunPkgMappings(hunPktsList);
									list.set(pos, scannedDocketsDataModel);
								} else {
									scannedDocketsDataModel = new ScannedDocketsDataModel();
									scannedDocketsDataModel.setDocketNo(s1);
									List<DktHunPkgMappingModel> hunPktsList = scannedDocketsDataModel
											.getDktHunPkgMappings();
									if (!Utils
											.isValidArrayList((ArrayList<?>) hunPktsList)) {
										hunPktsList = new ArrayList<DktHunPkgMappingModel>();
									}
									hunPktsList.add(new DktHunPkgMappingModel(
											dkt.getHunNo(), dkt.getPktNo(), dkt
													.getHunScanTime()));
									scannedDocketsDataModel
											.setDktHunPkgMappings(hunPktsList);
									list.add(scannedDocketsDataModel);

								}
							}
						} else
							continue;
					}
				}
			}
		}

	}

	private int getDocket(String num) {
		int pos = -1;
		if (Utils.isValidArrayList(list)) {
			for (int i = 0; i < list.size(); i++) {
				if (list.get(i).getDocketNo().equalsIgnoreCase(num)) {
					pos = i;
					return pos;
				}
			}
		}
		return pos;
	}

	private void showAlertDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setTitle("Confirm");

		builder.setMessage("Are you sure you want to submit?");
		builder.setPositiveButton(getString(R.string.ok_btn),
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						submit = new Submit().execute(data);
					}

				});
		builder.setNeutralButton(getString(R.string.cancel_btn),
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}

				});

		AlertDialog alertDialog = builder.create();
		alertDialog.show();

	}

	class Submit extends AsyncTask<Object, Object, Object> {

		@Override
		protected void onPreExecute() {
			showProgressDialog();

		}

		@Override
		protected Object doInBackground(Object... params) {
			try {

				String submitUrl = GatiScanUtils.getSubmitUrl();

				ObjectMapper mapper = new ObjectMapper();

				ScannedDktDetails dktDetails = new ScannedDktDetails(list);

				data = mapper.writeValueAsString(dktDetails);

				if (Utils.isValidString(data) && Utils.isValidString(submitUrl)) {

					Utils.logD("Submit Data : " + data);
					GeneralResponse response = (GeneralResponse) HttpRequest
							.postData(submitUrl, data, GeneralResponse.class);
					// PostData.getRespose(Globals.username, data);
					if (response != null) {
						if (response.isStatus()) {
							Utils.logD(response.toString());
						} else {
							Globals.lastErrMsg = response.getMessage();
						}
					} else {
						Globals.lastErrMsg = "Server Response is null";
					}

				}

			} catch (Exception e) {
				if (!Utils.isValidString(Globals.lastErrMsg))
					Globals.lastErrMsg = e.toString();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Object result) {

			if (showErrorDialog() && pDialog != null && pDialog.isShowing()) {
				if (Utils.isValidArrayList(Globals.docketsList))
					Globals.docketsList.clear();
				// Globals.scanDockets.clear();
				// Globals.hunList.clear();
				// Globals.pktList.clear();
				dataSource.docketsData.clearDocketsData(Globals.custCode,
						Globals.custVentCode);
				dataSource.scannedData.clearScanData(Globals.custCode,
						Globals.custVentCode);
				Globals.custCode = null;
				Globals.custVentCode = null;
				pDialog.dismiss();
				Utils.showToast(context, "Successful");
				goToHome();
			}

			super.onPostExecute(result);
		}
	}

	private void goToHome() {
		Intent intent = new Intent(context, HomeScreen.class);
		ComponentName cn = intent.getComponent();
		Intent mainIntent = IntentCompat.makeRestartActivityTask(cn);
		startActivity(mainIntent);
		finish();

	}

	@SuppressWarnings("deprecation")
	private void showProgressDialog() {
		pDialog = new ProgressDialog(context);
		pDialog.setTitle(getString(R.string.loading));
		pDialog.setMessage(getString(R.string.progress_dialog_title));
		pDialog.setIndeterminate(true);
		pDialog.setCancelable(false);
		pDialog.show();
	}

	@SuppressWarnings("deprecation")
	private boolean showErrorDialog() {
		boolean isNotErr = true;
		if (Globals.lastErrMsg != null && Globals.lastErrMsg.length() > 0) {

			errDlg = new AlertDialog.Builder(context).create();
			errDlg.setTitle(getString(R.string.alert_dialog_title));
			errDlg.setCancelable(false);
			errDlg.setMessage(Globals.lastErrMsg);
			Globals.lastErrMsg = "";
			errDlg.setButton(getString(R.string.ok_btn),
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							errDlg.dismiss();
						}
					});
			if (pDialog != null && pDialog.isShowing())
				pDialog.dismiss();
			isNotErr = false;
			errDlg.show();
		}
		return isNotErr;
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			finish();
			return true;
		}
		return true;
	}

	@Override
	protected void onPostResume() {
		super.onPostResume();
	}

}
