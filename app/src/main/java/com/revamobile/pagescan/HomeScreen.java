package com.revamobile.pagescan;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.revamobile.gatiscan.datacache.DataSource;
import com.revamobile.gatiscan.utils.Constants;
import com.revamobile.gatiscan.utils.Globals;
import com.revamobile.gatiscan.utils.Utils;

public class HomeScreen extends Activity {
	private Context context;

	private EditText custCodeEt, custVentCodeEt;
	private Button pushBtn, printBtn;

	private TextView versionTopTv, userNameTv;

	private DataSource dataSource;

	private String versionVal = "";

	private TextView logout;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.home_screen_new);

		context = HomeScreen.this;

		dataSource = new DataSource(context);

		custCodeEt = (EditText) findViewById(R.id.et_cust_code);
		custVentCodeEt = (EditText) findViewById(R.id.et_cust_vent_code);

		pushBtn = (Button) findViewById(R.id.btn_push_dockets);
		pushBtn.setOnClickListener(listener);

		printBtn = (Button) findViewById(R.id.btn_print_labels);
		printBtn.setOnClickListener(listener);

		versionTopTv = (TextView) findViewById(R.id.tv_version_top);

		try {
			versionVal = getPackageManager()
					.getPackageInfo(getPackageName(), 0).versionName;
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}

		if (Utils.isValidString(versionVal)) {
			versionTopTv.setText("Ver : " + versionVal);
		}

		userNameTv = (TextView) findViewById(R.id.tv_username);
		String user = dataSource.shardPreferences
				.getValue(Constants.USERNAME_PREF);
		if (Utils.isValidString(user))
			userNameTv.setText(user);

		logout = (TextView) findViewById(R.id.tv_logout);
		logout.setVisibility(View.VISIBLE);
		logout.setOnClickListener(Utils.logoutListener);
	}

	private View.OnClickListener listener = new View.OnClickListener() {

		@Override
		public void onClick(View v) {
			String cust = custCodeEt.getText().toString().trim();
			String custVent = custVentCodeEt.getText().toString().trim();
			switch (v.getId()) {
			case R.id.btn_push_dockets:

				if (Utils.isValidString(cust) && Utils.isValidString(custVent)) {
					Globals.custCode = cust;
					Globals.custVentCode = custVent;
					Utils.dissmissKeyboard(custCodeEt);
					goToPushDockets();
				} else {
					Utils.showSimpleAlert(context,
							"Please enter valid Cust Code and Cust Vent Code");
				}
				break;

			case R.id.btn_print_labels:
				if (Utils.isValidString(cust) && Utils.isValidString(custVent)) {
					Globals.custCode = cust;
					Globals.custVentCode = custVent;
					Utils.dissmissKeyboard(custCodeEt);
					goNext();
				} else {
					Utils.showSimpleAlert(context,
							"Please enter valid Cust Code and Cust Vent Code");
				}
				break;

			default:
				break;
			}
		}
	};

	private void goNext() {
		Intent intent = new Intent(context, OnLineScreen.class);
		startActivity(intent);
	}

	private void goToPushDockets() {
		Intent intent = new Intent(context, PushDocketsScreen.class);
		startActivity(intent);
	}

	@Override
	protected void onPostResume() {
		if (Globals.isBack) {
			Globals.isBack = false;
			custCodeEt.setText("");
			custVentCodeEt.setText("");
		}
		super.onPostResume();
	}

}
